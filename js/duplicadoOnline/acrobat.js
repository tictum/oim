	<!--
	// Variables que intervienen en la comprobacion del formato en el cual se generara el contrato (ReconocimientoFinProceso.jsp) 
	var agt=navigator.userAgent.toLowerCase();
	var ie = (agt.indexOf("msie") != -1);
	var ns = (navigator.appName.indexOf("Netscape") != -1);
	var win = ((agt.indexOf("win")!=-1) || (agt.indexOf("32bit")!=-1));
	var mac = (agt.indexOf("mac")!=-1);
	

	function detectarPdf()
	{
        if (ie && win) 
        { 
            pluginlist = detectIE("PDF.PdfCtrl.4","Acrobat Reader") + detectIE("PDF.PdfCtrl.5","Acrobat Reader") + detectIE("PDF.PdfCtrl.6","Acrobat Reader") + detectIE("AcroPDF.PDF.1","Acrobat Reader"); 
            return pluginlist;
        }
        if (ns || !win) 
        {
			nse = ""; 
            for (var i=0;i<navigator.mimeTypes.length;i++) 
            {
                nse += navigator.mimeTypes[i].type.toLowerCase();
			}
            pluginlist = detectNS("application/pdf","Acrobat Reader");
			return pluginlist;

        }
	
	}
	 
	function detectIE(ClassID,name) 
	{ 
		result = false;
		document.write("<SCRIPT LANGUAGE=VBScript>\n on error resume next \n result = IsObject(CreateObject(\"" + ClassID + "\"))<\/SCRIPT>"); 
	
	    if (result)
	    { 
			return name+','; 
		}
		else
		{
			return ''; 
		}
	}
	
	function detectNS(ClassID,name) 
	{ 
		n = ""; 
		if (nse.indexOf(ClassID) != -1) 
		{
			if (navigator.mimeTypes[ClassID].enabledPlugin != null) 
			{
	         	n = name+","; 
			}
	        return n; 
		}
	}
	
	function detectarFormatoContrato()
	{
		if( ! detectarPdf())
		{
			pdf = "false";
		}
		else
		{
			pdf = "true";
		}
		return pdf;
	}
	
//-----------------------------------------------------------------------------------
//     Funcion: mostrarDescargarAcrobat
//  Descripcion: Funci�n que oculta el mensaje de descarga del PDF en caso de que detecte que est� instalado
//-----------------------------------------------------------------------------------

 function mostrarDescargarAcrobat(){
 	//if(detectarPdf()){
 	if(!AcrobatInstalled()){ 		
 		document.getElementById("acrobat").style.display='block';
 	}
 }
 
 //-----------------------------------------------------------------------------------
//     Funcion: mostrarPDF
//  Descripcion: Funci�n para Abrir una ventana y devolvemos el foco a la ventana
//  Parametros: url de la ventana que se quiere abrir
//    ancho de la ventana
//    alto de la ventana
//    nombre que se le da a la ventana que queremos abrir 
//-----------------------------------------------------------------------------------
function mostrarPDF(url,ancho,alto,nombre)
{


 //controlamos que tengal el plug-in instalado

if( !AcrobatInstalled() ){
 	alert(acrobatError);
 	return false;
 }

 var objetoOpen;
 if(nombre != ''){
   	 objetoOpen = eval("window.open(url, '"+nombre+"', 'toolbar=no, directories=no, location=no, status=no, menubar=no, resizable=yes, scrollbars=yes, width="+ancho+", height="+alto+"')"); 

  }
 else{
  objetoOpen = eval("window.open(url, 'ventana', 'toolbar=no, directories=no, location=no, status=no, menubar=no, resizable=yes, scrollbars=yes, width="+ancho+", height="+alto+"')"); 
 }


 //Le damos el foco a la nueva ventana.
 if (objetoOpen != null){
  objetoOpen.focus();
 }

}

//-----------------------------------------------------------------------------------
//  Funcion: enviarOpcion
//  Descripcion: Env�a el formulario con la opci�n seleccionada previa comprobaci�n 
//				 de que efectivamente se haya seleccionado alguna- 
//
//-----------------------------------------------------------------------------------

 function enviarOpcion(){
 	
 	var radio = document.getElementsByName('documento'); 
	var valor = 'nulo';
	 for(i = 0; i < radio.length; i ++ ){
	  if(radio[i].checked){
	   valor=radio[i].value;
	  }
   	}
   	if(valor == 'nulo'){
   		alert(errorOpcion);
   		return false;
   	}
   
   	document.duplicadoDocumentosMutuaForm.submit();
 }
 


		//////////////////////////
		// Funciones de Ricardo //
		//////////////////////////


function AcrobatInstalled(){
var acrobat=new Object();
acrobat.installed=false;
acrobat.version='0.0';
if (navigator.plugins && navigator.plugins.length)
{  for ( var x = 0, l = navigator.plugins.length; x < l; ++x ) 
     {
        if (navigator.plugins[x].description.indexOf('Adobe Acrobat') != -1 || navigator.plugins[x].description.indexOf('Adobe PDF') != -1)
           {
               acrobat.version=parseFloat(navigator.plugins[x].description.split('Version ')[1]);

                if (acrobat.version.toString().length == 1) acrobat.version+='.0';

               acrobat.installed=true;
               break;
               }
         }
 }
  else if (window.ActiveXObject)
{
     for (x=2; x<10; x++)
        {
           try{
				oAcro=eval("new ActiveXObject('PDF.PdfCtrl."+x+"');");
				if (oAcro){
					acrobat.installed=true;
					acrobat.version=x+'.0';
					}
				}
				catch(e) {}
		}

	try{
		oAcro4=new ActiveXObject('PDF.PdfCtrl.1');
		if (oAcro4){
			acrobat.installed=true;
			acrobat.version='4.0';
			}
		}
		catch(e) {}

	try{
		oAcro7=new ActiveXObject('AcroPDF.PDF.1');
		if (oAcro7){
			acrobat.installed=true;
			acrobat.version='7.0';
			}
		}
		catch(e) {}
}
return acrobat.installed;
}
function descargaAcrobat(){

	window.open("http://www.adobe.es/products/acrobat/readstep.html");
}
//-->

 