/**
 * Oculta o muestra el campo de correo el�ctronico para que el usuario
 * lo rellene en caso de haber seleccionado esa forma de contacto.
 */
function comprueba() {
	if((document.forms[0].contacto.options[document.forms[0].contacto.selectedIndex].value)=="correo") {
		document.getElementById("dir").style.display = "block";
	} else {
		document.getElementById("dir").style.display = "none";
	}
}

/**
 * Deshabilita los combos de "Tipo de producto" y "Producto" relativos a
 * las opciones de Consulta e Incidencia sobre mis productos; resetea el 
 * valor contenido en el campo "Contrato"
 */
function deshabilitaCombos() {
	if (document.forms[0].tipoProducto) {
		document.forms[0].tipoProducto.disabled = 'disabled';
		document.forms[0].tipoProducto.selectedIndex = 0;
	}

	if (document.forms[0].producto) {
		document.forms[0].producto.disabled = 'disabled';
		document.forms[0].producto.selectedIndex = 0;
		if (document.forms[0].contrato.value != '') {
			document.forms[0].contrato.value = '';
		}
	}
}

/**
 * Habilita los combos de "Tipo de producto" y "Producto" relativos a
 * las opciones de Consulta e Incidencia sobre mis productos.
 */
function habilitaCombos() {
	if (document.forms[0].tipoProducto) {
		document.forms[0].tipoProducto.disabled = '';
	}

	if (document.forms[0].producto) {
		document.forms[0].producto.disabled='';
	}
}

/**
 * Resetea los valores de los combos de "Tipo de producto" y "Producto" 
 * relativos a las opciones de Consulta e Incidencia sobre mis productos; 
 * resetea el valor contenido en el campo "Contrato"
 */
function resetearCombos() {		
	if (document.forms[0].tipoProducto) {
		document.forms[0].tipoProducto.selectedIndex = 0;
	}

	if (document.forms[0].producto) {
		document.forms[0].producto.selectedIndex = 0;
		if (document.forms[0].contrato.value != '') {
			document.forms[0].contrato.value = '';
		}
		document.forms[0].producto.disabled = 'disabled';
	}
}

/**
 * Deshabilita los combos de "Tipo de producto" y "Producto" relativos a l
 * as opciones de Consulta e Incidencia sobre mis productos en el caso de 
 * que el usuario haya seleccionado el radiobutton de "Problema t�cnico con 
 * la web"
 */
function comprobarEstadoCombos() {
	if (document.forms[0].Opc3B.checked == true) {
		deshabilitaCombos();
	}
}

/**
 * Determina el formulario que se mostrar�n en la pantalla en 
 * fucni�n dela opcion seleccionado
 */
function mostrarOcultarFormularios(etiqueta) {
	if (etiqueta == 'consulta') {
		document.getElementById('tb1').click();

	} else if (etiqueta == 'incidencia') {
		document.getElementById('tb2').click();
	
	}  else if (etiqueta == 'problemaTecnico') {
		document.getElementById('tb3').click();
	
	}			
}

/**
 * Determina los campos del formulario que se mostrar�n en la pantalla en 
 * fucni�n del radiobutton seleccionado
 */
function mostrarOcultarBloques(etiqueta) {
	document.getElementById('cajaColor').className = 'cajaColor anchoCaja7 displayBlock';
	document.forms[0].motivo.value = '';
	if (etiqueta == 'consulta1') {
		mostrarBloqueConsulta();
	} else if (etiqueta == 'incidencia1' || etiqueta == 'incidencia2') {
		mostrarBloqueIncidencia();
	}  else if (etiqueta == 'problemaTecnico1' || etiqueta == 'problemaTecnico2') {
		mostrarBloqueProblemaTecnico();
	}			
}

/**
 * Muestra la informaci�n relativa a "Consulta sobre mis productos" y
 * oculta el resto.
 */
function mostrarBloqueConsulta() {
	mostrarOcultarObligatoriedad('none');
	
	document.getElementById('tipoProd').className = 'formu displayBlock';
	document.getElementById('prod').className = 'formu displayBlock';
	document.getElementById('formaContac').className = 'formu displayBlock';
	document.getElementById('descAsunto').className = 'formu displayBlock';
	onOfProducto();
	
	document.getElementById('incidencia1').className = 'displayBlock';
	document.getElementById('incidencia2').className = 'displayNone';
	
	document.getElementById('problemaTecnico1').className = 'displayBlock';
	document.getElementById('problemaTecnico2').className = 'displayNone';
	
	document.getElementById('Opc1').checked = true;
}

/**
 * Muestra la informaci�n relativa a "Incidencia con mis productos" y
 * oculta el resto.
 */
function mostrarBloqueIncidencia() {
	mostrarOcultarObligatoriedad('');

	document.getElementById('tipoProd').className = 'formu displayBlock';
	document.getElementById('prod').className = 'formu displayBlock';			
	document.getElementById('formaContac').className = 'formu displayBlock';
	document.getElementById('descAsunto').className = 'formu displayBlock';
	onOfProducto();
	
	document.getElementById('incidencia1').className = 'displayNone';
	document.getElementById('incidencia2').className = 'displayBlock';
	document.getElementById('Opc2B').checked = true;
	
	document.getElementById('problemaTecnico1').className = 'displayBlock';
	document.getElementById('problemaTecnico2').className = 'displayNone';
}

/**
 * Muestra la informaci�n relativa a "Problema t�cnico con la web" y
 * oculta el resto.
 */
function mostrarBloqueProblemaTecnico() {
	document.getElementById('tipoProd').className = 'formu displayNone';
	document.getElementById('prod').className = 'formu displayNone';
	document.getElementById('formaContac').className = 'formu displayBlock';
	document.getElementById('descAsunto').className = 'formu displayBlock';
	
	document.getElementById('incidencia1').className = 'displayNone';
	document.getElementById('incidencia2').className = 'displayBlock';
	
	document.getElementById('problemaTecnico1').className = 'displayNone';
	document.getElementById('problemaTecnico2').className = 'displayBlock';
	document.getElementById('Opc3B').checked = true;
}

/**
 * Imposibilita seleccionar un producto a no ser que se haya seleccionado
 * previamente el tipo del mismo
 */
function onOfProducto() {
	if (document.forms[0].tipoProducto.value == '[Seleccione]') {
		document.forms[0].producto.disabled = 'disabled';
	} else {
		document.forms[0].producto.disabled = '';
	}
}

/**
 * Resetea la pantalla cuando no hay ning�n radiobutton seleccionado
 */
function ocultarBloques() {
	document.getElementById('tipoProd').className = 'formu displayNone';
	document.getElementById('prod').className = 'formu displayNone';
	document.getElementById('formaContac').className = 'formu displayNone';
	document.getElementById('descAsunto').className = 'formu displayNone';
	document.getElementById('incidencia1').className = 'displayNone';
	document.getElementById('incidencia2').styleClass = 'displayBlock';
	document.getElementById('problemaTecnico1').className = 'displayNone';
	document.getElementById('problemaTecnico2').className = 'displayBlock';
}

/**
 * Determina los bloques del formulario que se deben mostrar en 
 * funci�n del radiobutton seleccionado
 */
function seleccionarRadio(radioSeleccionado) {			
	if (radioSeleccionado == 'ninguno') {
		noVeoMiProducto(false);
		ocultarBloques();
	} else if (radioSeleccionado == 'consulta') {
		noVeoMiProducto(true);
		mostrarOcultarBloques('consulta1');
	} else if (radioSeleccionado == 'incidencia') {
		noVeoMiProducto(false);
		mostrarOcultarBloques('incidencia1');
		if (document.getElementById('tipoProducto') != null) {
			if (document.getElementById('tipoProducto').value != 'Seleccione') {
				if (document.getElementById('tipoProducto').value == 'No veo mi producto') {
					document.getElementById('producto').disabled = 'disabled';
					document.getElementById('contrato').disabled = 'disabled';
					document.getElementById('prod').className = 'formu displayNone';
				} else {
					document.getElementById('producto').disabled = '';
					//document.getElementById('contrato').disabled = '';
					document.getElementById('prod').className = 'formu displayBlock';
				}
			}
		}
	} else if (radioSeleccionado == 'problemaTecnico') {
		noVeoMiProducto(true);
		mostrarOcultarBloques('problemaTecnico1');
	} 
}

/**
 * Incluye la opci�n "No veo mi producto" dentro del combo tipo de producto
 */
function noVeoMiProducto(borrar) {
	var valor = 'No veo mi producto';	
	if (document.getElementById('tipoProducto') != null) {
		var elementos = document.getElementById('tipoProducto').options.length;
		var ultimo = document.getElementById('tipoProducto').options[elementos -1];
		if (borrar && ultimo.value == valor) {
			ultimo.parentNode.removeChild(ultimo);
		} else if (ultimo.value != valor) {
			document.getElementById('tipoProducto').options[elementos] = new Option(valor, valor);
		}
	}
}

/**
 * Elimina la opci�n "teCuidamos" dentro del combo tipo de producto
 */
function teCuidamos(borrar) {
	var valor = 'Tecuidamos';	
	if (borrar && document.getElementById('tipoProducto') != null) {
		var elementos = document.getElementById('tipoProducto').options;
		for(var i=0; i<elementos.length; i++){
			var elem = elementos[i];
			if(elem != null){
				if(elem.value == valor){
					elem.parentNode.removeChild(elem);
				}
			}
		}
	}
}


/**
 * Muestra u oculta los asteriscos que marcan que campos son obligatorios
 */
function mostrarOcultarObligatoriedad(mostrar) {
	for (var i = 1; i < 4; i++) {
		document.getElementById('asterisco' + i).style.display = mostrar;
	}
}

/**
 * Se ejecuta al seleccionar un tipo de producto en el combo correspondiente
 */
function cargarTipoProducto() {
	document.forms[0].producto.disabled = '';
	document.forms[0].producto.selectedIndex = 0;
	var asunto = obtenerAsunto();
	document.tarificador.action='ContactarCargaProductoAction.do?asunto=' + asunto;
	document.tarificador.submit();			
}

/**
 * Se ejecuta al seleccionar un producto en el combo correspondiente
 */
function cargaProducto() {
	var asunto = obtenerAsunto();
	document.tarificador.action='ContactarCargaFinalAction.do?asunto=' + asunto;
	document.tarificador.submit();
}

/**
 * Obtiene el radiobutton seleccionado en pantalla
 */
function obtenerAsunto() {
	var asunto = '';
	var total = document.forms[0].asunto.length;
	var encontrado = false;
	var contador = 0;
	while (!encontrado && (contador < total)) {
		if (document.forms[0].asunto[contador].checked) {
			asunto = document.forms[0].asunto[contador].value;
			encontrado = true;
		} else {
			contador++;
		}
	}
	return asunto;
}

function mostarTextoMotivoConsulta(texto) {
	document.forms[0].motivo.value = texto;
}