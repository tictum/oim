var tablasProductos = function(){

	var CLASE_PARA_OCULTAR = "offscreen";

	function _tablasOperacional(){
		var $botonoperacional = $(".botonoperacional");
		$botonoperacional.on("click", function (event) {
			event.preventDefault();
			event.stopPropagation();
			var $that = $(this),
				parentt = $that.parent();
			_cambiaEstadoBoton(parentt);
			_cambiaEstadoTabla(parentt.find(".mostrarmas"));
		});
	}

	function _cambiaEstadoTabla(tbody){
		var ariaExp = "aria-expanded",
			ariaHid = "aria-hidden";
		tbody.toggle();
		tbody.attr(ariaExp, _getValue(tbody, ariaExp));
		tbody.attr(ariaHid, _getValue(tbody, ariaHid));
	}
	function _cambiaEstadoBoton(elemento){
		var ariaHid = "aria-hidden",
			$mostrar = elemento.find(".mostrar"),
			$ocultar = elemento.find(".ocultar");
		$mostrar.toggleClass(CLASE_PARA_OCULTAR);
		$ocultar.toggleClass(CLASE_PARA_OCULTAR);
		$mostrar.attr(ariaHid, _getValue($mostrar, ariaHid));
		$ocultar.attr(ariaHid, _getValue($ocultar, ariaHid));
	}
	function _getValue(elemento, atributo){
		var valor = elemento.attr(atributo);
		return !( valor === "true" );
    }
    function init(){
		//OCULTAMOS BOTONES Y PARTE DE POLIZAS
		var $fragmentosTabla = $(".mostrarmas");
		$fragmentosTabla.hide();
		$fragmentosTabla.attr("aria-hidden", "true");
		$(".botonoperacional .ocultar").addClass(CLASE_PARA_OCULTAR);
		_tablasOperacional();
	}
	return {
		init:init
	};

}();


var slider = function(){

	function init(){
		$('.flexslider').flexslider({
			animation: "fade",
			prevText: "Anterior",
			nextText: "Siguiente",
			pausePlay: true,
			pauseText: 'Detener',
			playText: 'Iniciar',
			slideshow: true,
			slideshowSpeed: 5000,
			initDelay: 0
		});
	}
	return {
		init:init
	};

}();

var accesosDirectos = function(){

	var CLASE_PARA_OCULTAR = "offscreen",
		elementoAbierto, dirDownToUp = false;

	function _asignarRecibirFoco(elemento, listado){
		elemento.on("focus", function(){
			if(elemento.parent().is(":last-child")){
				if(listado.hasClass(CLASE_PARA_OCULTAR)){
					_ocultarElQueEsteAbierto();
					listado.removeClass(CLASE_PARA_OCULTAR);
					elementoAbierto = listado;
					dirDownToUp = true;
				}else{
					dirDownToUp = false;
				}
			}
		});
	}
	function _asignarPerderFoco(elemento, listado){
		elemento.on("blur", function(){
			if(elemento.parent().is(":last-child") && !dirDownToUp){
				listado.addClass(CLASE_PARA_OCULTAR);
				_ocultarElQueEsteAbierto();
			}
		});
	}
	function _ocultarElQueEsteAbierto(){
		if(elementoAbierto){
			elementoAbierto.addClass(CLASE_PARA_OCULTAR);
		}
	}
	function _setFocusable(elemento){
		elemento.get(0).setAttribute("tabIndex", 0);
		elemento.on("focus",function(){
			var ul = $(this).find("ul");
			_ocultarElQueEsteAbierto();
			ul.removeClass(CLASE_PARA_OCULTAR);
			elementoAbierto = ul;
		});

	}
	function init() {
		var enlaces = $(".quieroIr"),
			opcionesDirectas = $ (".irDirecto a"),
			tm = setInterval(function(){ _ocultarElQueEsteAbierto();  }, 5000),
			zIndexNumber = 1000,
			isIE = !jQuery.support.cssFloat;
		enlaces.each(function(){
			var $that = $(this);
			_setFocusable($that);
			// arreglo bug z-index IE 7
			if(isIE){
				$(this).css('zIndex', zIndexNumber);
				zIndexNumber -= 10;
			}
		});
		opcionesDirectas.each(function(){
			var $that = $(this),
				ulParent = $that.parents("ul");
			_asignarPerderFoco($that, ulParent);
			_asignarRecibirFoco($that, ulParent);
		});


	}
	return {
		init:init
	};

}();

var avisos = function(){

	var elementoAbierto;
	function _controlVisibilidadTooltip(){
		var tooltips = $(".avisoMsg");
		tooltips.find(":last-child").on("blur", function(){
			var tooltip = $(this).parent();
			_setAriaHidden(tooltip, "true");
			tooltip.hide();
		})
		.on("focus", function(){
			elementoAbierto = $(this).parent();
		});
	}
	function _setKeyBoard(elemento){
		elemento.on("focus",function(){
			var tooltip = $(this).next();
			_setAriaHidden(tooltip, "false");
			tooltip.show();
		})
		.on("blur",function(){
			if(elementoAbierto){
				elementoAbierto.hide();
				_setAriaHidden(elementoAbierto, "true");
				elementoAbierto = null;
			}
		});
	}
	function _setAriaHidden(elemento, estado){
		elemento.attr("aria-hidden", estado);
	}
	function _setHoverEvent(elemento){
		elemento.hover(
			function(){
				var $that = $(this);
				$that.find(".avisoMsg").show();
				_setAriaHidden($that, "false");
			},
			function(){
				var $that = $(this);
				$that.find(".avisoMsg").hide();
				_setAriaHidden($that, "true");
			}
		);
	}
	function init(){
		var enlacesAvisos = $(".aviso > a");
		if (enlacesAvisos.length !== 0){
			enlacesAvisos.each(function(){
				var $that = $(this);
				_setKeyBoard($that); // el teclado lo manejamos con el enlace del bloque
				_setHoverEvent($that.parent()); //el hover lo hacemos con el bloque
			});
			_controlVisibilidadTooltip();

		}
	}
	return {
		init:init
	};

}();


var infos = function(){

	var CLASE_PARA_OCULTAR = "offscreen",
		CLASE_PARA_MOSTRAR = "show", lf, GAP_H = 10, ANCHO_PANTALLA=768;

	function _setKeyBoard(elemento){
		elemento.on("focus",function(){
			var tooltip = $(this).parent().next();
			_setAriaHidden(tooltip, "false");
			tooltip.removeClass(CLASE_PARA_OCULTAR);
			tooltip.addClass(CLASE_PARA_MOSTRAR);
			_setPos(tooltip);
		})
		.on("blur",function(){
			var tooltip = $(this).parent().next();
			_setAriaHidden(tooltip, "true");
			tooltip.addClass(CLASE_PARA_OCULTAR);
			tooltip.removeClass(CLASE_PARA_MOSTRAR);
		});
	}
	function _setAriaHidden(elemento, estado){
		elemento.attr("aria-hidden", estado);
	}
	function _setClickEvent(elemento){
		elemento.on("click", function(){
			var aux = $(this).parent().next();
			aux.removeClass(CLASE_PARA_OCULTAR);
			aux.addClass(CLASE_PARA_MOSTRAR);
			_setAriaHidden(aux, "false");
			_setPos(aux);

			var elem = elemento;
			elem.attr("tabindex", 0);
			elem.focus();

			$(document).on("touchstart", function(){ 
				aux.addClass(CLASE_PARA_OCULTAR);
				aux.removeClass(CLASE_PARA_MOSTRAR);
				_setAriaHidden(aux, "true");
				_setPos(aux);
			});
		})
		.on("blur", function(){
			var aux = $(this).parent().next();
			aux.addClass(CLASE_PARA_OCULTAR);
			aux.removeClass(CLASE_PARA_MOSTRAR);
			_setAriaHidden(aux, "true");
			_setPos(aux);
		});
	}
	function _setHoverEvent(elemento){
		elemento.hover(
			function(){
				var aux = $(this).parent().next();
				aux.removeClass(CLASE_PARA_OCULTAR);
				aux.addClass(CLASE_PARA_MOSTRAR);
				_setAriaHidden(aux, "false");
				_setPos(aux);
			},
			function(){
				var aux = $(this).parent().next();
				aux.addClass(CLASE_PARA_OCULTAR);
				aux.removeClass(CLASE_PARA_MOSTRAR);
				_setAriaHidden(aux, "true");
				_setPos(aux);
			}
		);
	}
	function _setPos(elemento){
		elemento.css("left", lf - GAP_H);
	}
	function _getPos(elemento){
		var offset = elemento.position();
		lf = offset.left;
	}
	function _testEventSelection(elemento){
		var retorno = true;
		var navegador = /iPad|iPhone|Android|Windows Phone/.test(navigator.userAgent);
		return $(window).width() < ANCHO_PANTALLA || elemento.parent().is("h3") || navegador;
	}
	function init(){
		var enlacesAvisos = $(".tooltInf");
		if (enlacesAvisos.length !== 0){
			enlacesAvisos.each(function(){
				var $that = $(this);
				_getPos($that);
				_setKeyBoard($that); // el teclado lo manejamos con el enlace del bloque

				if(_testEventSelection($that)) {
					_setClickEvent($that); //el click lo hacemos con el bloque
				} else {
					_setHoverEvent($that); //el hover lo hacemos con el bloque
				}
			});
		}
	}
	return {
		init:init
	};

}();


var persoProductos = function(){

	var elementoAbierto,  tooltip,
		CLASE_PARA_OCULTAR = "offscreen",
		CLASE_PARA_MOSTRAR = "show",
		MAX_ANCHO_PHONES = 767;

	
	function show(elemento){
		elemento.removeClass(CLASE_PARA_OCULTAR);
		elemento.addClass(CLASE_PARA_MOSTRAR);
		elementoAbierto = true;
	}

	function hide(elemento){
		elemento.addClass(CLASE_PARA_OCULTAR);
		elemento.removeClass(CLASE_PARA_MOSTRAR);
		elementoAbierto = false;
	}

	function _setHoverEvent(elemento){
		elemento.hover(
			function(){
				show(tooltip);
			},
			function(){}
		);
		elemento.on("focus",function(){
			show(tooltip);
		});
	}

	function _setOutEvent(){
		tooltip.hover(
			function(){},
			function(){
				hide(tooltip);
			}
		);
		tooltip.find("li:last-child").on("blur", "a", function(){
			if(tooltip.hasClass(CLASE_PARA_MOSTRAR)){
				hide(tooltip);
			}
		});
	}

	function _ocultarElQueEsteAbierto(){
		if(elementoAbierto){
			hide(tooltip);
		}
	}


	function init(){
		var enlace = $(".misPr > span"), tm;

		tooltip = enlace.next();
		tm = setInterval(function(){ _ocultarElQueEsteAbierto();  }, 3000),
		enlace.attr("tabindex", 0);
		_setHoverEvent(enlace);
		_setOutEvent(enlace);
		
	}
	return {
		init:init
	};

}();

var infoLightbox = function(){

	function init(){
		
		Shadowbox.open({
			content:'33-shadow-box.html',
			player:"iframe",
			height:800
		});
	}
	return {
		init:init
	};

}();

$(document).ready(function(){

	tablasProductos.init();
	accesosDirectos.init();
	avisos.init();
	infos.init();
	slider.init();
	persoProductos.init();

});

$(window).on("touchstart", function(){
	$(this).focus();
})

//window.onload = infoLightbox.init;
