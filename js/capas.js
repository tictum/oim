function ocultaCapas(elemento){
	//$(elemento).css('display', 'none');
	$(elemento).hide();
}

function muestraCapas(elemento){
	//$(elemento).css('display', 'block');
	$(elemento).show();
}

function muestraCapasIB(elemento){
	$(elemento).css('display', 'inline-block');
}

function cambiaVisibilidad(elemento){
	$(elemento).toggle();
}

function corregirIE8(){
	
	var ie8 = /IE 8/.test(navigator.userAgent);

	if(ie8){
		$("body")
			.addClass("prueba")
			.removeClass("prueba");
	}
}

function muestraDetalleTaller(elem){
	var e = $(elem);
	var fila = e.parentsUntil("tbody", "tr");
	var fila_detalle = $(fila).next("tr");
	var filas = $(fila).siblings();

	$.each(filas, function(){
		if($(this).hasClass("seleccionada-detalle")){
			$(this).removeClass("visible");
		} else {
			$(this).removeClass("seleccionada");
		}
	});

	fila.addClass("seleccionada");
	fila_detalle.addClass("visible");

	corregirIE8();
}

function muestraDetalleTallerMobile(elem){
	var e = $(elem);
	var dd = e.parentsUntil("dl", "dd");
	var dt_detalle = $(dd).nextAll("dt.detalle").first();

	var dt_detalles = $(dd).siblings("dt");
	$.each(dt_detalles, function(){
		$(this).removeClass("seleccionada");
	});
	dt_detalle.addClass("seleccionada");
}

function cambiaVisibilidadConChecks(checks, elems){
	var checked = false;
	$.each(checks, function(){
		if($(this).prop("checked")){
			checked = true;
		}
	});
	$.each(elems, function(){
		if(checked){
			$(this).addClass("visible");
		} else {
			$(this).removeClass("visible");
		}
	});
}

function cambiaVisibilidadSituaciones(elem){
	var e = $(elem);
	var fila = e.parentsUntil("tbody", "tr");
	var filas = $(fila).nextUntil(":not(.oculto)");
	var checks = $(fila).find("input[type='checkbox']");

	cambiaVisibilidadConChecks(checks, filas);

	corregirIE8();
}
function cambiaVisibilidadSituacionesMobile(elem){
	var e = $(elem);
	var dd = e.parent();
	var situaciones = $(dd).nextUntil(".sep");

	var checks = $(dd).find("input[type='checkbox']");
	cambiaVisibilidadConChecks(checks, situaciones);
}

function cambiaVisibilidadBoton(checks, elem){
	if ($(checks).prop('checked')){
		$(elem).css('display', 'block');
	}
	else
	{
		$(elem).css('display', 'none');
	}
}


function activarOpcion(activeElement, unActiveElement, className){
	$(activeElement).addClass(className);
	$(unActiveElement).removeClass(className);
	corregirIE8();
}


function enviaEventoSiChecked(element)
{
	if ($(element).prop('checked')){
		$(element).trigger("click");
	}
}
