window.mobilecheck = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
}

function esmovil() {
  var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
  || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4)))
 isMobile = true;
 return isMobile;
}

function esWP(){
  var isWindowsPhone = false;

  if (navigator.userAgent.match(/IEMobile/i)){
    isWindowsPhone=true;
  }
  return isWindowsPhone;
}

function esIE8(){
  var isIE8 = false;

  if (/MSIE 8.0/.test(navigator.userAgent)){
    isIE8=true;
  }
  return isIE8;
}


var logos = function(){
  function init(){

//    if (!jQuery.browser.mobile) {
  if (!window.mobilecheck) {
    _changeLogos();
  }
}
function _changeLogos(){
  $("#MAPFRE").attr("src","/oim/docsmovil/img/MAPFRE.png");
  $("#teCuidamos").attr("src","/oim/docsmovil/img/teCuidamos.png");
    //$("#teCuidamosPA").attr("src","/oim/docsmovil/img/teCuidamos-area-clientes.png");
    $("#w3c").attr("src","/oim/docsmovil/img/W3C-WAI-AA-WCAG-2.0.gif");
    $("#vs").attr("src","/oim/docsmovil/img/verisign-trusted.gif");
    $("#logo").attr("src","/oim/docsmovil/img/MAPFRE-footer.png");
    $("#buzonnum").attr("src","/oim/docsmovil/img/buzon.png");
    $(".avisoMsg .alerta").attr("src","/oim/docsmovil/img/alertaBig.png");
  }
  return {
    init:init
  };
}();


var printPage = function(){
  function init(){
    $("a.imprimir").on("click", function (event) {
      event.preventDefault();
      event.stopPropagation();
      window.print();
    });
  }
  return {
    init:init
  };
}();


var placeholders = function(){
  function init(){
    if($('input, textarea').length>0){
    	$('input, textarea').placeholder();
    }
  }
  return {
    init:init
  };
}();

/*function posicionarShadow() {
  var MAX_ANCHO_PHONES = 480;
  var layerWidth = document.body.clientWidth - 150;
  if (layerWidth < MAX_ANCHO_PHONES) {
    var alto = $('body.pestTC #sb-container #sb-wrapper #sb-wrapper-inner').height() + 200;
    $('body.pestTC #sb-container #sb-wrapper #sb-wrapper-inner').height(alto);
    var top = $('body.pestTC #sb-container #sb-wrapper').attr('style').split(':')[1].split(';')[0].slice(1, -2) - 100;
    var estilos = $('body.pestTC #sb-container #sb-wrapper').attr('style').split(';');
    estilos.shift();
    estilos = estilos.join(';');
    estilos = 'top:'+top+'px;'+estilos;
    $('body.pestTC #sb-container #sb-wrapper').attr('style', estilos);
  }
}*/

var lightbox = function(){

  CLASE_PARA_AVISOS = "avisopopup";
  function init(){
   accessibility_to_shadowbox.init();

   Shadowbox.init({
    player: "iframe",
    enableKeys:false,
    overlayOpacity:0.8,
    onOpen:function(){ accessibility_to_shadowbox.correct(); },
    onFinish:function(){
      var isIphone = /iphone/i.test(navigator.userAgent.toLowerCase());
      if (isIphone) {
        $('#sb-container #sb-body-inner').css('overflow', 'scroll');
        var height = (window.innerHeight?window.innerHeight:document.documentElement&&document.documentElement.clientHeight?document.documentElement.clientHeight:document.body.clientHeight);
        $('#sb-container').height(height);
      }
      if (esmovil()) {
        $('body.pestTC #sb-container #sb-wrapper #sb-wrapper-inner').css({
          'height': '408px',
          'margin-top': '-70px'
        });
        $('body.pestTC').addClass('noscroll');
      }
      accessibility_to_shadowbox.addFunctionsToClose();
    },
    onClose:function(){ $('body.pestTC').removeClass('noscroll'); }
  });

    /*  if($("."+CLASE_PARA_AVISOS).length !== 0){

        Shadowbox.open({
          content:'33-shadow-box.html',
          player:"iframe",
          height:800
        });
      }*/

    }

    return{
      init:init
    };

  }();


  (function () {
   'use strict';
   function parser(headings) {
    var len = headings.length, i, headingTag, nodeName, aria_level;
    for (i = 0; i < len; i += 1) {
     headingTag = headings[i];
     nodeName = headingTag.nodeName;
     aria_level = nodeName.charAt(1);
     headingTag.setAttribute("role", "heading");
     headingTag.setAttribute("aria-level", aria_level);
   }
 }
 function init() {
  /*global document: false */
  var h1 = document.getElementsByTagName("h1"),
  h2 = document.getElementsByTagName("h2"),
  h3 = document.getElementsByTagName("h3"),
  h4 = document.getElementsByTagName("h4"),
  h5 = document.getElementsByTagName("h5"),
  h6 = document.getElementsByTagName("h6");
  parser(h1);
  parser(h2);
  parser(h3);
  parser(h4);
  parser(h5);
  parser(h6);
}
init();
}());

  var tooltips = function(){
    function turnOn(o){
      if (!o.t){
        // store orginal contents as part of object
        o.t=o.title;
        o.c=o.innerHTML;
        // store tooltip build as part of the object
        o.h=o.c+'<span class="tooltip"><span class="ttbox"> '+o.title+'</span><span class="ttarrow"></span></span>';
      }
      if ($(o).css('position')==="relative"){
        o.innerHTML=o.h;
        o.title='';
      }
      $(o).find('span.tooltip').fadeIn('slow');
    }

    function turnOff(o){
     $(o).find('span.tooltip').fadeOut('fast');
     o.innerHTML=o.c;
     o.title=o.t;
   }
   function init(){
      // loop through all objects with a title attribute
      //var titles=$('[title]:not(.notooltip)');
      var titles=$('[title].tooltiplink'),
      len = titles.length,
      is_touch_device = 'ontouchstart' in document.documentElement;
      eventName = is_touch_device ? "touchstart" : "click";
      // use an efficient for loop as there may be a lot to cycle through
      for(var i = len - 1; i >- 1; i--){
         // titled object must be position:relative
         $(titles[i]).addClass('tooltipParent');
         // titled object must appear in the keyboard focus
         $(titles[i]).attr('tabindex','0');
         // mouse & keyboard functions
         $(titles[i])
         .hover(function(){turnOn(this);},function(){turnOff(this);})
         .focus(function(){turnOn(this);});
            //.blur(function(){turnOff(this);});



          }
          $('html').on(eventName, function(){
            $('span.tooltip').fadeOut('fast');
          });
        }
        return {
          init:init
        };
      }();

      var atencionCliente = function(){
        var CLASE_PARA_OCULTAR = "offscreen",
        CLASE_ON = "on",
        CLASE_PARA_CURSOR = "cur";
        function init(){
          var elems = $(".filaRdCh h2"),
          fields = $(".f0,.f1"),
          leyenda = $(".leyenda"), history,
          botonera = $(".filaBoton"), noAbiertos = false,
          obj = $(".atencionCliente");
          elems.attr("tabindex",0);
          elems.addClass(CLASE_PARA_CURSOR);
          elems.on("click keypress", function(event){
            var that = $(this),
            idx = that.attr("data-rel"),
            idxHistory, fieldHistory,
            ariaSel = "aria-selected",
            ariaEx = "aria-expanded",
            ariaHid = "aria-hidden",
            esClick = event.type === 'click',
            esTab = event.keyCode === 9,
            field = fields.eq(idx);
            if (esClick || !esTab) {
              that.toggleClass(CLASE_ON);
              that.attr(ariaSel,getValue(that, ariaSel));
              that.attr(ariaEx,getValue(that, ariaEx));


              if (history && (history.attr("data-rel") != idx)){
                idxHistory = history.attr("data-rel");
                fieldHistory = fields.eq(idxHistory);
                history.removeClass(CLASE_ON);
                history.attr(ariaSel,"false");
                history.attr(ariaEx,"false");
                fieldHistory.addClass(CLASE_PARA_OCULTAR);
                fieldHistory.attr(ariaHid, "true");
                fieldHistory.attr("disabled", "disabled");

              }

              field.toggleClass(CLASE_PARA_OCULTAR);
              field.attr(ariaHid, getValue(field, ariaHid));
              field.removeAttr("disabled");

              noAbiertos = obj.find("[aria-selected='true']").length;

              leyenda.attr(ariaHid, getValue(leyenda, ariaHid));
              botonera.attr(ariaHid, getValue(botonera, ariaHid));
              leyenda.addClass(CLASE_PARA_OCULTAR);
              botonera.addClass(CLASE_PARA_OCULTAR);

              history = that;

            }
            if(noAbiertos){
              leyenda.attr(ariaHid, getValue(leyenda, ariaHid));
              botonera.attr(ariaHid, getValue(botonera, ariaHid));
              leyenda.removeClass(CLASE_PARA_OCULTAR);
              botonera.removeClass(CLASE_PARA_OCULTAR);
            }

          });
        }
        function getValue(elemento, atributo){
          var valor = elemento.attr(atributo);
          return !( valor === "true" );
        }
        return {
          init:init
        };
      }();

      var faqPage = function(){
        var CLASE_PARA_OCULTAR = "offscreen",
        ON = "up";
        function init(){
          var dts, dds,
          obj = $(".faq");
          dts = obj.find("dt, dt label");
          dds = obj.find("dd");

          dts.on("click keypress", function(event){
            if($(this).is("label")){
              toggle($(this).parent().parent(), event);
            } else {
              toggle($(this), event);
            }
          });

          dds.on("click", "a", function(){
            checkForHash($(this));
          });

        }

        function checkForHash(elemento){
          var href = elemento.attr("href");
          if(href.indexOf('#').length !== -1){
            if($(href).attr("role") === "tab"){
              toggle($(href));
            }
          }
        }

        function toggle(elemento, ev){
          var ariaSel = "aria-selected",
          ariaEx = "aria-expanded",
          ariaHid = "aria-hidden",
          esClick = true, esTab = 0,
          dd = elemento.next();
          if(ev){
            esClick = ev.type === 'click';
            esTab = ev.keyCode === 9;
          }

          if (esClick || !esTab) {
            elemento.attr(ariaSel, getValue(elemento, ariaSel));
            elemento.attr(ariaEx, getValue(elemento, ariaEx));
            dd.toggleClass(CLASE_PARA_OCULTAR);
            dd.attr(ariaHid, getValue(dd, ariaHid));
            elemento.toggleClass(ON);
          }

        }

        function getValue(elemento, atributo){
          var valor = elemento.attr(atributo);
          return !( valor === "true" );
        }

        return {
          init:init
        };
      }();

      var operativaAltaPage = function(){
        function init(){
          $('.flexslider').flexslider({
            animation: "fade",
            prevText: "Anterior paso de la operativa de alta",
            nextText: "Siguiente paso de la operativa de alta",
            pausePlay: false,
            controlNav: true,
            manualControls: ".steps li",
            controlsContainer:".flex-container"
          });
        }
        return {
          init:init
        };
      }();

      var solicitudAltaPage = function(){
        var CLASE_PARA_MOSTRAR = "on";
        function init(){
          var obj = $(".solicitud-alta > li");
          obj.attr("tabindex", "0");
          obj.on("focus", function(){
           $(this).addClass(CLASE_PARA_MOSTRAR);
         });
          obj.on("blur", function(){
           $(this).removeClass(CLASE_PARA_MOSTRAR);
         });
        }
        return {
          init:init
        };
      }();

      var acordeonProductos = function(){
        var CLASE_PARA_OCULTAR = "offscreen",
        CLASE_SEL = "up", CLASE_PARA_CURSOR = "cur", CLASE_DESPLEGABLE = "desplegable",
        obj, history, is_touch_device, eventName;
        function init(){

          var elems, history, history_elemento, history_panel;
          obj = $(".gestion-productos");
          elems = obj.find(">ul>li");
          elems.addClass(CLASE_PARA_CURSOR);
          is_touch_device = 'ontouchstart' in document.documentElement;
          eventName = is_touch_device ? "touchstart" : "click";

          elems.on(eventName, function(event){

            var ariaSel = "aria-selected",
            ariaEx = "aria-expanded",
            ariaHid = "aria-hidden",
            esClick = event.type === 'click',
            esTab = event.keyCode === 9,
            that = $(this),
            elemento = that.find("h3"),
            panel = elemento.next(),
            elementoParentt = elemento.parent(),
            panelAriaVal = getValue(panel, ariaHid);
            event.stopPropagation();

            if (esClick || !esTab) {
              elemento.attr(ariaSel, getValue(elemento, ariaSel));
              elemento.attr(ariaEx, getValue(elemento, ariaEx));

              if(history){
                history_elemento = history.find("h3");
                history_panel = history_elemento.next();
                history_panel.addClass(CLASE_PARA_OCULTAR);
                history_panel.removeClass(CLASE_DESPLEGABLE);
                history_panel.attr(ariaHid, getValue(history_panel, ariaHid));
                history.removeClass(CLASE_SEL);
                history_elemento.attr(ariaSel, getValue(history_elemento, ariaSel));
                history_elemento.attr(ariaEx, getValue(history_elemento, ariaEx));
              }
              panel.attr(ariaHid, panelAriaVal);

              if(!panelAriaVal){
                panel.addClass(CLASE_DESPLEGABLE);
                panel.removeClass(CLASE_PARA_OCULTAR);
                elementoParentt.addClass(CLASE_SEL);
              }else{
                panel.removeClass(CLASE_DESPLEGABLE);
                panel.addClass(CLASE_PARA_OCULTAR);
                elementoParentt.removeClass(CLASE_SEL);
              }
              history = that;
            }
          });


          $('html').on(eventName, function(event){
            cerrarAcordeon(elems);

          });

        }
        function cerrarAcordeon(elementos){
          var elemento,
          elementoSel = obj.find("."+CLASE_SEL),
          panel, $that,
          ariaSel = "aria-selected",
          ariaEx = "aria-expanded",
          ariaHid = "aria-hidden";

          elementoSel.removeClass(CLASE_SEL);
          elemento = elementoSel.find("h3");
          panel = elemento.next();
          panel.addClass(CLASE_PARA_OCULTAR);
          panel.removeClass(CLASE_DESPLEGABLE);
          panel.attr(ariaHid, getValue(panel, ariaHid));
          elemento.attr(ariaSel, getValue(elemento, ariaSel));
          elemento.attr(ariaEx, getValue(elemento, ariaEx));

        }
        function getValue(elemento, atributo){
          var valor = elemento.attr(atributo);
          return !( valor === "true" );
        }
        return{
          init:init
        };
      }();

      var calendar = function(){
        function init(){
          var dp1 = new Datepicker('dp1', ['diaN', 'mesN', 'anyoN'], true, true);

          $('#btn_date').click(function(e) {
            dp1.showDlg();
            e.preventDefault();
            e.stopPropagation();
          });
          var dp2 = new Datepicker('dp2', ['diaN_h', 'mesN_h', 'anyoN_h'], true, true);
          $('#btn_date_h').click(function(e) {
            dp2.showDlg();
            e.preventDefault();
            e.stopPropagation();
          });

        }
        return {
          init:init
        };
      }();


      var acordeon = function(){
        var CLASE_PARA_OCULTAR = "offscreen",
        CLASE_ON = "on",
        CLASE_PARA_CURSOR = "cur";
        function init(){
          var obj = $(".accordion"),
          elems = obj.find("[aria-expanded]"),
          fields = obj.find("[role='tabpanel']"),
          history, noAbiertos = false;

          elems.attr("tabindex",0);
          elems.addClass(CLASE_PARA_CURSOR);

          if(obj.hasClass("accordion-radio")){
            var labels = obj.find("[aria-expanded] label");
            labels.on("click keypress", function(){
              $(this).parent().trigger("click");
            });

            var headers = obj.find("[aria-expanded]");
            headers.on("click keypress", function(event){
              $(this).children("input[type=radio]").attr("checked", true);

              if($(this).attr("aria-expanded") == "true"){
                event.stopImmediatePropagation();
              }

            });
          }

          elems.on("click keypress", function(event){
            var that = $(this),
            idx = that.attr("data-rel"),
            idxHistory, fieldHistory,
            ariaSel = "aria-selected",
            ariaEx = "aria-expanded",
            ariaHid = "aria-hidden",
            esClick = event.type === 'click',
            esTab = event.keyCode === 9,
            field = fields.eq(idx);

            if (esClick || !esTab) {
              that.toggleClass(CLASE_ON);
              that.attr(ariaSel,getValue(that, ariaSel));
              that.attr(ariaEx,getValue(that, ariaEx));


              if (history && (history.attr("data-rel") != idx)){
                idxHistory = history.attr("data-rel");
                fieldHistory = fields.eq(idxHistory);
                history.removeClass(CLASE_ON);
                history.attr(ariaSel,"false");
                history.attr(ariaEx,"false");
                fieldHistory.addClass(CLASE_PARA_OCULTAR);
                fieldHistory.attr(ariaHid, "true");

              }

              field.toggleClass(CLASE_PARA_OCULTAR);
              field.attr(ariaHid, getValue(field, ariaHid));

              noAbiertos = obj.find(" [aria-selected='true']").length;

              history = that;

            }


          });
        }
        function getValue(elemento, atributo){
          var valor = elemento.attr(atributo);
          return !( valor === "true" );
        }
        return {
          init:init
        };
      }();

      var camposDependientes = function(){
       var CLASE_PARA_OCULTAR = "offscreen";
       function init(){
        var obj = $(".cmbDepend");

        obj.bind("change", function(){
          var $that = $(this),
          id = $that.attr("id"),
          optionSelected = $("option:selected", $that),
          rel = $("."+id);

          if(optionSelected.data("trigger")){
            rel.removeClass(CLASE_PARA_OCULTAR);
            rel.attr("data-hidden", false);
          }else{
            rel.addClass(CLASE_PARA_OCULTAR);
            rel.attr("data-hidden", true);
          }


        });
      }

      return {
        init:init
      };
    }();

    var responsive = function() {
      var CLASE_PARA_MOSTRAR = 'on';
      function navigation(){
        mainMenu();
        optionsMenu();
      }
      function mainMenu(){
        var obj = $('.wrapNav div'),
        nav = obj.find('nav');

        obj.on('click', function(event){
          event.stopPropagation();
          nav.addClass(CLASE_PARA_MOSTRAR);
        });
        hide(nav);
      }
      function optionsMenu(){
        var obj = $('#opciones'),
        nav = obj.next();
        obj.on('click', function(event){
          event.stopPropagation();
          nav.addClass(CLASE_PARA_MOSTRAR);
        });
        hide(nav);
      }
      function hide(cual){
        $(document).on('click', function(event){
          cual.removeClass(CLASE_PARA_MOSTRAR);
        });
      }
      return {
        navigation:navigation
      };

    }();



    function getValoresTeclas(teclasPorFila) {
      var min = ['0','1','2','3','4','5','6','7','8','9','q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','ñ','z','x','c','v','b','n','m'];
      var may = ['0','1','2','3','4','5','6','7','8','9','Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L','Ñ','Z','X','C','V','B','N','M'];

      var teclas = {'default':[], 'shift':[]};

      for(var j, x, i = min.length; i; j = parseInt(Math.random() * i), x = min[--i], min[i] = min[j], min[j] = x);
        for(var j, x, i = may.length; i; j = parseInt(Math.random() * i), x = may[--i], may[i] = may[j], may[j] = x);

          var aux = '';
        var contadorFila = 0;

        for(var k = 0; k < min.length; k++){
          aux = aux + ' ' + min[k];
          if(((k+1) % teclasPorFila == 0)) {
            teclas['default'][contadorFila++] = aux;
            aux = '';
          }
        }
        if(aux) {
          teclas['default'][contadorFila++] = '{shift} ' + aux + ' {shift}';
          if($(window).width() > 480) {
            teclas['default'][contadorFila++] = '{bksp} {space} {clear}';
          } else {
            teclas['default'][contadorFila++] = '{space}';
            teclas['default'][contadorFila++] = '{bksp} {clear}';
          }
        } else {}

        var aux = '';
        var contadorFila = 0;
        for(var k = 0; k < may.length; k++){
          aux = aux + ' ' + may[k];
          if(((k+1) % teclasPorFila == 0)) {
            teclas['shift'][contadorFila++] = aux;
            aux = '';
          }
        }
        if(aux) {
          teclas['shift'][contadorFila++] = '{shift} ' + aux + ' {shift}';
          if($(window).width() > 480) {
            teclas['shift'][contadorFila++] = '{bksp} {space} {clear}';
          } else {
            teclas['shift'][contadorFila++] = '{space}';
            teclas['shift'][contadorFila++] = '{bksp} {clear}';
          }
        } else {}

        return teclas;
      }


      function pintaTeclado(elem) {

        var objKeyboardBase = {
          alwaysOpen: true,
          appendTo: $("#kbLayer"),
          display : {
            'accept': 'Aceptar',
            'cancel': 'Cancelar',
            'bksp': 'Borrar',
            'clear': 'Cancelar'
          },
          layout : 'custom',
          maxLength: 8,
          position: {
            of: $("#kbLayer")
          },
          usePreview: false
        }

        var objKeyboard = objKeyboardBase;

        if($(window).width() > 480){
          objKeyboard['customLayout'] = getValoresTeclas(10);
        } else {
          objKeyboard['customLayout'] = getValoresTeclas(5);
        }

        $("#"+elem).keyboard(objKeyboard);

        $('#'+elem).getkeyboard().reveal(); 
      }

      function reordenaTeclas(keyboard, elem){
        keyboard.destroy();
        pintaTeclado(elem);
      }


      function moveCursorToEnd(el) {
        if (typeof el.selectionStart == "number") {
          el.selectionStart = el.selectionEnd = el.value.length;
        } else if (typeof el.createTextRange != "undefined") {
          el.focus();
          var range = el.createTextRange();
          range.collapse(false);
          range.select();
        }
      }

/*
$(window).on('change.keyboard', function(e, keyboard, el){
  if(/^[a-z0-9ñ]{1}$/i.test(keyboard.lastKey)){
    reordenaTeclas(keyboard, el.id);
    moveCursorToEnd(el);
  }
});
*/

// detecta alto de ventana

function heightScreen() {
  var myHeight = 0;
  if( typeof( window.innerWidth ) == 'number' ) {
    //No-IE
    myHeight = window.innerHeight;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    //IE 6+
    myHeight = document.documentElement.clientHeight;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //IE 4 compatible
    myHeight = document.body.clientHeight;
  }
  return myHeight;
}

// detectar ancho ventana

function widthScreen() {
  var myWidth = 0;
  if( typeof( window.innerWidth ) == 'number' ) {
    //No-IE
    myWidth = window.innerWidth;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    //IE 6+
    myWidth = document.documentElement.clientWidth;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //IE 4 compatible
    myWidth = document.body.clientWidth;
  }
  return myWidth; 
} 


// fancybox modal popup init

function getDimensionLightbox(rel, dimension){
  if(!rel){
    return 500;
  }
  var tmp = rel.split(',');
  for(var i = 0; i < tmp.length; i++){
    if(tmp[i].indexOf(dimension) >= 0){
      return tmp[i].split(":")[1];
    }
  }
  return 500;
}

function initLightbox() {
  var win = jQuery(window);
  jQuery('a.lightbox').each(function(){
    var link = jQuery(this);
    link.fancybox({
      padding: 0,
      margin: 0,
      cyclic: false,
      autoScale: false,
      overlayShow: true,
      overlayOpacity: 0.8,
      overlayColor: '#333333',
      titlePosition: 'inside',
      titleShow: false,
      height: getDimensionLightbox(link.attr("data-dimension"), "height"),
      width: getDimensionLightbox(link.attr("data-dimension"), "width"),
      autoDimensions: false,

      openIframe: link.hasClass("iframe"),

      onComplete: function(box) {
        if(link.attr('href').indexOf('#') === 0) {
          jQuery('#fancybox-content').find('.btn-close, .btn-cancel, .lightbox-close').unbind('click.fb').bind('click.fb', function(e){
            jQuery.fancybox.close();
            e.preventDefault();
          });
        }
        resizeLightbox();
        win.bind('resize orientationchange', resizeLightbox);
        
        if(this.openIframe){
          var myHeight = heightScreen();
          var myWidth = widthScreen();
          if (esmovil()) {
            $("#fancybox-frame").css("height", myHeight+"px");
          } else {
            $("#fancybox-frame").css("max-height", (myHeight-80)+"px");
            $("#fancybox-frame").css("height", this.height+"px");
          }
          $("#fancybox-content iframe").css("max-width", myWidth+"px");
          $("#fancybox-content iframe").css("width", this.width+"px");

        }
        jQuery.fancybox.center();
      },
      onClosed: function(){
        win.unbind('resize orientationchange', resizeLightbox);
      },
      onStart: function(){
        $.fancybox.center();
      }
    });
  });
  function resizeLightbox(){
    jQuery('#fancybox-content, #fancybox-wrap').css({
      width:'auto',
      height: 'auto'
    });
    if(esIE8()){
      $('#fancybox-outer, #fancybox-frame, #fancybox-wrap' ).css('width', '600px'); 
    }
    if (window.innerWidth>=768&&!esWP()) {
     $('#fancybox-outer, #fancybox-frame').css('height', '400px'); 
   }
   if (window.innerWidth<=480&&esWP()) {
     $('#fancybox-outer, #fancybox-frame').css('width', '300px'); 
     $('#fancybox-outer, #fancybox-frame').css('height', '900px'); 
   }

   var isIphone = /iphone/i.test(navigator.userAgent.toLowerCase());
   if (esmovil() && isIphone && !esWP() && $('#fancybox-wrap').offset().top > 500) {
    var posicion = $(document).scrollTop() - 200;
    $("html, body").animate({
      scrollTop: posicion
    }, 0);

    var alto = $('#fancybox-wrap').css('top').slice(0, -2) - 200;
    $('#fancybox-wrap').css({
      top: alto+'px'
    });
    $('#fancybox-wrap').addClass('altoiphone');
  }


  if (esWP()) {
    var altocontenido = $('#fancybox-frame').height() + 33;
    $('#fancybox-outer, #fancybox-frame').css('height', altocontenido+'px');
    $('#fancybox-outer, #fancybox-frame').css({
      background: 'none',
      overflow: 'hidden'
    });
  }
  jQuery.fancybox.resize();
  jQuery.fancybox.center();


  /* Evitar que en movil se vaya hacia arriba */
  if (esmovil()) {
    $("#fancybox-wrap").css("margin-top", "20px");
  }
}
}

/* Fancybox overlay fix */
jQuery(function(){
  // detect device type
  var isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
  var isWinPhoneDevice = /Windows Phone/.test(navigator.userAgent);

  // create stylesheet
  var styleTag = $('<style type="text/css">')[0];

  // crossbrowser style handling
  var addCSSRule = function(selector, rules) {
    var rule = selector + '{' + rules + '}';
    if (styleTag.styleSheet) {
      styleTag.styleSheet.cssText = rule;
    } else {
      styleTag.appendChild(document.createTextNode(rule));
    }
  };

  if(isTouchDevice || isWinPhoneDevice) {
    // custom positioning for mobile devices
    setTimeout(function() {
      var page = jQuery('body'),
      fancySideShadows = jQuery('#fancybox-bg-ne, #fancybox-bg-e, #fancybox-bg-se'),
      fancyOverlay = jQuery('#fancybox-overlay'),
      fancyWrap = jQuery('#fancybox-wrap');

      // override position handler
      jQuery.fancybox.center = function() {
        if(fancyWrap.innerWidth() > fancyOverlay.innerWidth()) {
          fancyWrap.css({padding: 0});
          fancySideShadows.css({display: 'none'});
        } else {
          fancyWrap.css({padding: ''});
          fancySideShadows.css({display: ''});
        }
        fancyWrap.css({
          left: (page.innerWidth() - fancyWrap.innerWidth()) / 2
        });
      };
      jQuery(window).bind('resize orientationchange', function() {
        jQuery.fancybox.center();
      });
    }, 1);
  } else {
    // use position fixed for desktop
    addCSSRule('#fancybox-overlay', 'position:fixed;top:0;left:0');
  }

  $('head').append(styleTag);
});

/*
 * FancyBox - jQuery Plugin
 * Simple and fancy lightbox alternative
 *
 * Examples and documentation at: http://fancybox.net
 * 
 * Copyright (c) 2008 - 2010 Janis Skarnelis
 * That said, it is hardly a one-person project. Many people have submitted bugs, code, and offered their advice freely. Their support is greatly appreciated.
 *
 * Version: 1.3.4 (11/11/2010)
 * Requires: jQuery v1.3+
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */
 ;(function(B){var L,T,Q,M,d,m,J,A,O,z,C=0,H={},j=[],e=0,G={},y=[],f=null,o=new Image(),i=/\.(jpg|gif|png|bmp|jpeg)(.*)?$/i,k=/[^\.]\.(swf)\s*$/i,p,N=1,h=0,t="",b,c,P=false,s=B.extend(B("<div/>")[0],{prop:0}),S=/MSIE 6/.test(navigator.userAgent)&&B.browser.version<7&&!window.XMLHttpRequest,r=function(){T.hide();o.onerror=o.onload=null;if(f){f.abort()}L.empty()},x=function(){if(false===H.onError(j,C,H)){T.hide();P=false;return}H.titleShow=false;H.width="auto";H.height="auto";L.html('<p id="fancybox-error">The requested content cannot be loaded.<br />Please try again later.</p>');n()},w=function(){var Z=j[C],W,Y,ab,aa,V,X;r();H=B.extend({},B.fn.fancybox.defaults,(typeof B(Z).data("fancybox")=="undefined"?H:B(Z).data("fancybox")));X=H.onStart(j,C,H);if(X===false){P=false;return}else{if(typeof X=="object"){H=B.extend(H,X)}}ab=H.title||(Z.nodeName?B(Z).attr("title"):Z.title)||"";if(Z.nodeName&&!H.orig){H.orig=B(Z).children("img:first").length?B(Z).children("img:first"):B(Z)}if(ab===""&&H.orig&&H.titleFromAlt){ab=H.orig.attr("alt")}W=H.href||(Z.nodeName?B(Z).attr("href"):Z.href)||null;if((/^(?:javascript)/i).test(W)||W=="#"){W=null}if(H.type){Y=H.type;if(!W){W=H.content}}else{if(H.content){Y="html"}else{if(W){if(W.match(i)){Y="image"}else{if(W.match(k)){Y="swf"}else{if(B(Z).hasClass("iframe")){Y="iframe"}else{if(W.indexOf("#")===0){Y="inline"}else{Y="ajax"}}}}}}}if(!Y){x();return}if(Y=="inline"){Z=W.substr(W.indexOf("#"));Y=B(Z).length>0?"inline":"ajax"}H.type=Y;H.href=W;H.title=ab;if(H.autoDimensions){if(H.type=="html"||H.type=="inline"||H.type=="ajax"){H.width="auto";H.height="auto"}else{H.autoDimensions=false}}if(H.modal){H.overlayShow=true;H.hideOnOverlayClick=false;H.hideOnContentClick=false;H.enableEscapeButton=false;H.showCloseButton=false}H.padding=parseInt(H.padding,10);H.margin=parseInt(H.margin,10);L.css("padding",(H.padding+H.margin));B(".fancybox-inline-tmp").unbind("fancybox-cancel").bind("fancybox-change",function(){B(this).replaceWith(m.children())});switch(Y){case"html":L.html(H.content);n();break;case"inline":if(B(Z).parent().is("#fancybox-content")===true){P=false;return}B('<div class="fancybox-inline-tmp" />').hide().insertBefore(B(Z)).bind("fancybox-cleanup",function(){B(this).replaceWith(m.children())}).bind("fancybox-cancel",function(){B(this).replaceWith(L.children())});B(Z).appendTo(L);n();break;case"image":P=false;B.fancybox.showActivity();o=new Image();o.onerror=function(){x()};o.onload=function(){P=true;o.onerror=o.onload=null;F()};o.src=W;break;case"swf":H.scrolling="no";aa='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="'+H.width+'" height="'+H.height+'"><param name="movie" value="'+W+'"></param>';V="";B.each(H.swf,function(ac,ad){aa+='<param name="'+ac+'" value="'+ad+'"></param>';V+=" "+ac+'="'+ad+'"'});aa+='<embed src="'+W+'" type="application/x-shockwave-flash" width="'+H.width+'" height="'+H.height+'"'+V+"></embed></object>";L.html(aa);n();break;case"ajax":P=false;B.fancybox.showActivity();H.ajax.win=H.ajax.success;f=B.ajax(B.extend({},H.ajax,{url:W,data:H.ajax.data||{},dataType:"text",error:function(ac,ae,ad){if(ac.status>0){x()}},success:function(ad,af,ac){var ae=typeof ac=="object"?ac:f;if(ae.status==200||ae.status===0){if(typeof H.ajax.win=="function"){X=H.ajax.win(W,ad,af,ac);if(X===false){T.hide();return}else{if(typeof X=="string"||typeof X=="object"){ad=X}}}L.html(ad);n()}}}));break;case"iframe":E();break}},n=function(){var V=H.width,W=H.height;if(V.toString().indexOf("%")>-1){V=parseInt((B(window).width()-(H.margin*2))*parseFloat(V)/100,10)+"px"}else{V=V=="auto"?"auto":V+"px"}if(W.toString().indexOf("%")>-1){W=parseInt((B(window).height()-(H.margin*2))*parseFloat(W)/100,10)+"px"}else{W=W=="auto"?"auto":W+"px"}L.wrapInner('<div style="width:'+V+";height:"+W+";overflow: "+(H.scrolling=="auto"?"auto":(H.scrolling=="yes"?"scroll":"hidden"))+';position:relative;"></div>');H.width=L.width();H.height=L.height();E()},F=function(){H.width=o.width;H.height=o.height;B("<img />").attr({id:"fancybox-img",src:o.src,alt:H.title}).appendTo(L);E()},E=function(){var W,V;T.hide();if(M.is(":visible")&&false===G.onCleanup(y,e,G)){B('.fancybox-inline-tmp').trigger('fancybox-cancel');P=false;return}P=true;B(m.add(Q)).unbind();B(window).unbind("resize.fb scroll.fb");B(document).unbind("keydown.fb");if(M.is(":visible")&&G.titlePosition!=="outside"){M.css("height",M.height())}y=j;e=C;G=H;if(G.overlayShow){Q.css({"background-color":G.overlayColor,opacity:G.overlayOpacity,cursor:G.hideOnOverlayClick?"pointer":"auto",height:B(document).height()});if(!Q.is(":visible")){if(S){B("select:not(#fancybox-tmp select)").filter(function(){return this.style.visibility!=="hidden"}).css({visibility:"hidden"}).one("fancybox-cleanup",function(){this.style.visibility="inherit"})}Q.show()}}else{Q.hide()}c=R();l();if(M.is(":visible")){B(J.add(O).add(z)).hide();W=M.position(),b={top:W.top,left:W.left,width:M.width(),height:M.height()};V=(b.width==c.width&&b.height==c.height);m.fadeTo(G.changeFade,0.3,function(){var X=function(){m.html(L.contents()).fadeTo(G.changeFade,1,v)};B('.fancybox-inline-tmp').trigger('fancybox-change');m.empty().removeAttr("filter").css({"border-width":G.padding,width:c.width-G.padding*2,height:H.autoDimensions?"auto":c.height-h-G.padding*2});if(V){X()}else{s.prop=0;B(s).animate({prop:1},{duration:G.changeSpeed,easing:G.easingChange,step:U,complete:X})}});return}M.removeAttr("style");m.css("border-width",G.padding);if(G.transitionIn=="elastic"){b=I();m.html(L.contents());M.show();if(G.opacity){c.opacity=0}s.prop=0;B(s).animate({prop:1},{duration:G.speedIn,easing:G.easingIn,step:U,complete:v});return}if(G.titlePosition=="inside"&&h>0){A.show()}m.css({width:c.width-G.padding*2,height:H.autoDimensions?"auto":c.height-h-G.padding*2}).html(L.contents());M.css(c).fadeIn(G.transitionIn=="none"?0:G.speedIn,v)},D=function(V){if(V&&V.length){if(G.titlePosition=="float"){return'<table id="fancybox-title-float-wrap" cellpadding="0" cellspacing="0"><tr><td id="fancybox-title-float-left"></td><td id="fancybox-title-float-main">'+V+'</td><td id="fancybox-title-float-right"></td></tr></table>'}return'<div id="fancybox-title-'+G.titlePosition+'">'+V+"</div>"}return false},l=function(){t=G.title||"";h=0;A.empty().removeAttr("style").removeClass();if(G.titleShow===false){A.hide();return}t=B.isFunction(G.titleFormat)?G.titleFormat(t,y,e,G):D(t);if(!t||t===""){A.hide();return}A.addClass("fancybox-title-"+G.titlePosition).html(t).appendTo("body").show();switch(G.titlePosition){case"inside":A.css({width:c.width-(G.padding*2),marginLeft:G.padding,marginRight:G.padding});h=A.outerHeight(true);A.appendTo(d);c.height+=h;break;case"over":A.css({marginLeft:G.padding,width:c.width-(G.padding*2),bottom:G.padding}).appendTo(d);break;case"float":A.css("left",parseInt((A.width()-c.width-40)/2,10)*-1).appendTo(M);break;default:A.css({width:c.width-(G.padding*2),paddingLeft:G.padding,paddingRight:G.padding}).appendTo(M);break}A.hide()},g=function(){if(G.enableEscapeButton||G.enableKeyboardNav){B(document).bind("keydown.fb",function(V){if(V.keyCode==27&&G.enableEscapeButton){V.preventDefault();B.fancybox.close()}else{if((V.keyCode==37||V.keyCode==39)&&G.enableKeyboardNav&&V.target.tagName!=="INPUT"&&V.target.tagName!=="TEXTAREA"&&V.target.tagName!=="SELECT"){V.preventDefault();B.fancybox[V.keyCode==37?"prev":"next"]()}}})}if(!G.showNavArrows){O.hide();z.hide();return}if((G.cyclic&&y.length>1)||e!==0){O.show()}if((G.cyclic&&y.length>1)||e!=(y.length-1)){z.show()}},v=function(){if(B.support.opacity===false){m.get(0).style.removeAttribute("filter");M.get(0).style.removeAttribute("filter")}if(H.autoDimensions){m.css("height","auto")}M.css("height","auto");if(t&&t.length){A.show()}if(G.showCloseButton){J.show()}g();if(G.hideOnContentClick){m.bind("click",B.fancybox.close)}if(G.hideOnOverlayClick){Q.bind("click",B.fancybox.close)}B(window).bind("resize.fb",B.fancybox.resize);if(G.centerOnScroll){B(window).bind("scroll.fb",B.fancybox.center)}if(G.type=="iframe"){B('<iframe id="fancybox-frame" name="fancybox-frame'+new Date().getTime()+'" frameborder="0" hspace="0" '+(window.attachEvent?'allowtransparency="true""':"")+' scrolling="'+H.scrolling+'" src="'+G.href+'"></iframe>').appendTo(m)}M.show();P=false;B.fancybox.center();G.onComplete(y,e,G);K()},K=function(){var V,W;if((y.length-1)>e){V=y[e+1].href;if(typeof V!=="undefined"&&V.match(i)){W=new Image();W.src=V}}if(e>0){V=y[e-1].href;if(typeof V!=="undefined"&&V.match(i)){W=new Image();W.src=V}}},U=function(W){var V={width:parseInt(b.width+(c.width-b.width)*W,10),height:parseInt(b.height+(c.height-b.height)*W,10),top:parseInt(b.top+(c.top-b.top)*W,10),left:parseInt(b.left+(c.left-b.left)*W,10)};if(typeof c.opacity!=="undefined"){V.opacity=W<0.5?0.5:W}M.css(V);m.css({width:V.width-G.padding*2,height:V.height-(h*W)-G.padding*2})},u=function(){return[B(window).width()-(G.margin*2),B(window).height()-(G.margin*2),B(document).scrollLeft()+G.margin,document.documentElement.scrollTop || B(document).scrollTop()+G.margin]},R=function(){var V=u(),Z={},W=G.autoScale,X=G.padding*2,Y;if(G.width.toString().indexOf("%")>-1){Z.width=parseInt((V[0]*parseFloat(G.width))/100,10)}else{Z.width=G.width+X}if(G.height.toString().indexOf("%")>-1){Z.height=parseInt((V[1]*parseFloat(G.height))/100,10)}else{Z.height=G.height+X}if(W&&(Z.width>V[0]||Z.height>V[1])){if(H.type=="image"||H.type=="swf"){Y=(G.width)/(G.height);if((Z.width)>V[0]){Z.width=V[0];Z.height=parseInt(((Z.width-X)/Y)+X,10)}if((Z.height)>V[1]){Z.height=V[1];Z.width=parseInt(((Z.height-X)*Y)+X,10)}}else{Z.width=Math.min(Z.width,V[0]);Z.height=Math.min(Z.height,V[1])}}Z.top=parseInt(Math.max(V[3]-20,V[3]+((V[1]-Z.height-40)*0.5)),10);Z.left=parseInt(Math.max(V[2]-20,V[2]+((V[0]-Z.width-40)*0.5)),10);return Z},q=function(V){var W=V.offset();W.top+=parseInt(V.css("paddingTop"),10)||0;W.left+=parseInt(V.css("paddingLeft"),10)||0;W.top+=parseInt(V.css("border-top-width"),10)||0;W.left+=parseInt(V.css("border-left-width"),10)||0;W.width=V.width();W.height=V.height();return W},I=function(){var Y=H.orig?B(H.orig):false,X={},W,V;if(Y&&Y.length){W=q(Y);X={width:W.width+(G.padding*2),height:W.height+(G.padding*2),top:W.top-G.padding-20,left:W.left-G.padding-20}}else{V=u();X={width:G.padding*2,height:G.padding*2,top:parseInt(V[3]+V[1]*0.5,10),left:parseInt(V[2]+V[0]*0.5,10)}}return X},a=function(){if(!T.is(":visible")){clearInterval(p);return}B("div",T).css("top",(N*-40)+"px");N=(N+1)%12};B.fn.fancybox=function(V){if(!B(this).length){return this}B(this).data("fancybox",B.extend({},V,(B.metadata?B(this).metadata():{}))).unbind("click.fb").bind("click.fb",function(X){X.preventDefault();if(P){return}P=true;B(this).blur();j=[];C=0;var W=B(this).attr("rel")||"";if(!W||W==""||W==="nofollow"){j.push(this)}else{j=B('a[rel="'+W+'"], area[rel="'+W+'"]');C=j.index(this)}w();return});return this};B.fancybox=function(Y){var X;if(P){return}P=true;X=typeof arguments[1]!=="undefined"?arguments[1]:{};j=[];C=parseInt(X.index,10)||0;if(B.isArray(Y)){for(var W=0,V=Y.length;W<V;W++){if(typeof Y[W]=="object"){B(Y[W]).data("fancybox",B.extend({},X,Y[W]))}else{Y[W]=B({}).data("fancybox",B.extend({content:Y[W]},X))}}j=jQuery.merge(j,Y)}else{if(typeof Y=="object"){B(Y).data("fancybox",B.extend({},X,Y))}else{Y=B({}).data("fancybox",B.extend({content:Y},X))}j.push(Y)}if(C>j.length||C<0){C=0}w()};B.fancybox.showActivity=function(){clearInterval(p);T.show();p=setInterval(a,66)};B.fancybox.hideActivity=function(){T.hide()};B.fancybox.next=function(){return B.fancybox.pos(e+1)};B.fancybox.prev=function(){return B.fancybox.pos(e-1)};B.fancybox.pos=function(V){if(P){return}V=parseInt(V);j=y;if(V>-1&&V<y.length){C=V;w()}else{if(G.cyclic&&y.length>1){C=V>=y.length?0:y.length-1;w()}}return};B.fancybox.cancel=function(){if(P){return}P=true;B('.fancybox-inline-tmp').trigger('fancybox-cancel');r();H.onCancel(j,C,H);P=false};B.fancybox.close=function(){if(P||M.is(":hidden")){return}P=true;if(G&&false===G.onCleanup(y,e,G)){P=false;return}r();B(J.add(O).add(z)).hide();B(m.add(Q)).unbind();B(window).unbind("resize.fb scroll.fb");B(document).unbind("keydown.fb");if(G.type==="iframe"){m.find("iframe").attr("src",S&&/^https/i.test(window.location.href||"")?"javascript:void(false)":"about:blank")}if(G.titlePosition!=="inside"){A.empty()}M.stop();function V(){Q.fadeOut("fast");A.empty().hide();M.hide();B('.fancybox-inline-tmp').trigger('fancybox-cleanup');m.empty();G.onClosed(y,e,G);y=H=[];e=C=0;G=H={};P=false}if(G.transitionOut=="elastic"){b=I();var W=M.position();c={top:W.top,left:W.left,width:M.width(),height:M.height()};if(G.opacity){c.opacity=1}A.empty().hide();s.prop=1;B(s).animate({prop:0},{duration:G.speedOut,easing:G.easingOut,step:U,complete:V})}else{M.fadeOut(G.transitionOut=="none"?0:G.speedOut,V)}};B.fancybox.resize=function(){if(Q.is(":visible")){Q.css("height",B(document).height())}B.fancybox.center(true)};B.fancybox.center=function(){var V,W;if(P){return}W=arguments[0]===true?1:0;V=u();if(!W&&(M.width()>V[0]||M.height()>V[1])){return}M.stop().animate({top:parseInt(Math.max(V[3]-20,V[3]+((V[1]-m.height()-40)*0.5)-G.padding)),left:parseInt(Math.max(V[2]-20,V[2]+((V[0]-m.width()-40)*0.5)-G.padding))},typeof arguments[0]=="number"?arguments[0]:0)};B.fancybox.init=function(){if(B("#fancybox-wrap").length){return}B("body").append(L=B('<div id="fancybox-tmp"></div>'),T=B('<div id="fancybox-loading"><div></div></div>'),Q=B('<div id="fancybox-overlay"></div>'),M=B('<div id="fancybox-wrap"></div>'));d=B('<div id="fancybox-outer"></div>').append('<div class="fancybox-bg" id="fancybox-bg-n"></div><div class="fancybox-bg" id="fancybox-bg-ne"></div><div class="fancybox-bg" id="fancybox-bg-e"></div><div class="fancybox-bg" id="fancybox-bg-se"></div><div class="fancybox-bg" id="fancybox-bg-s"></div><div class="fancybox-bg" id="fancybox-bg-sw"></div><div class="fancybox-bg" id="fancybox-bg-w"></div><div class="fancybox-bg" id="fancybox-bg-nw"></div>').appendTo(M);d.append(m=B('<div id="fancybox-content"></div>'),J=B('<a id="fancybox-close"></a>'),A=B('<div id="fancybox-title"></div>'),O=B('<a href="javascript:;" id="fancybox-left"><span class="fancy-ico" id="fancybox-left-ico"></span></a>'),z=B('<a href="javascript:;" id="fancybox-right"><span class="fancy-ico" id="fancybox-right-ico"></span></a>'));J.click(B.fancybox.close);T.click(B.fancybox.cancel);O.click(function(V){V.preventDefault();B.fancybox.prev()});z.click(function(V){V.preventDefault();B.fancybox.next()});if(B.fn.mousewheel){M.bind("mousewheel.fb",function(V,W){if(P){V.preventDefault()}else{if(B(V.target).get(0).clientHeight==0||B(V.target).get(0).scrollHeight===B(V.target).get(0).clientHeight){V.preventDefault();B.fancybox[W>0?"prev":"next"]()}}})}if(B.support.opacity===false){M.addClass("fancybox-ie")}if(S){T.addClass("fancybox-ie6");M.addClass("fancybox-ie6");B('<iframe id="fancybox-hide-sel-frame" src="'+(/^https/i.test(window.location.href||"")?"javascript:void(false)":"about:blank")+'" scrolling="no" border="0" frameborder="0" tabindex="-1"></iframe>').prependTo(d)}};B.fn.fancybox.defaults={padding:10,margin:40,opacity:false,modal:false,cyclic:false,scrolling:"auto",width:560,height:340,autoScale:true,autoDimensions:true,centerOnScroll:false,ajax:{},swf:{wmode:"transparent"},hideOnOverlayClick:true,hideOnContentClick:false,overlayShow:true,overlayOpacity:0.7,overlayColor:"#777",titleShow:true,titlePosition:"float",titleFormat:null,titleFromAlt:false,transitionIn:"fade",transitionOut:"fade",speedIn:300,speedOut:300,changeSpeed:300,changeFade:"fast",easingIn:"swing",easingOut:"swing",showCloseButton:true,showNavArrows:true,enableEscapeButton:true,enableKeyboardNav:true,onStart:function(){},onCancel:function(){},onComplete:function(){},onCleanup:function(){},onClosed:function(){},onError:function(){}};B(document).ready(function(){B.fancybox.init()})})(jQuery);

 function hideVideoWP(contenedor){
  var ua = navigator.userAgent;
  if(/Windows Phone/.test(ua)){
    $(contenedor).remove();
  }
}

function hideRemoveElementAnimation(elem){
  $(elem).animate({opacity: 0}, 1000, function(){
    $('.user-bubble').removeClass('mostrando');
    //$(elem).remove();
  });
}

function ocultaBubble() {
  if ($('.user-bubble.mostrando').length == 0) {
    $('.user-bubble').addClass('mostrando').attr('style', '');
    if($('.user-bubble').length > 0){
      var time = 5000;
      if(/MSIE 8.0/.test(navigator.userAgent)){
        time = 8000;
      }
      setTimeout(function(){
        hideRemoveElementAnimation('.user-bubble');
      }, time);
    }
  }
}

function desplegableWP() {
  var isWinPhoneDevice = /Windows Phone/.test(navigator.userAgent);
  var layerWidth = document.body.clientWidth;
  if (isWinPhoneDevice && layerWidth > 360 && layerWidth < 535) {
    $('.siniestros.patrimoniales .estiloLista').addClass('wp');
  } else {
    $('.siniestros.patrimoniales .estiloLista').removeClass('wp');
  }
}

$(window).resize(function(event) {

  camidomigasanchoipad();

  desplegableWP();

  if (esmovil()) {
    centrarfancybox();
  }

  if(esWP()){
    $('.visible-wp').removeClass('visible-wp');
    if (window.innerWidth<=325) {
      $('.siniestros.patrimoniales .bloqPest .capaAlerta .no-ico p').addClass("cuadro-WP");
      $('.siniestros.patrimoniales .bloqPest .capaAlerta .no-ico p span').removeClass("break-word");
      $('.siniestros.patrimoniales .bloqPest .capaAlerta .no-ico p span').addClass("break-wp"); 
    } 
    if (window.innerWidth>=324) {
      $('.siniestros.patrimoniales .bloqPest .capaAlerta .no-ico p').removeClass("cuadro-WP");
      $('.siniestros.patrimoniales .bloqPest .capaAlerta .no-ico p span').addClass("break-word");
      $('.siniestros.patrimoniales .bloqPest .capaAlerta .no-ico p span').removeClass("break-wp");
    }  
  }
  if(window.innerWidth<=325){
    $('.interior #principal .llamamos  h1.title').addClass('wphoneDato');
  }
  if (window.innerWidth>=324){ 
    $('.interior #principal .llamamos  h1.title').removeClass('wphoneDato');
  }

  if (esmovil()) {
    if (window.innerWidth < 1024) {
      $('.siniestros.patrimoniales .tiposiniestromatriz .tooltiplink').hide();
      $('.siniestros.patrimoniales .tiposiniestromatriz .iframe').show();
    } else {
      $('.siniestros.patrimoniales .tiposiniestromatriz .tooltiplink').show();
      $('.siniestros.patrimoniales .tiposiniestromatriz .iframe').hide();
    }
  }
});

function camidomigasanchoipad() {
  if (navigator.userAgent.match(/iPad/i) != null && widthScreen() == 1024) {
    $('section#contenido.posicion div#contenidoInt.total div.C360').addClass('anchoipad');
  } else {
    $('section#contenido.posicion div#contenidoInt.total div.C360').removeClass('anchoipad');
  }
}

function centrarfancybox() {
  // centrar verticalmente fancybox
  if (window.innerHeight > 500) {
    $('#fancybox-wrap').addClass('centrado');
  } else {
    $('#fancybox-wrap').removeClass('centrado');
  }
}

$(document).ready(function(){

  camidomigasanchoipad();

  logos.init();
  printPage.init();
  placeholders.init();
  tooltips.init();
  responsive.navigation();

  if(window.innerWidth > 768) {
   $('.autorizacion-sanitaria .lightbox.iframe').attr('data-dimension', 'height:250,width:600');
   $('.siniestros .lightbox.iframe').attr('data-dimension', 'height:250,width:600');
 }
 else{ 
   $('.autorizacion-sanitaria .lightbox.iframe').attr('data-dimension', 'height:250,width:100%');
   $('.siniestros .lightbox.iframe').attr('data-dimension', 'height:250,width:100%'); 
 }

 initLightbox();

 if($(".atencionCliente").length !== 0) atencionCliente.init();
 if($("dl.faq").length !== 0) faqPage.init();
 if($(".operativaAlta").length !== 0) operativaAltaPage.init();
 if($(".solicitud-alta").length !== 0) solicitudAltaPage.init();
 if($(".gestion-productos").length !== 0) acordeonProductos.init();
 if($(".datepicker").length !== 0) calendar.init();
 if($(".accordion").length !== 0) acordeon.init();
 if($(".cmbDepend").length !== 0) camposDependientes.init();
 if($(".teclado").length !== 0) pintaTeclado($(".teclado").attr("id"));

 if($(".CL61 .content-video, article.video").length !== 0) hideVideoWP(".CL61 .content-video, article.video");
  //if($("article.video").length !== 0) hideVideoWP("article.video");

  $(".siniestros .general.talleres tbody tr").click(function() {
    muestraDetalleTaller($(this).find("input").prop("checked", true));
  });

  if($('#cajaNumTarjeta .help a').length > 0){
    if(esmovil()){
      $('#cajaNumTarjeta .help a')
      .on('click', function(e){
        e.preventDefault();
        $('#cajaNumTarjeta #idCarnet').toggleClass('hidden');
      })
      .on('blur', function(){
        $('#cajaNumTarjeta #idCarnet').addClass('hidden');
      });
    } else {
      $('#cajaNumTarjeta .help a')
      .on('click', function(e){
        e.preventDefault();
      })
      .on('mouseenter', function(){
        $('#cajaNumTarjeta #idCarnet').removeClass('hidden');
      })
      .on('mouseleave', function(){
        $('#cajaNumTarjeta #idCarnet').addClass('hidden');
      });

    }
  }

  $('#opcionesul .profile a').hover(function(event) {
    ocultaBubble();
  });

});

$(window).load(function() {
  lightbox.init();
  ocultaBubble();

  if (esmovil()) {
      // alto de shadowbox en patrimoniales tipo siniestro
      if ($('.tiposiniestromatriz').length > 0) {
        $('#fancybox-wrap').addClass('altocaja');
        centrarfancybox();
      }
    }
  });

$(window).on("orientationchange", function(){
  if($("#acPoblacion_list")){
    $("#acPoblacion_list").css("background", "grey");
  }
  
});

//PARA LAS COOKIES
// function initModalFancybox() {
//   var win = jQuery(window);
//   jQuery('a.comprobarCookie').each(function(){
//     var link = jQuery(this);
//     link.fancybox({
//       padding: 0,
//       margin: 0,
//       cyclic: false,
//       autoScale: false,
//       overlayShow: true,
//       overlayOpacity: 0.8,
//       overlayColor: '#333333',
//       titlePosition: 'inside',
//       titleShow: false,
//       height: getDimensionLightbox(link.attr("data-dimension"), "height"),
//       width: getDimensionLightbox(link.attr("data-dimension"), "width"),
//       autoDimensions: false,
//       hideOnOverlayClick : false,
//       openIframe: link.hasClass("iframe"),

//       onComplete: function(box) {
//         if(link.attr('href').indexOf('#') === 0) {
//           jQuery('#fancybox-content').find('.btn-close, .btn-cancel, .lightbox-close').unbind('click.fb').bind('click.fb', function(e){
//             jQuery.fancybox.close();
//             e.preventDefault();
//           });
//         }
//         resizeLightbox();
//         win.bind('resize orientationchange', resizeLightbox);

//         if(this.openIframe){
//           var myHeight = heightScreen();
//           var myWidth = widthScreen();
//           if (esmovil()) {
//             $("#fancybox-frame").css("height", myHeight+"px");
//           } else {
//             $("#fancybox-frame").css("max-height", (myHeight-80)+"px");
//             $("#fancybox-frame").css("height", this.height+"px");
//           }
//           $("#fancybox-content iframe").css("max-width", myWidth+"px");
//           $("#fancybox-content iframe").css("width", this.width+"px");

//         }
//         jQuery.fancybox.center();
//       },
//       onClosed: function(){
//         win.unbind('resize orientationchange', resizeLightbox);
//       },
//       onStart: function(){
//         $.fancybox.center();
//       }
//     });
//   });
//   function resizeLightbox(){
//     jQuery('#fancybox-content, #fancybox-wrap').css({
//       width:'auto',
//       height: 'auto'
//     });
//     jQuery.fancybox.resize();
//     jQuery.fancybox.center();
//   }
// }

$(document).ready(function(){

  //Hay que hacer la carga inicial de la función initModalFancybox() para que funcione, sino no va, pero hay que controlar las cookies al inicio
  if($('.comprobarCookie').hasClass('iframe')){
    var clave = obtenerCookie('modalClose');
    
    if(clave=='') {
      lightbox.init() ;
    }
  }

  function resizeLightbox(){
    jQuery('#fancybox-content, #fancybox-wrap').css({
      width:'auto',
      height: 'auto'
    });
    jQuery.fancybox.resize();
    jQuery.fancybox.center();

    /* Evitar que en movil se vaya hacia arriba */
    if (esmovil()) {
      $("#fancybox-wrap").css("margin-top", "20px");
    }
  }
//  function crearCookie(clave, valor, diasexpiracion) {
//    var d = new Date();
//    d.setTime(d.getTime() + (diasexpiracion*24*60*60*1000));
//    var expires = "expires="+d.toGMTString();
//    document.cookie =  clave + "=" + valor + "; " + expires+";";
//  }
function crearCookie(clave, valor) {

  document.cookie =  clave + "=" + valor + "; ";
}
function obtenerCookie(clave) {

  var name = clave + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') 
      {c = c.substring(1);}
    if (c.indexOf(name) == 0) 
      {return c.substring(name.length,c.length);}
  }
  return "";
}

if(window.innerWidth < 767 && $(".patrimoniales .title").length > 0){
    if(obtenerCookie("ScrollMenu") === "true"){
      var desplazamiento = $(".patrimoniales .title").offset().top;
      $('html,body').animate({
        scrollTop: (desplazamiento-10)
      }, 2000);
    }else{
    crearCookie("ScrollMenu", "true");
    }
}

//Borrado de cookie si pincho en X o fuera del iframe
$("#sb-nav-close").on('click', function(){
  document.cookie =  "modalClose= ; ";
});
$("#sb-overlay").on('click', function(){
  document.cookie =  "modalClose= ; ";
});
//Fin borrado cookie

$('.comprobarCookie').on("click", function(event){
  if (event.preventDefault) {
    event.preventDefault ();
  }else{
    event.returnValue = false; 
  }
  var clave = obtenerCookie('modalClose');
  if(clave!='') {
    location.href="/oim/DesconexionAction.do";
  }else {  
    crearCookie('modalClose', 'true');
    lightbox.init() ;
  }  
});

$('.siniestros.patrimoniales .date input[type="tel"]').on('keyup', function(e) {
  if (/\D/g.test($(this).val()) && ($(this).val()) && e.keyCode != 8) {
    $(this).val($(this).val().replace(/[^0-9\/]/g,''));
  }
});

$('.siniestros.patrimoniales .tiposiniestromatriz input[type=radio]').on('change', function(event) {
  $(this).siblings('.icon').addClass('selected');
  $(this).parents('li').siblings('li').find('.icon').removeClass('selected');
});


if (esmovil() && (window.innerWidth < 1024)) {
  $('.siniestros.patrimoniales .tiposiniestromatriz .tooltiplink').hide();
} else {
  $('.siniestros.patrimoniales .tiposiniestromatriz .iframe').hide();
}

$('.siniestros.patrimoniales .tiposiniestromatriz a.tooltiplink').on('click', function(event) {
  event.preventDefault();
}).on('hover', function(event) {
  if ($(this).position().left < 20) {
    $(this).addClass('left');
  } else {
    $(this).removeClass('left');
  }
  if (($(this).closest('ul').width() - $(this).position().left) < 130) {
    $(this).addClass('right');
  } else {
    $(this).removeClass('right');
  }
});

desplegableWP();

if(esWP()){
  $('.visible-wp').removeClass('visible-wp');
  if (window.innerWidth<=325) {
    $('.siniestros.patrimoniales .bloqPest .capaAlerta .no-ico p').addClass("cuadro-WP");
    $('.siniestros.patrimoniales .bloqPest .capaAlerta .no-ico p span').removeClass("break-word");
    $('.siniestros.patrimoniales .bloqPest .capaAlerta .no-ico p span').addClass("break-wp"); 
  } 
  if (window.innerWidth>=324) {
    $('.siniestros.patrimoniales .bloqPest .capaAlerta .no-ico p').removeClass("cuadro-WP");
    $('.siniestros.patrimoniales .bloqPest .capaAlerta .no-ico p span').addClass("break-word");
    $('.siniestros.patrimoniales .bloqPest .capaAlerta .no-ico p span').removeClass("break-wp");
  }  
}

if(esIE8()){
    $(".siniestros.patrimoniales [maxlength]").on('keydown', function(e){
        var text = $(this).val();
        var lengthMax = $(this).attr("maxlength");
        if (text.length > lengthMax){
            $(this).val(text.substr(0, lengthMax)); 
        }
    });
  $('.siniestros.patrimoniales .tiposiniestromatriz .icon').on('click', function(event) {
    if ($(this).siblings('input[type=checkbox]').is(':checked')) {
      $(this).siblings('input[type=checkbox]').prop("checked", false);
      $(this).removeClass('selected');
    } else {
      $(this).addClass('selected');
      $(this).siblings('input[type=checkbox]').prop("checked", true);
    }
  });
}


var isIphone = /iphone/i.test(navigator.userAgent.toLowerCase());
if (isIphone) {
  $('#contenido .interior #principal .siniestros.patrimoniales .caja div.filaBoton div.acepto input[type="checkbox"]#acepto').addClass('iphone');
  $('#contenido .interior #principal .llamamos .caja div.texto input[type="checkbox"]').css('margin-top', '8px');
}

$('.no-phone a').on('click', function(event) {
  event.preventDefault();
});

if(esWP()){
  $('#contenido .interior #principal .llamamos .caja div.texto input[type=checkbox]').css('margin-top', '11px');
  if(window.innerWidth<=325){
    $('.interior #principal .llamamos  h1.title').addClass('wphoneDato');
  }
  if (window.innerWidth>=324){ 
    $('.interior #principal .llamamos  h1.title').removeClass('wphoneDato');
  }
}

    // Cerrar popup shadowbox
    $('.popup .siniestros.patrimoniales .cerrar a').on('click', function(event) {
      parent.$.fancybox.close();
    });


    //scroll en Iphone e Ipad
    var isIphone = /iphone/i.test(navigator.userAgent.toLowerCase());
    var isIpad = navigator.userAgent.match(/iPad/i) != null;
    if (isIphone || isIpad) {
      $('#fancybox-content').css({
        overflow: 'scroll', 
        '-webkit-overflow-scrolling': 'touch' 
      }); 

    }
  });

function openWindow(percent) {
  var w = 1248, h = 536;
  if (window.screen) {
    w = window.screen.availWidth * percent / 100;
    h = window.screen.availHeight * percent / 100;
  }
  window.open('https://lt.morningstar.com/ikx1j4iork/snapshot/snapshot.aspx?&SecurityToken=F0GBR04OPF%5d2%5d1%5dFOESP$$ALL_3656&ClearXrayPortfolioManagerApiInputData=true','mywindow','width='+w+',height='+h);
}

$("#btnexternal").click( function(){
  if (window.screen.width < 1200) {
    openWindow(80)
  }else{
    window.open ("https://lt.morningstar.com/ikx1j4iork/snapshot/snapshot.aspx?&SecurityToken=F0GBR04OPF%5d2%5d1%5dFOESP$$ALL_3656&ClearXrayPortfolioManagerApiInputData=true","mywindow","menubar=1,resizable=1,width=1248,height=536");
  }
});






