// Archivo que contiene todas las funciones javascript que intervienen en el nuevo proceso de alta (alta on line) 

// Definicion de variables globales
	var evt;
	var control = false;
	var validar = true;
	var message = "";
	var reini = false;
	var tieneFocoCancelar = false;
	var tieneFocoContinuar = false;
	// Variables que intervienen en el inicio del proceso de alta
	var cancelar;
	var vieneQueEsOIM;
	// Variables que intervienen en la comprobacion del formato en el cual se generara el contrato (ReconocimientoFinProceso.jsp)
	var agt=navigator.userAgent.toLowerCase();
	var ie = (agt.indexOf("msie") != -1);
	var ns = (navigator.appName.indexOf("Netscape") != -1);
	var win = ((agt.indexOf("win")!=-1) || (agt.indexOf("32bit")!=-1));
	var mac = (agt.indexOf("mac")!=-1);
	
	// Variables que intervienen en la descarga del contrato (ReconocimientoFinProcesoDescarga.jsp)
	var enviado;
	var formato;
	var mensaje;
	var urlAcceso;

// Funciones que intervienen en el inicio del proceso de alta (SolicitudAlta_NUEVA.jsp)
	function iniciarProcesoAlta()
	{
		if (cancelar)
		{
			
			document.location.href = "./SolicitudAltaDatosAction.do?cancel=0";
			
		}
		else if (inicio)
		{
			if (ie && win)
			{
				document.location.href = "./SolicitudAltaDatosAction.do?inicio=0&ie=0";
			}
			else
			{
				document.location.href = "./SolicitudAltaDatosAction.do?inicio=0";
			}
		}
	}
	
// Funciones que intervienen en la captura de datos (SolicitudDatosAlta.jsp)
	function muestraconletras()
	{
		/*if((document.getElementById('cboTipoDoc').options[document.getElementById('cboTipoDoc').selectedIndex].text).indexOf("Pasaporte")!=-1)
		{
			document.getElementById("conletras").style.display = "none";
		}
		else
		{
			document.getElementById("conletras").style.display = "block";
		}*/
		if ((document.getElementById("cboTipoDoc").options[document.getElementById("cboTipoDoc").selectedIndex].text).indexOf("N.I.F")==-1)
		{
			document.getElementById("apellido2").style.visibility = "hidden";
		}
		else
		{
			document.getElementById("apellido2").style.visibility = "visible";
		}
	}
	
	function cancelarCapturaDatos()
	{	
		if (!tieneFocoContinuar)
		{
			
			var result = confirm(mensaje);
			var url;
			if (result)
			{
				// Se comprueba si el proceso se inicia desde la opcion de menu verical "solicitud de alta" o
				// desde la opci�n "Que es la �rea de Clientes" para ver donde se redirije al cancelar
				
				if (vieneQueEsOIM)
				{
					url = "./QueEsOIMAction.do?cancel=0";
				}
				else
				{
					url = "./SolicitudAltaAction.do?cancel=0";
				}
				
				// se fija el campo oculto hidValidar a false para que no se validen los campos del formulario
				// puesto que no se va a enviar
				document.forms[0].hidValidar.value="false";
				document.forms[0].action= url;
				validar = false;
				
				
			}
			else
			{
				// fijamos el campo oculto hidValidar a false para que no se validen los campos del formulario
				// puesto que no se va a enviar
				document.forms[0].hidValidar.value="false";
				validar = false;
				document.forms[0].action= "./SolicitudAltaDatosAction.do";
			}
		}
		else
		{
	
			document.forms[0].submit();
		}
	
	}
	
	function limpiarVista(limpia)
	{
		if (limpia)
		{
			document.forms[0].cboTipoDoc.value="0";
			document.forms[0].txtNumDocumento.value="";
			document.forms[0].cboTratamiento.value="Seleccionar";
			document.forms[0].txtNombre.value="";
			document.forms[0].txtApellido.value="";
			document.forms[0].txtApellido2.value="";
			document.forms[0].txtFechaNacimiento1.value="";
			document.forms[0].txtFechaNacimiento2.value="";
			document.forms[0].txtFechaNacimiento3.value="";
			document.forms[0].txtTelefono1.value="";
			document.forms[0].txtTelefono2.value="";
			document.forms[0].cboHorario.value="1";
			document.forms[0].cboHorario2.value="1";
			document.forms[0].txtCorreoElectronico.value="";
			document.forms[0].txtConfirmacionCorreoElectronico.value="";
			document.forms[0].cboTipoContrato.value="1";
			document.forms[0].cboTipoVia.value="000";
			document.forms[0].txtNombreVia.value="";
			document.forms[0].txtNumero.value="";
			document.forms[0].txtPortal.value="";
			document.forms[0].txtEscalera.value="";
			document.forms[0].txtPiso.value="";
			document.forms[0].txtPuerta.value="";
			document.forms[0].txtComplementoDireccion.value="";
			document.forms[0].txtLocalidad.value="";
			document.forms[0].txtCP.value="";
			document.forms[0].txtProvincia.value="";
		}
		
		
	}
	
	function borrar()
	{	
		if (document.getElementById('txtCP').value == "")
		{
			if(document.forms[0].txtProvincia.value != "")
			{
				document.forms[0].txtProvincia.value="";
				
			}
		}
	}
	
// Funciones que intervienen en la confirmacion de los datos (RevisionDatosSolicitudAlta.jsp)

	function capaBoton()
	{
		document.getElementById("boton1").style.display="none";			
  		document.getElementById("comprobando").style.display = "block";		
	}

	function cancelarRevisionDatosAlta()
	{
		var result = confirm(mensaje);
		if (result)
		{
			obtenerURL();
		}
		else
		{
			return result;	
		}
	}
	
	function obtenerURL()
	{
		if (vieneQueEsOIM)
		{
			document.location.href="./QueEsOIMAction.do?cancel=0";
		}
		else
		{
			document.location.href="./ValidarIdentificacionAction.do";
		}
	}
	
	function prepararURL()
	{
		if (vieneQueEsOIM)
		{
			document.location.href="./SolicitudAltaOnlineAction.do.do?param=0&queEsOIM=1";
		}
		else
		{
			document.location.href="./SolicitudAltaOnlineAction.do?param=0";
		}
	}

// Funciones que intervienen en la deteccion del formato en el cual se generara el contrato (ReconocimientoFinProceso.jsp)
	
	function comprobarRadio()
	{
		var formato;
		if (document.getElementById('onLine').name == 'onLine' && document.getElementById('onLine').checked)
		{
			if (detectarFormatoContrato())
			{
				formato = "pdf";
			}
			else
			{
				formato = "html";
			}
		}
		return formato;
	}
	
	function cambioEstado(pObjeto)
	{
		if (pObjeto.name == 'onLine' && pObjeto.checked)
		{
			if (document.getElementById('correopostal').checked)
			{
				document.getElementById('correopostal').checked = "";	
				pObjeto.checked = "checked";
				return;
			}
			else if (!document.getElementById('correopostal').checked)
			{
				document.getElementById('correopostal').checked = "checked";
				pObjeto.checked = "";
			}	
		}
		
		if (pObjeto.name == 'correopostal' && pObjeto.checked)
		{
			if (document.getElementById('onLine').checked)
			{
				document.getElementById('onLine').checked = "";	
				pObjeto.checked = "checked";
				return;
			}
			else if (!document.getElementById('onLine').checked)
			{
				document.getElementById('onLine').checked = "checked";
				pObjeto.checked = "";
				
			}			
		}
	}
	
	function Enviar()
	{	
		var formato = comprobarRadio();
		if (formato != null)
		{
			document.location.href="./ImpresionContratoAltaAction.do?formato=" + formato;
		}
		else
		{
			document.location.href="./EnvioContratoCorreoPostalAction.do";
		}
	}
	

// Funciones que intervienen en la descarga del contrato (ReconocimientoFinProcesoDescarga.jsp)

	function AbreContratoHTML(URL,anchura,altura)
	{
		enviado = true;
		ventana=window.open(URL,"ventanita","scrollbars=yes,width="+anchura+",height="+altura);
		ventana.resizeTo(anchura,altura);
	}
	
	function comprobarFormato(comprobar)
	{
		if (comprobar)
		{
			if (detectarFormatoContrato())
			{
				formato = "pdf";				
			}
			else
			{
				formato = "html";
			}
			if (ie) {
				document.location.href="./ImpresionContratoAltaAction.do?formato=" + formato + "&accesoOIM=0&compFormato=false";
			}
			if (document.forms[0] != null) {
				if (ns) {
					window.onbeforeunload = null;
					document.forms[0].action ="/oim/ImpresionContratoAltaAction.do?formato=" + formato + "&accesoOIM=0&compFormato=false";
				}
				document.forms[0].submit();
			}
			
		}
		
	}
	

	
// Funciones que intervienen cuando se descarga el contrato en formato HTML (ContratoAltaPopUp.jsp)

	function imprimir()
	{
		mostrarCapa('ejemplarTitular');
		ocultarCapa('iconoImprimir');
		window.print();
		ocultarCapa('ejemplarTitular');
		mostrarCapa('iconoImprimir');
	}
	
	function mostrarCapa(nombreCapa)
	{
		document.getElementById(nombreCapa).style.display="block"; 
	}
	
	function ocultarCapa(nombreCapa)
	{
		document.getElementById(nombreCapa).style.display="none"; 
	}
	

// Funciones que intervienen en las paginas de validaciones
	// Funciones pagina valicacion correo introducico incorrectamente
	function comprobarModificacionBBDD()
	{
		if (document.getElementById('Si').checked)
		{
			document.location.href = "./ValidacionIncorrectaEmailAction.do?cambio=0";
		}
		else if (document.getElementById('No').checked)
		{
			document.location.href = "./ValidacionIncorrectaEmailAction.do";
		}
	}
	function cambiarEstadoRadioButton(pObjeto)
	{
		if (pObjeto.name == 'Si' && pObjeto.checked)
		{
			if (document.getElementById('No').checked)
			{
				document.getElementById('No').checked = "";	
				pObjeto.checked = "checked";
				return;
			}
			else if (!document.getElementById('No').checked)
			{
				document.getElementById('No').checked = "checked";
				pObjeto.checked = "";
			}	
		}
		
		if (pObjeto.name == 'No' && pObjeto.checked)
		{
			if (document.getElementById('Si').checked)
			{
				document.getElementById('Si').checked = "";	
				pObjeto.checked = "checked";
				return;
			}
			else if (!document.getElementById('Si').checked)
			{
				document.getElementById('Si').checked = "checked";
				pObjeto.checked = "";
				
			}			
		}
	}
	
// Funciones comunes

	function Cerrar()
	{	
		window.close();
	}
	
	// Intervienen tanto ReconocimientoFinProceso.jsp como en ReconocimientoFinProcesoDescarga.jsp
	
	function detectarPdf()
	{
        if (ie && win) 
        { 
            pluginlist = detectIE("PDF.PdfCtrl.4","Acrobat Reader") + detectIE("PDF.PdfCtrl.5","Acrobat Reader") + detectIE("PDF.PdfCtrl.6","Acrobat Reader") + detectIE("AcroPDF.PDF.1","Acrobat Reader"); 
            return pluginlist;
        }
        if (ns || !win) 
        {
			nse = ""; 
            for (var i=0;i<navigator.mimeTypes.length;i++) 
            {
                nse += navigator.mimeTypes[i].type.toLowerCase();
			}
            pluginlist = detectNS("application/pdf","Acrobat Reader");
			return pluginlist;

        }
	
	}
	 
	function detectIE(ClassID,name) 
	{ 
		result = false;
		document.write("<SCRIPT LANGUAGE=VBScript>\n on error resume next \n result = IsObject(CreateObject(\"" + ClassID + "\"))<\/SCRIPT>"); 
	
	    if (result)
	    { 
			return name+','; 
		}
		else
		{
			return ''; 
		}
	}
	
	function detectNS(ClassID,name) 
	{ 
		n = ""; 
		if (nse.indexOf(ClassID) != -1) 
		{
			if (navigator.mimeTypes[ClassID].enabledPlugin != null) 
			{
	         	n = name+","; 
			}
	        return n; 
		}
	}
	
	function detectarFormatoContrato()
	{
		if( ! detectarPdf())
		{
			pdf = "false";
		}
		else
		{
			pdf = "true";
		}
		return pdf;
	}
	
	/*function detectarNavegador()
	{
		if (reini)
		{
			if (ie && win) 
			{
				document.getElementById("capapasos").style.visibility = "visible";
			}
		}
		else if (ie && win)
		{
		
			document.getElementById("capapasos").style.visibility = "visible";
		}

	}*/
	
	// Muestra un mensaje de aviso cuando se intenta cerrar la ventana del navegador
	function mostrarAvisoCierreVentanaNavegador(evt) 
	{
		if (typeof evt == 'undefined') 
		{
           	evt = window.event;
        }

        if (evt) 
        {
        	evt.returnValue = mensaje;
        }
       
        return mensaje;

	}
	// Controla que el evento de onunload no salte por un cambio de pantalla, pondra la variabla control a true
	// cuando se pulse un boton o un enlace que se encuentre en la pagina
	function controlCambio()
	{
		control = true;
	}
	
	// Funcion para deshabilitar boton forward del navegador para IE version 7
	//if (history.forward(1)){location.replace(history.forward(1))}
	
	
	
	// Funcion para deshabilitar el boton F5 y controlar el foco al pulsar enter
	document.onkeydown = pulsaTecla;
	
	if (ns)
	{
		window.captureEvents(Event.KEYDOWN);
	}
	
	function pulsaTecla(evt)
	{
		// Codigo para NS
		if (document.layers || (document.getElementById && ! document.all))
		{
			if (evt.which == 116)
			{	
				return false;
			}
			if (evt.which == 13)
			{
				if (!tieneFocoCancelar)
				{
					if (!tieneFocoContinuar)
					{
						controlCambio();
						return false;
					}
				}
			}
			
		}
		else // Codigo para IE
		{
			if (window.event && window.event.keyCode == 116)
			{
				window.event.keyCode = 505;
			}
			if (window.event && window.event.keyCode == 505)
			{
				return false;
			}
			if (window.event && window.event.keyCode == 13)
			{	
				if (!tieneFocoCancelar)
				{
					if (!tieneFocoContinuar)
					{
						controlCambio();
						return false;
					}
				}
			}
			
		}
	}
	
	function foco (campo)
	{
		if (campo.name == "bntCancelar")
		{
			tieneFocoCancelar = true;
			tieneFocoContinuar = false;
		}
		else
		{
			tieneFocoContinuar = true;
			tieneFocoCancelar = false;
		}
		
	}
	
	// FIN --- Funcion para deshabilitar el boton F5
	
	// Funciones para deshabilitar boton derecho del raton
	// Funcion para IE
	function clickIE()
	{
		if (document.all)
		{
			(message);
			return false;
		}
	}
	
	// Funcion para NS
	function clickNS(e)
	{
		if (document.layers || (document.getElementById && ! document.all))
		{
			if (e.which == 2 || e.which == 3)
			{
				(message);
				return false;
			}
		}
	}
	
	// Codigo para NS
	if (document.layers)
	{
		document.captureEvents(Event.MOUSEDOWN);
		document.onmousedown = clickNS;
	}
	else // Codigo para IE
	{
		document.onmouseup = clickNS;
		document.oncontextmenu = clickIE;
	}
	document.oncontextmenu = new Function("return false");
	
	// FIN --- Funciones para deshabilitar boton derecho del raton

	