

	// Nombre del applet. Se usar� para obtener la referencia del applet a partir del document
	var siavalAppletName = "siavalApplet";

	
 	function firmarDocumentoXML(sDocumento, listaIDs, papelFirmante, tituloVentanaSeleccionFirmante)
 	{
  		return (document.applets[siavalAppletName].firmarDocumentoXML(sDocumento, listaIDs, papelFirmante, tituloVentanaSeleccionFirmante));
 	}


 	function firmarFicheroXML(ficheroEntrada, ficheroSalida, listaIDs, papelFirmante, tituloVentanaSeleccionFirmante)
 	{
  		return (document.applets[siavalAppletName].firmarFicheroXML(ficheroEntrada, ficheroSalida, listaIDs, papelFirmante, tituloVentanaSeleccionFirmante));
 	}

	
 	function firmarDocumentoXMLFirmado(documento, idNodoFirmado, papelFirmante, tituloVentanaSeleccionFirmante)
 	{
  		return (document.applets[siavalAppletName].firmarDocumentoXMLFirmado(documento, idNodoFirmado, papelFirmante, tituloVentanaSeleccionFirmante));
 	}

	
 	function firmarFicheroXMLFirmado(ficheroEntrada, ficheroSalida, idNodoFirmado, papelFirmante, tituloVentanaSeleccionFirmante)
 	{
  		return (document.applets[siavalAppletName].firmarFicheroXMLFirmado(ficheroEntrada, ficheroSalida, idNodoFirmado, papelFirmante, tituloVentanaSeleccionFirmante));
 	}

	
 	function firmarDocumentoPKCS7(lmessage, incluir)
 	{
  		return (document.applets[siavalAppletName].firmarDocumentoPKCS7(lmessage, incluir));
	}


	
 	function firmarFicheroPKCS7(ficheroEntrada, ficheroSalida, incluir)
 	{
		return (document.applets[siavalAppletName].firmarFicheroPKCS7(ficheroEntrada, ficheroSalida, incluir));
 	}

	
 	function firmarPDF(ficheroEntrada, ficheroSalida)
 	{
  		return (document.applets[siavalAppletName].firmarPDF(ficheroEntrada, ficheroSalida));
 	}


 	function firmarPDF(ficheroEntrada, ficheroSalida, style)
 	{
  		return (document.applets[siavalAppletName].firmarPDF(ficheroEntrada, ficheroSalida, style));
 	}

	
 	function firmarFecharPDF(ficheroEntrada, ficheroSalida, urlTimeStampServer)
 	{
  		return (document.applets[siavalAppletName].firmarFecharPDF(ficheroEntrada, ficheroSalida, urlTimeStampServer));
 	}

	
 	function firmarFecharPDF(ficheroEntrada, ficheroSalida, urlTimeStampServer, style)
 	{
  		return (document.applets[siavalAppletName].firmarFecharPDF(ficheroEntrada, ficheroSalida, urlTimeStampServer, style));
 	}

	
 	function firmarPDFBase64(documento)
 	{
		return (document.applets[siavalAppletName].firmarPDFBase64(documento));
 	}

	
 	function firmarPDFBase64(documento, style)
 	{
		return (document.applets[siavalAppletName].firmarPDFBase64(documento, style));
 	}

	
 	function firmarFecharPDFBase64(documento, urlTimeStampServer)
 	{
		return (document.applets[siavalAppletName].firmarFecharPDFBase64(documento, urlTimeStampServer));
 	}

	
 	function firmarFecharPDFBase64(documento, urlTimeStampServer, style)
 	{
		return (document.applets[siavalAppletName].firmarFecharPDFBase64(documento, urlTimeStampServer, style));
 	}


	
	function beginBatchMode()
	{
		return (document.applets[siavalAppletName].beginBatchMode());
	}

	
	function endBatchMode()
	{
		return (document.applets[siavalAppletName].endBatchMode());
	}

	
	function getCode(res)
 	{
  		var ind = res.indexOf('#');

  		if ( ind != -1 )
  		{
    		return res.substring(0,ind);
  		}
  		else
  			return res;
	}

	
 	function getDescription(res)
 	{
  		var ind = res.indexOf('#');

  		if ( ind != -1 )
  		{
    		return res.substring(ind + 1);
  		}
  		else
  			return "";
 	}


	
	 function firmarDocumentoNoXMLaFichero(sRutaOrigen, sRutaDestino, papelFirmante, des, mime, tituloVentanaSeleccionFirmante)
	 {
	  return (document.applets[siavalAppletName].firmarDocumentoNoXMLaFichero(sRutaOrigen, sRutaDestino, papelFirmante, des, mime, tituloVentanaSeleccionFirmante));
	 }

	
 	function validarDocumentoPKCS7Attached(documentoP7Base64)
 	{
		return (document.applets[siavalAppletName].validarDocumentoPKCS7Attached(documentoP7Base64));
 	}

	
 	function validarFicheroPKCS7Attached(rutaDocumentoP7, p7EnBase64)
 	{
		return (document.applets[siavalAppletName].validarFicheroPKCS7Attached(rutaDocumentoP7, p7EnBase64));
 	}

	
 	function validarDocumentoPKCS7Detached(documentoP7Base64, original)
 	{
		return (document.applets[siavalAppletName].validarDocumentoPKCS7Detached(documentoP7Base64, original));
 	}

	
 	function validarFicheroPKCS7Detached(rutaP7, p7InBase64, rutaOriginal, originalInBase64)
 	{
		return (document.applets[siavalAppletName].validarFicheroPKCS7Detached(rutaP7, p7InBase64, rutaOriginal, originalInBase64));
 	}

	
 	function validarDocumentoXMLSignature(documentoXML)
 	{
		return (document.applets[siavalAppletName].validarDocumentoXMLSignature(documentoXML));
 	}

	
 	function validarFicheroXMLSignature(rutaDocumentoXML)
 	{
		return (document.applets[siavalAppletName].validarFicheroXMLSignature(rutaDocumentoXML));
 	}

	
 	function validarDocumentoPDF(documentoPDFBase64)
 	{
		return (document.applets[siavalAppletName].validarDocumentoPDF(documentoPDFBase64));
 	}

	
 	function validarFicheroPDF(rutaDocumentoPDF)
 	{
		return (document.applets[siavalAppletName].validarFicheroPDF(rutaDocumentoPDF));
 	}

	
 	function validarDocumentoSMIME(documentoSMIME)
 	{
		return (document.applets[siavalAppletName].validarDocumentoSMIME(documentoSMIME));
 	}

	
 	function validarFicheroSMIME(rutaDocumentoSMIME)
 	{
		return (document.applets[siavalAppletName].validarFicheroSMIME(rutaDocumentoSMIME));
 	}

	
 	function agregarFirmanteADocumentoPKCS7(pkcs7)
 	{
		return (document.applets[siavalAppletName].agregarFirmanteADocumentoPKCS7(pkcs7));
 	}

	
 	function agregarFirmanteAFicheroPKCS7(rutaPkcs7, ficheroEnBase64)
 	{
		return (document.applets[siavalAppletName].agregarFirmanteAFicheroPKCS7(rutaPkcs7, ficheroEnBase64));
 	}

	
 	function agregarFirmanteADocumentoPKCS7Attached(pkcs7)
 	{
		return (document.applets[siavalAppletName].agregarFirmanteADocumentoPKCS7Attached(pkcs7));
 	}

	
 	function agregarFirmanteAFicheroPKCS7Attached(rutaPkcs7, ficheroEnBase64, ficheroSalida)
 	{
		return (document.applets[siavalAppletName].agregarFirmanteAFicheroPKCS7Attached(rutaPkcs7, ficheroEnBase64, ficheroSalida));
 	}

	
 	function agregarFirmanteADocumentoPKCS7Detached(pkcs7, externalDoc)
 	{
		return (document.applets[siavalAppletName].agregarFirmanteADocumentoPKCS7Detached(pkcs7, externalDoc));
 	}

	
 	function agregarFirmanteAFicheroPKCS7Detached(rutaPkcs7, ficheroEnBase64, externalDoc, externalDocEnBase64, ficheroSalida)
 	{
		return (document.applets[siavalAppletName].agregarFirmanteAFicheroPKCS7Detached(rutaPkcs7, ficheroEnBase64,externalDoc,externalDocEnBase64, ficheroSalida));
 	}


 	function protegerFicheroConfig(rutaOrigen, rutaDestino)
 	{
		return (document.applets[siavalAppletName].protegerFicheroConfig(rutaOrigen, rutaDestino));
 	}

	
	function cifrarDocumentoPKCS7(message, certificados, encryptionAlgorithm)
	{
		return (document.applets[siavalAppletName].cifrarDocumentoPKCS7(message, certificados, encryptionAlgorithm));
	}

	
	function cifrarFicheroPKCS7(rutaFichero, ficheroSalida, certificados, encryptionAlgorithm)
	{
		return (document.applets[siavalAppletName].cifrarFicheroPKCS7(rutaFichero, ficheroSalida, certificados, encryptionAlgorithm));
	}

	
	function cifrarDocumentoXML(documento, listaIDs, certificados, encryptionAlgorithm)
	{
		return (document.applets[siavalAppletName].cifrarDocumentoXML(documento, listaIDs, certificados, encryptionAlgorithm));
	}

	
	function cifrarFicheroXML(ficheroEntrada, ficheroSalida, listaIDs, certificados, encryptionAlgorithm)
	{
		return (document.applets[siavalAppletName].cifrarFicheroXML(ficheroEntrada, ficheroSalida, listaIDs, certificados, encryptionAlgorithm));
	}

	
	function descifrarDocumentoPKCS7ConP12(documentoP7Base64, pathP12, contraseniaP12)
	{
		return (document.applets[siavalAppletName].descifrarDocumentoPKCS7ConP12(documentoP7Base64, pathP12, contraseniaP12));
	}

	
	function descifrarFicheroPKCS7ConP12(ficheroP7Cifrado, ficheroSalida, pathP12, contraseniaP12)
	{
		return (document.applets[siavalAppletName].descifrarFicheroPKCS7ConP12(ficheroP7Cifrado, ficheroSalida, pathP12, contraseniaP12));
	}

	
	function descifrarDocumentoXMLConP12(documentoXml, pathP12, contraseniaP12)
	{
		return (document.applets[siavalAppletName].descifrarDocumentoXMLConP12(documentoXml, pathP12, contraseniaP12));
	}

	
	function descifrarFicheroXMLConP12(ficheroXMLCifrado, ficheroSalida, pathP12, contraseniaP12)
	{
		return (document.applets[siavalAppletName].descifrarFicheroXMLConP12(ficheroXMLCifrado, ficheroSalida, pathP12, contraseniaP12));
	}

	
	function firmarXMLVentanaMultiplesReferencias(documento, listaRefsIds, listaRefsHttp, listaRefsAbsolutePathFile, listaRefsRelativePathFile, firmaTodo, papelFirmante, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].firmarDocumentoXMLMultiplesReferencias(documento, listaRefsIds, listaRefsHttp, listaRefsAbsolutePathFile, listaRefsRelativePathFile, firmaTodo, papelFirmante, tituloVentanaSeleccionFirmante));
	}

	
	function firmarXMLFicheroMultiplesReferencias(ficheroEntrada, ficheroSalida, listaRefsIds, listaRefsHttp, listaRefsAbsolutePathFile, listaRefsRelativePathFile, firmaTodo, papelFirmante, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].firmarFicheroXMLMultiplesReferencias(ficheroEntrada, ficheroSalida, listaRefsIds, listaRefsHttp, listaRefsAbsolutePathFile, listaRefsRelativePathFile, firmaTodo, papelFirmante, tituloVentanaSeleccionFirmante));
	}

	
	function adjuntarSelladoDeTiempoFicheroXML(ficheroEntrada, ficheroSalida, urlTimeStampServer)
	{
		return (document.applets[siavalAppletName].adjuntarSelladoDeTiempoFicheroXML(ficheroEntrada, ficheroSalida, urlTimeStampServer));
	}


	function adjuntarSelladoDeTiempoDocumentoXML(documento, urlTimeStampServer)
	{
		return (document.applets[siavalAppletName].adjuntarSelladoDeTiempoDocumentoXML(documento, urlTimeStampServer));
	}

	
	function adjuntarSelladoDeTiempoDocumentoPKCS7FirmadoEnBase64(message, urlTimeStampServer)
	{
		return (document.applets[siavalAppletName].adjuntarSelladoDeTiempoDocumentoPKCS7FirmadoEnBase64(message, urlTimeStampServer));
	}


	
	function adjuntarSelladoDeTiempoFicheroPKCS7Firmado(ficheroEntrada, ficheroSalida, urlTimeStampServer)
	{
		return (document.applets[siavalAppletName].adjuntarSelladoDeTiempoFicheroPKCS7Firmado(ficheroEntrada, ficheroSalida, urlTimeStampServer));
	}

	
	function establecerAlgoritmoHash(algoritmo)
	{
		return (document.applets[siavalAppletName].establecerAlgoritmoHash(algoritmo));
	}

	
	function firmarDocumentoCAdES(message, messageInBase64, incluir, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].firmarDocumentoCAdES(message, messageInBase64, incluir, tituloVentanaSeleccionFirmante));
	}

	
	function firmarFicheroCAdES(rutaFichero, ficheroSalida, incluir, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].firmarFicheroCAdES(rutaFichero, ficheroSalida, incluir, tituloVentanaSeleccionFirmante));
	}

	
	function agregarFirmanteAFicheroCAdESAttached(ficheroCAdES, ficheroSalida, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].agregarFirmanteAFicheroCAdESAttached(ficheroCAdES, ficheroSalida, tituloVentanaSeleccionFirmante));
	}

	
	function agregarFirmanteAFicheroCAdESDetached(ficheroCAdES, rutaExternalDoc, ficheroSalida, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].agregarFirmanteAFicheroCAdESDetached(ficheroCAdES, rutaExternalDoc, ficheroSalida, tituloVentanaSeleccionFirmante));
	}

	
	function agregarFirmanteADocumentoCAdESAttached(cadesDoc, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].agregarFirmanteADocumentoCAdESAttached(cadesDoc, tituloVentanaSeleccionFirmante));
	}

	
	function agregarFirmanteADocumentoCAdESDetached(cadesDoc, externalDoc, externalDocInBase64, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].agregarFirmanteADocumentoCAdESDetached(cadesDoc, externalDoc, externalDocInBase64, tituloVentanaSeleccionFirmante));
	}

	
	function firmarFirmasFicheroCAdESAttached(ficheroCAdES, ficheroSalida, firmasObjetivo, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].firmarFirmasFicheroCAdESAttached(ficheroCAdES, ficheroSalida, firmasObjetivo, tituloVentanaSeleccionFirmante));
	}

	
	function firmarFirmasFicheroCAdESDetached(ficheroCAdES, rutaExternalDoc, ficheroSalida, firmasObjetivo, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].firmarFirmasFicheroCAdESDetached(ficheroCAdES, rutaExternalDoc, ficheroSalida, firmasObjetivo, tituloVentanaSeleccionFirmante));
	}

	
	function firmarFirmasDocumentoCAdESAttached(cadesDoc, firmasObjetivo, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].firmarFirmasDocumentoCAdESAttached(cadesDoc, firmasObjetivo, tituloVentanaSeleccionFirmante));
	}

	
	function firmarFirmasDocumentoCAdESDetached(cadesDoc, externalDoc, externalDocInBase64, firmasObjetivo, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].firmarFirmasDocumentoCAdESDetached(cadesDoc, externalDoc, externalDocInBase64, firmasObjetivo, tituloVentanaSeleccionFirmante));
	}

	
	function identificarFirmasDocumentoCAdES(cadesDoc)
	{
		return (document.applets[siavalAppletName].identificarFirmasDocumentoCAdES(cadesDoc));
	}

	
	function identificarFirmasFicheroCAdES(ficheroCAdES)
	{
		return (document.applets[siavalAppletName].identificarFirmasFicheroCAdES(ficheroCAdES));
	}

	
	function extraerContenidoDocumentoCAdES(cadesDoc, contenidoEnBase64)
	{
		return (document.applets[siavalAppletName].extraerContenidoDocumentoCAdES(cadesDoc, contenidoEnBase64));
	}

	
	function extraerContenidoFicheroCAdES(ficheroCAdES, ficheroSalida)
	{
		return (document.applets[siavalAppletName].extraerContenidoFicheroCAdES(ficheroCAdES, ficheroSalida));
	}

	
	function validarDocumentoCAdESAttached(docCAdES)
	{
		return (document.applets[siavalAppletName].validarDocumentoCAdESAttached(docCAdES));
	}
	
	function validarDocumentoCAdESDetached(docCAdES, original, originalEnBase64)
	{
		return (document.applets[siavalAppletName].validarDocumentoCAdESDetached(docCAdES, original, originalEnBase64));
	}

	
	function validarFicheroCAdESDetached(rutaCAdES, rutaOriginal)
	{
		return (document.applets[siavalAppletName].validarFicheroCAdESDetached(rutaCAdES, rutaOriginal));
	}

	
	function validarFicheroCAdESAttached(rutaCAdES)
	{
		return (document.applets[siavalAppletName].validarFicheroCAdESAttached(rutaCAdES));
	}

	
	function firmarSecuenciaFirmasFicheroCAdES(ficheroCAdES, ficheroSalida, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].firmarSecuenciaFirmasFicheroCAdES(ficheroCAdES, ficheroSalida, tituloVentanaSeleccionFirmante));
	}

	
	function firmarSecuenciaFirmasDocumentoCAdES(cadesDoc, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].firmarSecuenciaFirmasDocumentoCAdES(cadesDoc, tituloVentanaSeleccionFirmante));
	}

	
	function firmarNivelFirmasFicheroCAdES(ficheroCAdES, ficheroSalida, nivel, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].firmarNivelFirmasFicheroCAdES(ficheroCAdES, ficheroSalida, nivel, tituloVentanaSeleccionFirmante));
	}

	
	function firmarNivelFirmasDocumentoCAdES(cadesDoc, nivel, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].firmarNivelFirmasDocumentoCAdES(cadesDoc, nivel, tituloVentanaSeleccionFirmante));
	}

	
	function firmarSecuenciaFirmasDocumentoPKCS7(p7Doc, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].firmarSecuenciaFirmasDocumentoPKCS7(p7Doc, tituloVentanaSeleccionFirmante));
	}

	
	function firmarSecuenciaFirmasFicheroPKCS7(ficheroPKCS7, ficheroSalida, tituloVentanaSeleccionFirmante)
	{
		return (document.applets[siavalAppletName].firmarSecuenciaFirmasFicheroPKCS7(ficheroPKCS7, ficheroSalida, tituloVentanaSeleccionFirmante));
	}

	
	function identificarFirmasDocumentoPKCS7(p7Doc)
	{
		return (document.applets[siavalAppletName].identificarFirmasDocumentoPKCS7(p7Doc));
	}

	
	function identificarFirmasFicheroPKCS7(ficheroPKCS7)
	{
		return (document.applets[siavalAppletName].identificarFirmasFicheroPKCS7(ficheroPKCS7));
	}

	
	function extraerContenidoDocumentoPKCS7(p7Doc, contenidoEnBase64)
	{
		return (document.applets[siavalAppletName].extraerContenidoDocumentoPKCS7(p7Doc, contenidoEnBase64));
	}

	
	function extraerContenidoFicheroPKCS7(ficheroPKCS7, ficheroSalida)
	{
		return (document.applets[siavalAppletName].extraerContenidoFicheroPKCS7(ficheroPKCS7, ficheroSalida));
	}

    
	function recargarPropiedad(nombreFichero)
	{
		(document.applets[siavalAppletName].recargarPropiedad(nombreFichero));
	}

