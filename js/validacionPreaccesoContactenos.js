// Varibles globales
var messageErrorFormatoNIF="";
var messageErrorFormatoNIE="";
var messageErrorLetraNIF="";
var messageErrorLetraNIE="";	
	
	function cuentaChar()
	{
		var valor=document.getElementById("comentarios").value;
		if(valor.length>2048) 
		{
			alert("Ha llegado al l�mite de tama�o de texto");
			document.forms[0].comentarios.value=valor.substring(0,2048);
		}
	}
	
	function cambiar(cual) {

		limpiarCampos();
		document.ContactenosPreAccesoForm.numeroPoliza.value="";
		tipoDocumentoSeleccionado();
		document.getElementById("nPoliz").innerHTML = '';
		
		if (cual=="si"){
			document.getElementById("segApellido").style.visibility = "visible";
			document.getElementById("docum").style.display="block";
			document.getElementById("msgPoliza").style.display="block";			
			
			document.getElementById("tipoProducto").disabled=false;
			document.forms[0].tipoProducto.options[0].selected=true;	
			document.ContactenosPreAccesoForm.numeroPoliza.disabled=false;
		
			if (document.ContactenosPreAccesoForm.nif.value == "ANONIMO"){
				document.ContactenosPreAccesoForm.nif.value = "";
			}

		} 
        else {
			document.getElementById("docum").style.display="none";
			document.getElementById("msgPoliza").style.display="none";
			document.getElementById("segApellido").style.visibility="hidden";
			document.getElementById("tipoProductoOIM").style.visibility = "hidden";
			document.getElementById("tipoProductoOIM").disabled=true;
			document.getElementById("tipoProductoOIMId").style.display="none";
			
			if (document.getElementById("tipoProductoMAPFRE") != null) {
				document.getElementById("tipoProductoMAPFRE").style.visibility = "hidden";
				document.getElementById("tipoProductoMAPFRE").disabled=true;	
				document.getElementById("tipoProductoMAPFREId").style.display="none";								
			}
			
			document.ContactenosPreAccesoForm.nif.value = "ANONIMO";			
			document.forms[0].tipoProducto.options[0].selected=true;
			document.ContactenosPreAccesoForm.numeroPoliza.disabled=true;
			document.getElementById("nombre").disabled=false;
			document.getElementById("apellido1").disabled=false;
			document.getElementById("apellido2").disabled=false;
			document.getElementById("codigoPostal").disabled=false;						
		}
	}
	
	function iniciando(){
		try{
			pintaropcion('8');
		}
		catch(exception){}
		
		try{
			calendario();
		}
		catch(exception){}
	
	}

	function validarNif() {
				
		/* El pasaporte no se valida */
		if (document.forms[0].tipoDocumento.value != "3" && 
			document.forms[0].nif.value != "" &&
			document.forms[0].nif.value != "ANONIMO"
			) 
		{					
			document.body.style.cursor = "wait";			
			document.forms[0].nif.value = rellenaNif(document.forms[0].nif.value);
			document.getElementById("dynamicFrame").src = "ValidacionDocumentoAction.do?documento=" + document.forms[0].nif.value + "&tipoDocumento=" + document.forms[0].tipoDocumento.value;			
			document.body.style.cursor='default';
		}
	}
	
	function rellenaNif(nif) {
	
		if (nif.length == 1) 
			nif = "000000000" + nif;
		else if (nif.length == 2) 
			nif = "00000000" + nif;
		else if (nif.length == 3) 
			nif = "0000000" + nif;
		else if (nif.length == 4) 
			nif = "000000" + nif;
		else if (nif.length == 5) 
			nif = "00000" + nif;
		else if (nif.length == 6) 
			nif = "0000" + nif;
		else if (nif.length == 7) 
			nif = "000" + nif;
		else if (nif.length == 8) 
			nif = "00" + nif;
		else if (nif.length == 9) 
			nif = "0" + nif;
			
		return nif;
	}	    
	
	function esOIM() {
			
		/* Si se elige la Oificina Internet MAPFRE */	
		if ((document.getElementById("tipoProducto").value == "101 - Oficina Internet MAPFRE" ||
			 document.getElementById("tipoProducto").value == "101 - �rea de Clientes") &&
			 document.ContactenosPreAccesoForm.esCliente.value == 'SI')
		{	
			document.getElementById("tipoProductoOIM").disabled=false;
			document.getElementById("tipoProductoOIM").options[0].selected=true;
			document.getElementById("tipoProductoOIMId").style.display="block";
			document.getElementById("tipoProductoOIM").style.visibility = "visible";			
		}
		else {
			document.getElementById("tipoProductoOIM").style.visibility = "hidden";
			document.getElementById("tipoProductoOIM").disabled=true;			
			document.getElementById("tipoProductoOIMId").style.display="none";
			document.getElementById("tipoProductoMAPFRE").style.visibility = "hidden";
			document.getElementById("tipoProductoMAPFRE").disabled=true;
			document.getElementById("tipoProductoMAPFREId").style.display="none";											
		}
		
		document.getElementById("nPoliz").innerHTML = '';
	}

	function esNoReconocido() {
//		if (document.getElementById("tipoProductoOIM").options[document.getElementById("tipoProductoOIM").selectedIndex].text.toUpperCase().indexOf("RECONOCIDO")!=-1 &&
//			document.getElementsByName('rclienteOIM')[0].checked == true) 
		if (document.getElementById("tipoProductoOIM").options[document.getElementById("tipoProductoOIM").selectedIndex].text.toUpperCase().indexOf("RECONOCIDO")!=-1) 
		{				
			document.getElementById("numeroPoliza").disabled=false;
			document.getElementById("msgPoliza").style.display="block";
		}
		else {
			document.getElementById("numeroPoliza").disabled=true;
			document.getElementById("msgPoliza").style.display="none";										
		}
	}
	
	function habilitarCampos() {	
		document.getElementById("nombre").disabled=false;
		document.getElementById("apellido1").disabled=false;
		document.getElementById("apellido2").disabled=false;	
	}
	

	function limpiarCampos() {
		document.getElementById("nif").value = "";
		document.getElementById("nombre").value = "";
		document.getElementById("apellido1").value = "";
		document.getElementById("apellido2").value = "";
		document.getElementById("direccionCorreo").value="";
		document.getElementById("comentarios").value="";
		document.getElementById("repiteDireccionCorreo").value="";	
		document.getElementById("tipoProductoOIM").selectedIndex = 0;	
	}
		
	function cambioDocumento() {
		limpiarCampos();
		
		/* Si es pasaporte no se valida y activamos los campos */
		if (document.forms[0].tipoDocumento.value != "3"){ 
			habilitarCampos();	
		}
	}	
		
	/* Funcion para eliminar todos los elementos de un select */
	function clearSelect(select) {
		if (select.length != 0) { 
			var qty = select.length;
	    	for (var i=0; i<qty; i++) { 
	      		select.options[0] = null;
	    	}
	    select.selectedIndex = 0;
		}
	}

	
	function validarMail(pEmail) { 

		if(pEmail.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) == -1) {
	   		return false;
		}
		
		return true;
	}
	
	function validarCodigoPostal (cp){
		
		var codProvincia="";
		
		if (cp.length==0){
			return false;
		} else {
			if (cp.length==5){
				codProvincia = cp.substring(0,2);
				if (!((0 < parseFloat(codProvincia)) && (parseFloat(codProvincia) < 53)))  {
					return false;
				}
			}else {
				return false;
			}
		}
		return true;
	}
	// Nueva funci�n que valida NIE con letras X,Y y Z
	function comprobarDocumento (tipoDocumento,documento){
	
		//Se eliminan en primer lugar los caracteres no alfanum�ricos del nif/nie
		documento = eliminarNoAlfaNum(documento);
		
		//Rellenamos con ceros por la izquierda
		documento = rellenaCeros(documento).toUpperCase();
		
		 //Se comprueba si es nif o nie, verificando si hay una X en la cadena introducida
  		var checkNifNie = "";
  		
  		
  		var indice_X = null;
  		var pre = null;
  		if (documento.indexOf("X")>0 && documento.indexOf("X")!=documento.length-1){
  			indice_X = documento.indexOf("X");
  			pre = "0";
  		}else if(documento.indexOf("Y")>0 && documento.indexOf("Y")!=documento.length-1){
  			indice_X = documento.indexOf("Y");
  			pre = "1";
  		}else if(documento.indexOf("Z")>0 && documento.indexOf("Z")!=documento.length-1){
  			indice_X = documento.indexOf("Z");
  			pre = "2";
  		}
  		
  		
		var firstNie = "";
		var secondNie = "";
  
		if(indice_X == null){
		  	checkNifNie = "nif";
		}else{
		  	checkNifNie = "nie";
		  	if(indice_X > 0){
	    	    firstNie= documento.substr(0,indice_X);
	        }
	        secondNie = documento.substr(indice_X+1,documento.length);
  	
  		}
  		
  		document.forms[0].nif.value= documento;
  		
  		if(tipoDocumento ==0 && !validarFormatoNif(documento)){
  			alert(messageErrorFormatoNIF);
		  	return false;	
		}else if(tipoDocumento ==0 && !validarLetraNif(documento)){
			alert(messageErrorLetraNIF);
			return false;	
		}else if(tipoDocumento ==2 && !validarFormatoNie(documento,firstNie,secondNie,indice_X)){
			alert(messageErrorFormatoNIE);
			return false;	
		}else if(tipoDocumento ==2 && !validarLetraNif(pre+secondNie)){
			alert(messageErrorLetraNIE);
			return false;
		}
		
		return true;
 
	}
	
	/** Funci�n Obsoleta. La validaci�n del NIE es distinta
	
	function comprobarDocumento (tipoDocumento,documento){
	
		//Se eliminan en primer lugar los caracteres no alfanum�ricos del nif/nie
		documento = eliminarNoAlfaNum(documento);
		
		//Rellenamos con ceros por la izquierda
		documento = rellenaCeros(documento).toUpperCase();
		
		 //Se comprueba si es nif o nie, verificando si hay una X en la cadena introducida
  		var checkNifNie = "";
  		var indice_X = documento.indexOf("X");
		var firstNie = "";
		var secondNie = "";
  
		if(indice_X == -1 || indice_X == 9){
		  	checkNifNie = "nif";
		}else{
		  	checkNifNie = "nie";
	  	if(indice_X > 0){
    	    firstNie= documento.substr(0,indice_X);
        }
        secondNie = documento.substr(indice_X+1,documento.length);
  	
  		}
  		
  		document.forms[0].nif.value= documento;
  		
  		if(tipoDocumento ==0 && !validarFormatoNif(documento)){
  			alert(messageErrorFormatoNIF);
		  	return false;	
		}else if(tipoDocumento ==0 && !validarLetraNif(documento)){
			alert(messageErrorLetraNIF);
			return false;	
		}else if(tipoDocumento ==2 && !validarFormatoNie(documento,firstNie,secondNie,indice_X)){
			alert(messageErrorFormatoNIE);
			return false;	
		}else if(tipoDocumento ==2 && !validarLetraNif(secondNie)){
			alert(messageErrorLetraNIE);
			return false;
		}
		
		return true;
 
	}
	**/
	function tipoDocumentoSeleccionado() {
		tipoDoc = document.forms[0].tipoDocumento.value;
		imgRequerido ='<span class="necesario" id="apellido2Necesario">*</span>';
		imgNoRequerido ='<span class="necesario" id="apellido2Necesario">&nbsp;</span>';		
		if (((tipoDoc == 2)||(tipoDoc == 3)) && (document.forms[0].esCliente.value =='SI')){
			document.getElementById("apel2Required").style.display = "none";
			//document.getElementById("segApellido").innerHTML = imgNoRequerido;
		}else {
			document.getElementById("apel2Required").style.display = "inline";
			//document.getElementById("segApellido").innerHTML = imgRequerido;
		}
	}
	
	function recogeOrdenElementosContactoE(oForm) {
		var sElementos = "";
		var obj = "document."+oForm;
		
		for(i=0; i<obj.length; i++) {
	
			if (document.ContactenosPreAccesoForm[i] != null) {		
				if( (document.ContactenosPreAccesoForm[i].type != "hidden") && (document.ContactenosPreAccesoForm[i].name != undefined) && (sElementos.indexOf(document.ContactenosPreAccesoForm[i].name) == -1) ) {
					if (document.ContactenosPreAccesoForm[i].type != "radio") {
						if ( !document.ContactenosPreAccesoForm[i].disabled){
							sElementos += document.ContactenosPreAccesoForm[i].name + "$";
						}	
					}	
				} else {
					if( (document.ContactenosPreAccesoForm[i].type == "hidden") && (document.ContactenosPreAccesoForm[i].id == "TITULO") ) {
						sElementos += document.ContactenosPreAccesoForm[i].name + "$";
					}
					if( (document.ContactenosPreAccesoForm[i].type == "hidden") && (document.ContactenosPreAccesoForm[i].id == "MAILRESP") ) {
						sElementos += document.ContactenosPreAccesoForm[i].name + "$";
					}
				}
			}
			else {
				break;
			}
			
		}
		
		sElementos = sElementos.substring(0, sElementos.lastIndexOf("$"));
		document.ContactenosPreAccesoForm.ordenElementos.value = sElementos;	
	}

/*	function inicioDiv(){

		// Preprando los div
		// Desahabilitamos todos los componentes
		document.getElementById('tipoDocumento').disabled= true;
		document.getElementById('nif').disabled= true;
		document.getElementById('tipoProductoOIM').disabled= true;
		document.getElementById('numeroPoliza').disabled = true;

		
		document.getElementById('clienteOIM').style.display = 'block';
		document.getElementById('noClienteOIM1').style.display = 'block';
		document.getElementById('divFormulario').style.display = 'none';
		document.getElementById('noClienteOIM2').style.display = 'none'; 
		

		document.getElementById('msgPoliza').style.display='none';
		if (document.getElementById('esCliente').value == 'SI'){
			controlDiv(1);
			document.getElementsByName('rclienteOIM')[0].checked=true;
			esNoReconocido();
			
		}else if (document.getElementById('esCliente').value == 'NO'){
			controlDiv(2);
			document.getElementsByName('rclienteOIM')[1].checked=true;
		
		}
	}	
*/	
	function inicioDiv(){
		document.getElementById('msgPoliza').style.display='none';
	}	

	function controlDiv(accion){

		document.getElementById('btnEnviar').disabled = false;
		if (accion=='1'){
			document.getElementById('esCliente').value = 'SI';
			document.getElementById('clienteOIM').style.display = 'block';
			document.getElementById('noClienteOIM1').style.display = 'none';
			document.getElementById('divFormulario').style.display = 'block';
			document.getElementById('divNIF').style.display = 'block'; 
			document.getElementById('divTipoConsulta').style.display = 'block'; 			
			document.getElementById('noClienteOIM2').style.display = 'block';
			document.getElementById('apellido2Necesario').innerHTML = '*';
			document.getElementById('tipoDocumento').disabled= false;
			document.getElementById('nif').disabled= false;
			document.getElementById('tipoProductoOIM').disabled= false;
			esNoReconocido();
		
		}else if (accion='2'){
			document.getElementById('esCliente').value = 'NO';
			document.getElementById('clienteOIM').style.display = 'block';
			document.getElementById('noClienteOIM1').style.display = 'block';
			document.getElementsByName('rclienteOIM')[1].checked=true;
			document.getElementById('divFormulario').style.display = 'block';
			document.getElementById('divNIF').style.display = 'none'; 
			document.getElementById('divTipoConsulta').style.display = 'none'; 			
			document.getElementById('noClienteOIM2').style.display = 'none';
			document.getElementById('apellido2Necesario').innerHTML = '&nbsp;';
			document.getElementById('msgPoliza').style.display='none';
			document.getElementById('tipoDocumento').disabled= true;
			document.getElementById('nif').disabled= true;
			document.getElementById('tipoProductoOIM').disabled= true;
			document.getElementById('numeroPoliza').disabled = true;
		}
	
	
	
	
	}	
	
	
	 