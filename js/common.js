var colorFillHover = "d81e05";
var colorFillSelected = "568ae5";


function loadMapeo(){
	var MIN_WINDOW_WIDTH = 800;
	if($(window).width() >= MIN_WINDOW_WIDTH){
		$(function() {
			if (esmovil()) {
				$.fn.maphilight.defaults = {
					fill:true,
					fillOpacity: 0.5,
					stroke:true,
					strokeColor:colorFillSelected,
					strokeOpacity:0.5,
					strokeWidth:1,
					fade:true
				}
			} else {
				$.fn.maphilight.defaults = {
					fill:true,
					fillColor:colorFillHover,
					fillOpacity: 0.5,
					stroke:true,
					strokeColor:colorFillSelected,
					strokeOpacity:0.5,
					strokeWidth:1,
					fade:true
				}
			}
			$('img[usemap]').maphilight();
		});
	}
}

loadMapeo();

function cambiaVisibilidadArea(elem){
	var data = elem.mouseout().data('maphilight') || {};
	data.alwaysOn = !data.alwaysOn;
	if (esmovil()) {
		data.fillColor = data.alwaysOn ? colorFillSelected : {};
	} else {
		data.fillColor = data.alwaysOn ? colorFillSelected : colorFillHover;
	}
	elem.data('maphilight', data).trigger('alwaysOn.maphilight');

	if(!/(iPad|Android)/.test(navigator.userAgent) && !window.mobilecheck()){
		elem.trigger('mouseenter');
	}
}

function getIdentificadorPadre(elem){
	var padre = elem.parentsUntil(".total", ".seleccion");
	var clases = padre.attr("class");
	var id = clases.substring(clases.indexOf('tab'), clases.indexOf('tab')+4);
	return id;
}

function actualizaContador(elem){
	var id = getIdentificadorPadre(elem);
	var contador = $("#"+id+" .contador");

	var valorContador = parseInt(contador.html());

	if(elem.is(":checked")){
		valorContador += 1;
		if(valorContador > 0){
			contador.removeClass("hide");
		}
	} else {
		valorContador -= 1;
		if(valorContador == 0){
			contador.addClass("hide");
		}
	}
	contador.html(valorContador);
}

function actualizarListaResumen(elem){
	var id = getIdentificadorPadre(elem);
	var texto = $('label[for="'+elem.attr('id')+'"]').html();
	var vista = $(".vistas .menu-tabs #"+id+" .vista").html();

	if(elem.is(":checked")){ // Marco un elemento
		$(".selected-pieces").removeClass("hide");
		if($(".selected-pieces dl").length	> 0){ // Existe la lista de vistas
			if(!$(".selected-pieces dl dt."+id).length > 0){ // No existe la vista
				$(".selected-pieces dl").append("<dt class='"+id+"'>"+vista+"</dt><dd><span>"+texto+"</span></dd>");
			} else { // Existe la vista
				if($(".selected-pieces dl dt."+id+" + dd ul").length > 0){ // Existe la lista de piezas
					$(".selected-pieces dl dt."+id+" + dd ul li:last-child").removeClass("last");
					$(".selected-pieces dl dt."+id+" + dd ul").append("<li class='last'>"+texto+"</li>");
				} else { // No existe la lista de piezas, cambio el span por una lista y añado la nueva opcion
					var tmp = $(".selected-pieces dl dt."+id+" + dd span").html();
					$(".selected-pieces dl dt."+id+" + dd")
						.append("<ul><li>"+tmp+"</li><li class='last'>"+texto+"</li></ul>")
						.children("span").remove();
				}
			}
		} else { // No existe la lista de vistas 
			$(".selected-pieces").append("<dl><dt class='"+id+"'>"+vista+"</dt><dd><span>"+texto+"</span></dd></dl>");
		}
	} else { // Desmarco un elemento
		if($(".selected-pieces dl dt."+id+" + dd ul").length > 0) { // Es un elemento de una lista
			$.each($(".selected-pieces dl dt."+id+" + dd ul li"), function(){
	 			if($(this).html() == texto){
 					$(this).remove();
 					return false;
 				}
			});
			if($(".selected-pieces dl dt."+id+" + dd ul li").length	== 1){ // Queda un solo elemento, lo cambio a un span
				$(".selected-pieces dl dt."+id+" + dd").append("<span>"+$(".selected-pieces dl dt."+id+" + dd ul li").html()+"</span>");
				$(".selected-pieces dl dt."+id+" + dd ul").remove();
			} else {
				$(".selected-pieces dl dt."+id+" + dd ul li.last").removeClass("last");
				$(".selected-pieces dl dt."+id+" + dd ul li:last-child").addClass("last");
			}

		} else { // No es un elemento de una lista
			$(".selected-pieces dl dt."+id+" + dd").remove(); // Borro el listado de piezas
			$(".selected-pieces dl dt."+id).remove(); // Borro la vista

			if($(".selected-pieces dl dt").length	== 0){ // No quedan vistas
				$(".selected-pieces dl").remove(); // Borro el listado de vistas
				$(".selected-pieces").addClass("hide"); // Oculto el resumen de las piezas seleccionadas
			}
		}
	}
}


function actualizarCeckAll(elem){
	var id = getIdentificadorPadre(elem);
	var marcar = true;

	var checkAll = $(".seleccion."+id+" .selAll input[type=checkbox]");

	$.each($(".seleccion." + id + " .secciones ul input[type=checkbox]"), function(i){
		if(!$(this).is(":checked")){
			marcar = false;
			return false;
		}
	});
	checkAll.prop("checked", marcar);
}

function actualizarLabel(elem){
	$("label[for='"+elem.prop('id')+"']").parentsUntil("ul", "li").toggleClass("selected");
	if(!(!/(iPad|Android)/.test(navigator.userAgent) && !window.mobilecheck())){
		$("label[for='"+elem.prop('id')+"']").parentsUntil("ul", "li").addClass("dispositivoMovil");

	}
}

function tareasSeleccion(elem){
	actualizaContador(elem);
	actualizarListaResumen(elem);
	actualizarCeckAll(elem);
	actualizarLabel(elem);
}

function seleccionChecks(lista, marcar){
	$.each(lista, function(){
		if(marcar){
			if(!$(this).is(":checked")){
				$(this).trigger('click');
			}
			$(this).trigger('mouseleave');
		} else {
			if($(this).is(":checked")){
				$(this).trigger('click');
			}
			$(this).trigger('mouseleave');
		}
	});
}

$(document).ready(function(){

	// Control menu secciones de coche
	$(".menu-tabs > li").on('click', function(e){
		// Opcion de menu lateral activa
		$("[id *= tab]").removeClass("active");
		$("#"+this.id).addClass("active");
		// Seccion del coche seleccionada visible
		$("div[class *= tab]").css("display", "none");
		$("div."+this.id).css("display", "inline-block");
		// Activo el resaltado de mapa de la seccion de coche seleccionada
		$(".seleccion." + this.id + " img[usemap]").maphilight();
		return false;
	});

	// Seleccion de areas en el mapa
	$(".seleccion area")
		.on('click', function(e){
			if (!esmovil()) {
				e.preventDefault();
				cambiaVisibilidadArea($(this));
				// Marco/Desmarco el checkbox asociado
				$("#" + this.id + "Chk").prop('checked', !$("#" + this.id + "Chk").prop('checked'));

				tareasSeleccion($("#" + this.id + "Chk"));
			}
		})
		.on('mouseover', function(){});

	// Seleccion de checkboxes y comportamiento hover
	$(".seleccion .secciones ul input[type=checkbox]")
		.on('click', function(e){
			var idArea = this.id.substring(0, this.id.length - 3);
			cambiaVisibilidadArea($("#"+idArea));

			tareasSeleccion($(this));

		})
		.on('mouseenter', function(e){
			var idArea = this.id.substring(0, this.id.length - 3);
			$('#'+idArea).mouseover();
		})
		.on('mouseleave', function(e){
			var idArea = this.id.substring(0, this.id.length - 3);
			$('#'+idArea).mouseout();
		});

	$(".seleccion .secciones label")
		.on('mouseenter', function(e){
			var check = $(this).prop('for');
			$('#'+check).trigger('mouseenter');
		})
		.on('mouseleave', function(e){
			var check = $(this).prop('for');
			$('#'+check).trigger('mouseleave');
		});


	// Check seleccionado
	// Lista
	$(".seleccion .secciones h3 input[type=checkbox]").on('click', function(){
		var padre = $(this).parentsUntil(".seleccion", ".secciones");
		var marcar = $(this).is(":checked");

		seleccionChecks(padre.find("ul input[type=checkbox]"), marcar);
	});


	/* lista movida en tablet */
	if (esmovil()) {
		$.each($(".interior #principal #main .secciones-vistas .seleccion"), function(){
			var lista = $(this).find(".secciones");
			var p = $(this).find("p").first();
			lista.insertAfter(p);
			$(this).css("padding-bottom", "1.5em");
		});
	}

});

$(window).on('resize', function(){
//	loadMapeo();
});
