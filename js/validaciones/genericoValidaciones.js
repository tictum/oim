// Variables que intervienen en la comprobacion del formato en el cual se generara el contrato (ReconocimientoFinProceso.jsp)
	var agt=navigator.userAgent.toLowerCase();
	var ie = (agt.indexOf("msie") != -1);
	var ns = (navigator.appName.indexOf("Netscape") != -1);
	var win = ((agt.indexOf("win")!=-1) || (agt.indexOf("32bit")!=-1));
	var mac = (agt.indexOf("mac")!=-1);
	var control = "false";
	var controlFnmt=false;



// Funciones que intervienen el procedimientoIncorrectoPendienteFirma

	function cambiarEstadoRadioButton(pObjeto)
	{
		if (pObjeto.name == 'solicitudDuplicado' && pObjeto.checked)
		{
			if (document.getElementById('descargaImpresion').checked)
			{
				document.getElementById('descargaImpresion').checked = "";	
				pObjeto.checked = "checked";
				return;
			}
			else if (!document.getElementById('descargaImpresion').checked)
			{
				document.getElementById('descargaImpresion').checked = "checked";
				pObjeto.checked = "";
			}	
		}
		
		if (pObjeto.name == 'descargaImpresion' && pObjeto.checked)
		{
			if (document.getElementById('solicitudDuplicado').checked)
			{
				document.getElementById('solicitudDuplicado').checked = "";	
				pObjeto.checked = "checked";
				return;
			}
			else if (!document.getElementById('solicitudDuplicado').checked)
			{
				document.getElementById('solicitudDuplicado').checked = "checked";
				pObjeto.checked = "";
				
			}			
		}
		if (pObjeto.name == 'reenvioCorreo' && pObjeto.checked)
		{
			if (document.getElementById('cambioCorreo').checked)
			{
				document.getElementById('cambioCorreo').checked = "";	
				pObjeto.checked = "checked";
				return;
			}
			else if (!document.getElementById('cambioCorreo').checked)
			{
				document.getElementById('cambioCorreo').checked = "checked";
				pObjeto.checked = "";
			}	
		}
		
		if (pObjeto.name == 'cambioCorreo' && pObjeto.checked)
		{
			if (document.getElementById('reenvioCorreo').checked)
			{
				document.getElementById('reenvioCorreo').checked = "";	
				pObjeto.checked = "checked";
				return;
			}
			else if (!document.getElementById('reenvioCorreo').checked)
			{
				document.getElementById('reenvioCorreo').checked = "checked";
				pObjeto.checked = "";
				
			}			
		}
	}
	
	function enviar()
	{
		if (document.getElementById('solicitudDuplicado').checked)
		{
			document.location.href = "./SolicitudDuplicadoContratoAction.do";
		}
		else if (document.getElementById('descargaImpresion').checked)
		{	
			document.location.href = "./ImpresionContratoAltaAction.do?formato=0&accesoOIM=0";
		}
	}
	
	function enviarSolicitudDuplicado()
	{
		document.location.href = "./ValidarIdentificacionAction.do";
	}
	
	function comprobarFormato(comprobar)
	{
		if (comprobar)
		{
			if (detectarFormatoContrato())
			{
				formato = "pdf";
				
			}
			else
			{
				formato = "html";
			}
			document.location.href="./ImpresionContratoAltaAction.do?formato=" + formato + "&accesoOIM=1&compFormato=false";
			
		}
		
	}
	
	function detectarPdf()
	{
        if (ie && win) 
        { 
            pluginlist = detectIE("PDF.PdfCtrl.4","Acrobat Reader") + detectIE("PDF.PdfCtrl.5","Acrobat Reader") + detectIE("PDF.PdfCtrl.6","Acrobat Reader") + detectIE("AcroPDF.PDF.1","Acrobat Reader"); 
            return pluginlist;
        }
        if (ns || !win) 
        {
			nse = ""; 
            for (var i=0;i<navigator.mimeTypes.length;i++) 
            {
                nse += navigator.mimeTypes[i].type.toLowerCase();
			}
            pluginlist = detectNS("application/pdf","Acrobat Reader");
			return pluginlist;

        }
	
	}
	 
	function detectIE(ClassID,name) 
	{ 
		result = false;
		document.write("<SCRIPT LANGUAGE=VBScript>\n on error resume next \n result = IsObject(CreateObject(\"" + ClassID + "\"))<\/SCRIPT>"); 
	
	    if (result)
	    { 
			return name+','; 
		}
		else
		{
			return ''; 
		}
	}
	
	function detectNS(ClassID,name) 
	{ 
		n = ""; 
		if (nse.indexOf(ClassID) != -1) 
		{
			if (navigator.mimeTypes[ClassID].enabledPlugin != null) 
			{
	         	n = name+","; 
			}
	        return n; 
		}
	}
	
	function detectarFormatoContrato()
	{
		if( ! detectarPdf())
		{
			pdf = "false";
		}
		else
		{
			pdf = "true";
		}
		return pdf;
	}
	
	
// Funciones que interviene en la ValidacionSolicitudExistente

	function codigoPostal()
	{
		var strCP = document.forms[0].txtCP.value;
		 if (strCP.length!=5)
		{
			//alert("Debe introducir un c�digo postal v�lido");
			return;
		}
		strCP = strCP.substring(0,2);
		if(seleccionarCombo2(document.forms[0].cboProvincia, strCP)){
			document.forms[0].txtProvincia.value=document.forms[0].cboProvincia.options[document.forms[0].cboProvincia.selectedIndex].text;	
		}else{
			document.forms[0].txtProvincia.value="";
		}
	}
	
	function borrar()
	{	
		if (document.getElementById('txtCP').value == "")
		{
			if(document.forms[0].txtProvincia.value != "")
			{
				document.forms[0].txtProvincia.value="";
				
			}
		}
	}
	
	function muestraCampoObligatorio()
	{	
		if ((document.getElementById("cboTipoDoc").options[document.getElementById("cboTipoDoc").selectedIndex].text).indexOf("N.I.F")==-1)
		{
			document.getElementById("apellido2").style.visibility = "hidden";
		}
		else
		{
			document.getElementById("apellido2").style.visibility = "visible";
		}
	}
	
// Funciones que intervienen en el reenvio de correo electronico de activacion
	function enviarSolicitudReenvioCorreo()
	{
		if (document.getElementById('cambioCorreo').name == 'cambioCorreo' && document.getElementById('cambioCorreo').checked)
		{
			document.forms[0].hidControlCambioDatos.value = "true";
		}
		document.forms[0].submit();
	}
	
	function recargarEstadoRadio(estadoCombo)
	{
		if (estadoCombo)
		{
			document.getElementById('cambioCorreo').checked = "checked";
			document.getElementById('reenvioCorreo').checked = "";
		}
		else
		{
			document.getElementById('cambioCorreo').checked = "";
			document.getElementById('reenvioCorreo').checked = "checked";
		}
	}

