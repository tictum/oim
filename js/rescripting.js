var accessibility_to_shadowbox = function(){

	var tmp,
		contador = 1,
		opLink = null;
		SB_CONTAINER = "sb-container",
		SB_TITLE_INNER = "sb-title-inner",
		SB_PLAYER = "sb-player";

	function init(){

		document.onkeydown = _teclado;
		_shadowLinks();
	}

	function addFunctionsToClose(){
		var close_element = document.getElementById("sb-nav-close");
		close_element.onclick = function(){
			Shadowbox.close();
			hideBox();
			restoreFocus();
		};
	}

	function correct() {
		tmp = setInterval(function(){
			_addTitle();
		},2000);
	}
	function hideBox(){
		var shadowbox_container = document.getElementById(SB_CONTAINER);
		if(shadowbox_container) shadowbox_container.style.display = "none";
	}
	function restoreFocus(){
		if(opLink){
			var nav = document.getElementsByTagName("a"),
				navLen = nav.length;
			for(var i = 0; i < navLen; i++){
				if(opLink === nav[i]) nav[i].focus();
			}
			opLink = null;
		}
	}
	function _addTitle(){
		var shadowbox_content = document.getElementById(SB_PLAYER),
			shadowbox_content_nodeName, shadowbox_title_inner, shadowbox_title_inner_html;

		if(contador) clearInterval(tmp);
		if(shadowbox_content){
			shadowbox_content_nodeName = shadowbox_content.nodeName;
			contador = 1;
			shadowbox_title_inner = document.getElementById(SB_TITLE_INNER);
			shadowbox_title_inner_html = shadowbox_title_inner.innerHTML;
			if(shadowbox_content_nodeName.toLowerCase() === "img"){
				shadowbox_content.setAttribute("alt",shadowbox_title_inner_html);
			}else{
				shadowbox_content.setAttribute("title",shadowbox_title_inner_html);
			}
			if(shadowbox_content_nodeName.toLowerCase() === "iframe"){
				shadowbox_content.contentWindow.onkeydown = _teclado;
			}
		}else{
			contador = 0;
		}

	}
	function _shadowLinks() {
		var nav = document.getElementsByTagName("a"),
			shadowLink = [], i;
		for(i = 0; i < nav.length; i++){
			if(nav[i].getAttribute("rel"))
				if(nav[i].getAttribute("rel").indexOf("shadowbox") >= 0){
					shadowLink.push(nav[i]);
				}
		}
		for(i = 0; i < shadowLink.length; i++){
			shadowLink[i].onclick = function(){
				var shadow = document.getElementById(SB_CONTAINER),
					parentt;
				opLink = this;
				parentt = opLink.parentNode;
				shadow.style.display = "";
				//parentt.insertBefore(shadow, parentt.childNodes[0] || null);
				//parentt.appendChild(shadow.cloneNode(true));
				correct();
			};
		}
	}
	function _teclado(event){
		var SB = Shadowbox,
			SL = SB.lib, key;
		if (!event) var event = window.event;
		if (window.event) key = window.event.keyCode;
		else if (event.which) key = event.which;
		else{
			return true;
		}
		if (!key) return true;
		if(key == 27 || key == 81 || key == 88 || key == 27){
			SB.close();
		}else if(key == 37){
			SB.previous();
		}else if(key == 39){
			SB.next();
		}else if(key == 32){
			SB[(typeof slide_timer == 'number' ? 'pause' : 'play')]();
		}
	}


	return {
		init:init,
		correct:correct,
		restoreFocus:restoreFocus,
		hideBox:hideBox,
		addFunctionsToClose:addFunctionsToClose
	};

}();
