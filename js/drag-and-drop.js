// JavaScript Document
var order = function(){ 
    var sortable,
        dragAct = true,
        msgAct = "Puede habilitar el orden sin utilizar el rat�n.",
        msgDesact = "Puede habilitar el orden utilizando el rat�n.";
    function init(){
        sortable = $("#sortable");
        sortable.sortable();
        sortable.disableSelection();
        _asignarEventoCambioTipoOrdenacion();
    }
    function enviar() {
        var elementosAOrdenar = $("#sortable").find("li");
        if (dragAct) {
            
                elementosAOrdenar.each(function(idx){
                    var cadena = $(this).attr("idagru");
                    cadena =  cadena+'|';
                    $('#hidCadOrdenacion').val($('#hidCadOrdenacion').val()+cadena);
                 });
                $('#idFormOrdenacion').submit();
        } else {
            var error = false;
            var ordenacion = [];
            elementosAOrdenar.each(function(idx){
                var agrupacion = $(this).attr("idagru");
                var idSelected = $(this).find("select").attr("id");
                var posicionSeleccionada = $('#'+idSelected+' option:selected').text();
                if (ordenacion[posicionSeleccionada]== null ||  ordenacion[posicionSeleccionada]=='' ){
                    ordenacion[posicionSeleccionada] = agrupacion;
                } else {
                    error = true;
                }
            });
            if (error) {
                alert('La ordenaci�n no es correcta');
            
            } else {
                $.each (ordenacion,function(index,elemen){
                    var cadenaOrdenacion ='';
                    if (elemen!= null) {
                        cadenaOrdenacion =  elemen+'|';
                        $('#hidCadOrdenacion').val($('#hidCadOrdenacion').val()+cadenaOrdenacion);
                    }
                });
                $('#idFormOrdenacion').submit();
            }
        }   
    }
    function _asignarEventoCambioTipoOrdenacion(){
        var tipoOrd = $(".cambiarTipoOrdenacion");
            tipoOrd.on("click", function(event){
                event.preventDefault();
                event.stopPropagation();
                if (!dragAct){
                    _borrarCombosDeOrdenacion( $(this) );
                }else{
                    _crearCombosDeOrdenacion( $(this) );
                }
                dragAct = !dragAct;
            });
    }
    function _borrarCombosDeOrdenacion(elemento){
        var elementosAOrdenar = sortable.find("li");
        elemento.removeClass("dragOn").addClass("dragOff");
        elemento.html(msgAct);
        sortable.sortable( "enable" );
        sortable.disableSelection();
        elementosAOrdenar.find("label").remove();
    }
    function isMovil(){
        var elementosAOrdenar =  $("#sortable").find("li"),
        cmb2 = "", i = 0;
        len = elementosAOrdenar.length;
        for(; i < len; i++){
            cmb2 += "<option>"+(i+1)+"</option>";
        }
        sortable.sortable("disable");
        sortable.enableSelection();
        elementosAOrdenar.each(function(idx){
            var cmb1 = "<label for='ord"+idx+"'><select id='ord"+idx+"'>",
                cmb3 = "</select></label>",
                combo = "";
                combo = cmb1 + cmb2 + cmb3;
            $(this).append( $(combo) );
        });
        dragAct = !dragAct;
    }
    function _crearCombosDeOrdenacion(elemento){
        var elementosAOrdenar = sortable.find("li"),
            cmb2 = "", i = 0;
            len = elementosAOrdenar.length,
        elemento.removeClass("dragOff").addClass("dragOn");
        elemento.html(msgDesact);
        for(; i < len; i++){
            cmb2 += "<option>"+(i+1)+"</option>";
        }
        sortable.sortable("disable");
        sortable.enableSelection();
        elementosAOrdenar.each(function(idx){
            
            var cmb1 = "<label for='ord"+idx+"'><select id='ord"+idx+"'>",
                cmb3 = "</select></label>",
                combo = "";
                combo = cmb1 + cmb2 + cmb3,
                $that = $(this),
                vorder = elementosAOrdenar.index($that);
            $that.append( $(combo) );
            $that.find("option").eq(vorder).attr("selected", "selected");

        });
    }
    return {
        init:init,
        enviar:enviar,
        isMovil:isMovil
    };
}();
$(function() {
    order.init();
});
