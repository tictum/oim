	function rellenaCampos(){
		document.forms[0].hidTipDoc.value=document.forms[0].cboTipoDoc.options[document.forms[0].cboTipoDoc.selectedIndex].text;
		document.forms[0].hidTipVia.value=document.forms[0].cboTipoVia.options[document.forms[0].cboTipoVia.selectedIndex].text;
		document.forms[0].hidProvincia.value=document.forms[0].cboProvincia.options[document.forms[0].cboProvincia.selectedIndex].text;
	}
	
	function enviarDatos(){
		rellenaCampos();
		var aleat = Math.random();
		document.EnvioCorreoOIMDesbloqueoActionForm.action = document.EnvioCorreoOIMDesbloqueoActionForm.action + '?'+aleat;		
		document.EnvioCorreoOIMDesbloqueoActionForm.submit();
	}
	
	function codigoPostal(){
		var strCP = document.forms[0].txtCP.value;
		strCP = strCP.substring(0,2);
		if(seleccionarCombo2(document.forms[0].cboProvincia, strCP)){
			document.forms[0].txtProvincia.value=document.forms[0].cboProvincia.options[document.forms[0].cboProvincia.selectedIndex].text;	
		}else{
			document.forms[0].txtProvincia.value="";
		}
	}
		
	function recogeOrdenElementos(oForm) {
		var sElementos = "";
		var obj = eval("document." + oForm);

		for(i=0; i<obj.length; i++) {
			if( (obj.elements[i].type != "hidden") && (sElementos.indexOf(obj.elements[i].name) == -1) ) {
				sElementos += obj.elements[i].name + "$";
			} else {
				if( (obj.elements[i].type == "hidden") && (obj.elements[i].id == "TITULO") ) {
					sElementos += obj.elements[i].name + "$";
				}
			}
		}
	
		sElementos = sElementos.substring(0, sElementos.lastIndexOf("$"));
		obj.ordenElementos.value = sElementos;	
	}
	
	function desactivar(){
		document.forms[0].txtFechaNacimiento1.disabled=true;
		document.forms[0].txtFechaNacimiento2.disabled=true;
		document.forms[0].txtFechaNacimiento3.disabled=true;
		document.forms[0].txtNombre.disabled=true;
		document.forms[0].txtApellido.disabled=true;
		document.forms[0].txtApellido2.disabled=true;
		document.forms[0].cboTipoDoc.disabled=true;
		document.forms[0].txtNumDocumento.disabled=true;
	}
	
	function segundoApellidoObligatorio() {
		tipoDoc = document.forms[0].cboTipoDoc.value;
		imgRequerido ='<span class="lynd">*</span>';
		if (tipoDoc == 0){
			document.getElementById("segApellido").innerHTML = imgRequerido;
		}else {
			document.getElementById("segApellido").innerHTML = '';
		}
	}