function formatearIBAN(iban, bloque_iban) {
	
	if (iban != null && iban != '' && bloque_iban != null) {
		iban = 	iban.substring( 0, 4) + ' ' + 
				iban.substring( 4, 8) + ' ' + 
				iban.substring( 8,12) + ' ' + 
				iban.substring(12,16) + ' ' + 
				iban.substring(16,20) + ' ' + 
				iban.substring(20,24);
				
		 bloque_iban.appendChild(document.createTextNode(iban)); 
	}		
}

function formatearIBAN2(iban, bloque_iban) {
	
	if (iban != null && iban != '' && bloque_iban != null) {
		iban = 	'IBAN' + ' ' + 
				iban.substring( 0, 4) + ' ' + 
				iban.substring( 4, 8) + ' ' + 
				iban.substring( 8,12) + ' ' + 
				iban.substring(12,16) + ' ' + 
				iban.substring(16,20) + ' ' + 
				iban.substring(20,24);
				
		 bloque_iban.appendChild(document.createTextNode(iban)); 
	}		
}

function formatearIBAN3(iban, bloque_iban) {
	
	if (iban != null && iban != '' && bloque_iban != null) {
		iban = 	'IBAN:' + ' ' + 
				iban.substring( 0, 4) + ' ' + 
				iban.substring( 4, 8) + ' ' + 
				iban.substring( 8,12) + ' ' + 
				iban.substring(12,16) + ' ' + 
				iban.substring(16,20) + ' ' + 
				iban.substring(20,24);
				
		 bloque_iban.appendChild(document.createTextNode(iban)); 
	}		
}

/*function prefijoES(prefijo) {
	if (isNaN(parseInt(String.fromCharCode(event.keyCode)))) {
		event.returnValue = false;
	} else if (prefijo.value != null && 
			   prefijo.value != '' && 
			   prefijo.value.length > 2 && 
			   !(isNaN(prefijo.value.substring(2,prefijo.value.length)))) {
		prefijo.value = 'ES' + prefijo.value.substring(2,prefijo.value.length);
	} else {
		prefijo.value = 'ES';
	}
}*/

function prefijoES(campo) {
	if (campo.value.length < 4) {
		if (campo.value.length == 0) {
			if (String.fromCharCode(event.keyCode) == 'E' || String.fromCharCode(event.keyCode) == 'e')  {
				campo.value = 'E';
			}
		} else if (campo.value.length == 1) {
			if (String.fromCharCode(event.keyCode) == 'S' || String.fromCharCode(event.keyCode) == 's')  {
				campo.value = 'ES';
			}
		} else {
			if (!isNaN(parseInt(String.fromCharCode(event.keyCode)))) {
				campo.value = campo.value + String.fromCharCode(event.keyCode).toUpperCase();
			}
		}
	}
	event.returnValue = false;
}

function prefijoESUp(prefijo) {
	if (prefijo.value != null && prefijo.value != '' ) {
		if (prefijo.value.length == 1 && !(isNaN(prefijo.value))) {
			prefijo.value = 'ES' + prefijo.value;
		} 		
	}
}

function trocearIBAN(iban) {
	if (iban != null && iban != '') {
		document.getElementById('iban1').value = iban.substring( 0, 4); 
		document.getElementById('iban2').value = iban.substring( 4, 8); 
		document.getElementById('iban3').value = iban.substring( 8,12); 
		document.getElementById('iban4').value = iban.substring(12,16); 
		document.getElementById('iban5').value = iban.substring(16,20); 
		document.getElementById('iban6').value = iban.substring(20,24);
	}
}

function validarIBAN() {
	var dcIban  = document.forms[0].iban1.value;
	var banco   = document.forms[0].iban2.value;
	var oficina = document.forms[0].iban3.value;
	var dc      = document.forms[0].iban4.value;
	var cuenta  = document.forms[0].iban5.value + document.forms[0].iban6.value;
	
	cuenta = dc.substring(2,4) + cuenta;
	dc = dc.substring(0,2);
	
	return validarDcIban(dcIban, banco, oficina, dc, cuenta)
}

function validarIBAN2() {
	var dcIban  = document.getElementById('iban1').value;
	var banco   = document.getElementById('iban2').value;
	var oficina = document.getElementById('iban3').value;
	var dc      = document.getElementById('iban4').value;
	var cuenta  = document.getElementById('iban5').value + document.getElementById('iban6').value;
	
	cuenta = dc.substring(2,4) + cuenta;
	dc = dc.substring(0,2);
	
	return validarDcIban(dcIban, banco, oficina, dc, cuenta)
}

function ocultarMostrarCapasIBAN() {
	if (document.getElementById('conoceIBANSi').checked) {
		blockNone('capaIBAN', 'block');
		blockNone('capaCCC', 'none');
	} else if (document.getElementById('conoceIBANNo').checked) {
		blockNone('capaCCC', 'block');
		blockNone('capaIBAN', 'none');
	}
	
}

function ocultarMostrarCapasIBAN2() {
	if (document.getElementById('conoceIBANSi').checked) {
		blockNone('capaIBAN', 'block');
		blockNone('capaCCC', 'none');
	} else if (document.getElementById('conoceIBANNo').checked) {
		blockNone('capaCCC', 'block');
		blockNone('capaIBAN', 'none');
	}
	
}

function cargaIBANCCCPantalla() {
	if (document.getElementById('conoceIBANSi').checked && 
		document.getElementById('iban2').value != null &&
		document.getElementById('iban2').value != '') {
		
		document.getElementById("banco").value    = document.getElementById('iban2').value;
		document.getElementById("sucursal").value = document.getElementById('iban3').value;
		document.getElementById("dc").value       = document.getElementById('iban4').value.substring(0,2);
		document.getElementById("cuenta").value   = document.getElementById('iban4').value.substring(2,4) + document.getElementById('iban5').value + document.getElementById('iban6').value;		
	
	} else if (document.getElementById('conoceIBANNo').checked &&
			   document.getElementById("banco").value != null &&
			   document.getElementById("banco").value != '') {
		
		var ibanTmp = obtenerDcIban(document.getElementById("banco").value, 
									document.getElementById("sucursal").value, 
									document.getElementById("dc").value,
									document.getElementById("cuenta").value);
		
		document.getElementById('iban1').value = ibanTmp.substring( 0, 4);
		document.getElementById('iban2').value = ibanTmp.substring( 4, 8);
		document.getElementById('iban3').value = ibanTmp.substring( 8,12);
		document.getElementById('iban4').value = ibanTmp.substring(12,16);
		document.getElementById('iban5').value = ibanTmp.substring(16,20);
		document.getElementById('iban6').value = ibanTmp.substring(20,24);
	}
}