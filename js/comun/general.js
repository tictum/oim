/**
  * Comprobaci�n de los campos del formulario de alta de la solicitud
  *
  */

function comprobarCampos( ){
  
  formu =document.forms[0];
  var clave = formu.clave.value;
  var diaFecVenci = formu.diaFecVenci.value;
  var mesFecVenci = formu.mesFecVenci.value;
  var anhoFecVenci = formu.anhoFecVenci.value;
  var nifNie= formu.numDoc.value;
  var diaFec = formu.diaFecNacimiento.value;
  var mesFec = formu.mesFecNacimiento.value;
  var anhoFec = formu.anhoFecNacimiento.value;
  var tfno = formu.tfnoContacto.value;
  var email = formu.email.value;
  var diaFebrero;
  var diaFebreroVenci;  
  var noClave = 0;
  var noNifNie = 0;
  var noFec = 0;
  var noTfno = 0;
  var noEmail = 0;
  var hayVacios = false;
  var msjeVacios = "Debe rellenar los campos ";
  var today = new Date();
  var day = today.getDay();
  var month = today.getMonth()+1;
  var year  = today.getFullYear();
  

  
  if(anyoBisiesto(anhoFecVenci)){
  	diaFebreroVenci = 29;
  }else{
  	diaFebreroVenci = 28;
  }
   if(anyoBisiesto(anhoFec)){
  	diaFebrero = 29;
  }else{
  	diaFebrero = 28;
  }
  
  if(clave == null || clave == ""){
  	noClave = 1;
  	hayVacios = true;
  	msjeVacios+="Clave de activaci�n, ";
  }
   if(diaFecVenci == null || diaFecVenci == "" || mesFecVenci == null || mesFecVenci == "" ||
     anhoFecVenci == null || anhoFecVenci == ""){
   	noFec = 1;
   	hayVacios = true;
   	msjeVacios+="Fecha de vencimiento de la p�liza, ";
  }
  if(nifNie == null || nifNie == ""){
  	noNifNie = 1;
  	hayVacios = true;
  	msjeVacios+="NIF/NIE, ";
  }
  if(diaFec == null || diaFec == "" || mesFec == null || mesFec == "" ||
     anhoFec == null || anhoFec == ""){
   	noFec = 1;
   	hayVacios = true;
   	msjeVacios+="Fecha nacimiento, ";
  }
  if(tfno == null || tfno == ""){
  	noTfno = 1;	
  	hayVacios = true;
  	msjeVacios+="Tel�fono de contacto, ";
  }
  if(email == null || email ==""){
  	noEmail = 1;
  	hayVacios = true;
  	msjeVacios+="Correo electr�nico, "; 
  }
  //Se eliminan en primer lugar los caracteres no alfanum�ricos del nif/nie
  var fixNifNie = eliminarNoAlfaNum(nifNie);
  //Despu�s se rellena con ceros por la izquierda
 if(noNifNie==0)
 {
   fixNifNie = rellenaCeros(fixNifNie);
}
  //Se comprueba si es nif o nie, verificando si hay una X en la cadena introducida
  var checkNifNie = "";
  var indice_X = fixNifNie.indexOf("X");
  var firstNie = "";
  var secondNie = "";
  
  if(indice_X == -1 || indice_X == 9){
  	checkNifNie = "nif";
  }else{
  	checkNifNie = "nie";
  	if(indice_X > 0){
           firstNie= fixNifNie.substr(0,indice_X);
        }
        secondNie = fixNifNie.substr(indice_X+1,fixNifNie.length);
  	
  }
  formu.numDoc.value = fixNifNie;
  if(hayVacios){
  	msjeVacios = msjeVacios.substring(0,(msjeVacios.length)-2);
  	alert(msjeVacios);
  	return false;
  }else if(clave.length != 8){
  	alert("El campo Clave de activaci�n debe tener 8 caracteres");
  	return false;
  }else if(!validacionAlfaNumEsp(clave)){
  	alert("El campo Clave de activaci�n s�lo admite caracteres alfanum�ricos");
  	return false;
  }
  
    else if(!validacionNumerico(diaFecVenci)||!validacionNumerico(mesFecVenci)||!validacionNumerico(anhoFecVenci)){
  	alert("Los campos de la Fecha de vencimiento de la p�liza deben ser num�ricos");
  	return false;
  }else if(diaFecVenci.length != 2 || mesFecVenci.length != 2 || anhoFecVenci.length != 4){
  	alert("La longitud de los campos de la Fecha de vencimiento de la p�liza no es correcta (Formato dd/mm/aaaa)");
  	return false;
  }else if(diaFecVenci < 1 || diaFecVenci > 31){
  	alert("El d�a de la Fecha de vencimiento de la p�liza debe estar comprendido entre 1 y 31");
  	return false;
  }else if(mesFecVenci < 1 || mesFecVenci > 12){
  	alert("El mes de la Fecha de vencimiento de la p�lizadebe estar comprendido entre 1 y 12");
  	return false;
  }else if ((mesFecVenci==2) && ((diaFecVenci<1) || (diaFecVenci>diaFebreroVenci))){
	alert("El dia introducido no es v�lido para el mes");
	return false;
  }else if (((mesFecVenci==1) || (mesFecVenci==3) || (mesFecVenci==5) || (mesFecVenci==7) || (mesFecVenci==8) ||
             (mesFecVenci==10) || (mesFecVenci==12)) && ((diaFecVenci<1) || (diaFecVenci>31))){
        alert("El dia introducido no es v�lido para el mes");
	return false;
  }else if (((mesFecVenci==4) || (mesFecVenci==6) || (mesFecVenci==9) || (mesFecVenci==11)) && ((diaFecVenci<1) || (diaFecVenci>30))){
        alert("El dia introducido no es v�lido para el mes");
        return false;
  }else if(anhoFecVenci < 1900){
  	alert("El a�o de la Fecha de vencimiento de la p�liza debe ser igual o mayor a 1900");
  	return false;
  }/*else if(anhoFecVenci > year){
  	alert("El campo Fecha de vencimiento de la p�liza no puede ser superior a la actual");
  	return false;
  }else if((anhoFecVenci == year) && (mesFecVenci > month)){
  	alert("El campo Fecha de vencimiento de la p�liza no puede ser superior a la actual");
  	return false;
  }else if(anhoFecVenci == year && mesFecVenci == month && diaFecVenci > day){
  	alert("El campo Fecha de vencimiento de la p�liza no puede ser superior a la actual");
  	return false;
  
  }*/else if(checkNifNie == "nif" && !validarFormatoNif(fixNifNie)){
  	alert("El formato del NIF no es correcto. (Formato NIF:99999999X)");
  	return false;	
  }else if(checkNifNie == "nif" && !validarLetraNif(fixNifNie)){
  	alert("La letra del NIF no es correcta");
  	return false;	
  }else if(checkNifNie == "nie" && !validarFormatoNie(fixNifNie,firstNie,secondNie,indice_X)){
  	alert("El formato del NIE no es correcto. (Formato NIE:X99999999X)");
  	return false;	
  }else if(checkNifNie == "nie" && !validarLetraNif(secondNie)){
  	alert("La letra del NIE no es correcta");
  	return false;
  }
  //alert("El formato del NIF no es correcto. (Formato NIF:99999999X, formato NIE:X99999999X)");
  else if(!validacionNumerico(diaFec)||!validacionNumerico(mesFec)||!validacionNumerico(anhoFec)){
  	alert("Los campos de la Fecha de nacimiento deben ser num�ricos");
  	return false;
  }else if(diaFec.length != 2 || mesFec.length != 2 || anhoFec.length != 4){
  	alert("La longitud de los campos de la Fecha de nacimiento no es correcta (Formato dd/mm/aaaa)");
  	return false;
  }else if(diaFec < 1 || diaFec > 31){
  	alert("El d�a de la Fecha de nacimiento debe estar comprendido entre 1 y 31");
  	return false;
  }else if(mesFec < 1 || mesFec > 12){
  	alert("El mes de la Fecha de nacimiento debe estar comprendido entre 1 y 12");
  	return false;
  }else if ((mesFec==2) && ((diaFec<1) || (diaFec>diaFebrero))){
	alert("El dia introducido no es v�lido para el mes");
	return false;
  }else if (((mesFec==1) || (mesFec==3) || (mesFec==5) || (mesFec==7) || (mesFec==8) ||
             (mesFec==10) || (mesFec==12)) && ((diaFec<1) || (diaFec>31))){
        alert("El dia introducido no es v�lido para el mes");
	return false;
  }else if (((mesFec==4) || (mesFec==6) || (mesFec==9) || (mesFec==11)) && ((diaFec<1) || (diaFec>30))){
        alert("El dia introducido no es v�lido para el mes");
        return false;
  }else if(anhoFec < 1900){
  	alert("El a�o de la Fecha de nacimiento debe ser igual o mayor a 1900");
  	return false;
  }else if(anhoFec > year){
  	alert("El campo Fecha de nacimiento no puede ser superior a la actual");
  	return false;
  }else if((anhoFec == year) && (mesFec > month)){
  	alert("El campo Fecha de nacimiento no puede ser superior a la actual");
  	return false;
  }else if(anhoFec == year && mesFec == month && diaFec > day){
  	alert("El campo Fecha de nacimiento no puede ser superior a la actual");
  	return false;
  }
  else if(tfno.length < 9){
  	alert("El campo Tel�fono de contacto debe tener 9 d�gitos");
  	return false;
  }else if(!validacionNumerico(tfno)){
  	alert("El campo Tel�fono de contacto s�lo admite caracteres num�ricos");
  	return false;
  }else if(!validarMail(email)){
  	alert("El campo Correo electr�nico tiene un formato incorrecto");
  	return false;
  }else{
    	return true;
  }
  
}

function comprobarCamposCampanyaEndesa( ){
  	formu =document.forms[0];
  	var clave = formu.clave.value;
  	var claveFocus = formu.clave;
  	var nifNie= formu.numDoc.value;
  	var nifNieFocus = formu.numDoc;
  	var diaFec = formu.diaFecNacimiento.value;
  	var diaFecFocus = formu.diaFecNacimiento;
  	var mesFec = formu.mesFecNacimiento.value;
  	var mesFecFocus = formu.mesFecNacimiento;
  	var anhoFec = formu.anhoFecNacimiento.value;
  	var anhoFecFocus = formu.anhoFecNacimiento;
  	var tipoDoc = formu.txtTipoDoc.value;
  	var tipoDocFocus = formu.txtTipoDoc;
  	var campoFocus;
  	var diaFebrero; 
  	var noClave = 0;
  	var noNifNie = 0;
  	var noFec = 0;
  	var campoFocus;
  	var hayVacios = false;
  	var msjeVacios = "";
  	var today = new Date();
  	var day = today.getDay();
  	var month = today.getMonth()+1;
  	var year  = today.getFullYear();
  
   	if(anyoBisiesto(anhoFec)){
  		diaFebrero = 29;
  	}else{
  		diaFebrero = 28;
  	}
  
  	if(clave == null || clave == ""){
  		noClave = 1;
  		hayVacios = true;
  		msjeVacios+="Debe indicar la Clave de activaci�n.    \n";
  		campoFocus=claveFocus;
  		 
  	}else if (tipoDoc == -1) {
  		hayVacios = true;
  		msjeVacios+="Debe indicar el Tipo de documento.    \n";
  		if (campoFocus == null){
  		    campoFocus=tipoDocFocus;
  		}
  	}else if(nifNie == null || nifNie == ""){
  		noNifNie = 1;
  		hayVacios = true;
  		msjeVacios+="Debe indicar el N� de documento.    \n";
  		if (campoFocus == null){
  		    campoFocus=nifNieFocus;
  		} 
  	}else if(diaFec == null || diaFec == "" || mesFec == null || mesFec == "" ||
     			anhoFec == null || anhoFec == ""){
   		noFec = 1;
   		hayVacios = true;
   		msjeVacios+="Debe indicar la Fecha de nacimiento.     ";
   		if (campoFocus == null){
   		    if ((diaFec == null || diaFec == "" )){
   			    campoFocus=diaFecFocus;
       		} else if ((mesFec == null || mesFec == "")){
   		        campoFocus=mesFecFocus;
     		} else if ((anhoFec == null || anhoFec == "")){
    		    campoFocus=anhoFecFocus;
    		}
   		}		
   		
  	}
  	
  	if(nifNie == null || nifNie == ""){
  		noNifNie = 1;
  	} 
  		
  	//Se eliminan en primer lugar los caracteres no alfanum�ricos del nif/nie
  	var fixNifNie = eliminarNoAlfaNum(nifNie);
  	//Despu�s se rellena con ceros por la izquierda
 	if(noNifNie==0){
   		fixNifNie = rellenaCeros(fixNifNie);
	}
  	//Se comprueba si es nif o nie, verificando si hay una X en la cadena introducida
  	var checkNifNie = "";
  	var indice_X = fixNifNie.indexOf("X");
  	var firstNie = "";
  	var secondNie = "";
  
  	if(indice_X == -1 || indice_X == 9){
  		checkNifNie = "nif";
  	}else{
  		checkNifNie = "nie";
  		if(indice_X > 0){
           firstNie= fixNifNie.substr(0,indice_X);
        }
        secondNie = fixNifNie.substr(indice_X+1,fixNifNie.length);
  	}
  	formu.numDoc.value = fixNifNie;
  	if(hayVacios){
  		msjeVacios = msjeVacios.substring(0,(msjeVacios.length)-2);
  		alert(msjeVacios);
  		campoFocus.focus();
  		return false;
  	}else if(clave.length != 8){
  		alert("El campo Clave de activaci�n debe tener 8 caracteres");
  		claveFocus.focus();
  		return false;
  	}else if(!validacionAlfaNumEsp(clave)){
  		alert("El campo Clave de activaci�n s�lo admite caracteres alfanum�ricos");
  		claveFocus.focus();
  		return false;
  	}else if((tipoDoc == 0 && checkNifNie == "nie")||(tipoDoc == 2 && checkNifNie == "nif")){
  		alert("El N� de Documento introducido no se corresponde con el Tipo de Documento seleccionado");
  		nifNieFocus.focus();
  	}else if(tipoDoc == 0 && !validarFormatoNif(fixNifNie)){
  		alert("El formato del NIF no es correcto. (Formato NIF:99999999X)");
  		nifNieFocus.focus();
  		return false;	
  	}else if(tipoDoc == 0 && !validarLetraNif(fixNifNie)){
  		alert("La letra del NIF no es correcta");
  		nifNieFocus.focus();
  		return false;	
  	}else if(tipoDoc == 2 && !validarFormatoNie(fixNifNie,firstNie,secondNie,indice_X)){
  		alert("El formato del NIE no es correcto. (Formato NIE:X99999999X)");
  		nifNieFocus.focus();
  		return false;	
  	}else if(tipoDoc == 2 && !validarLetraNif(secondNie)){
  		alert("La letra del NIE no es correcta");
  		nifNieFocus.focus();
  		return false;
  	}else if(!validacionNumerico(diaFec)){
  		alert("Los campos de la Fecha de nacimiento deben ser num�ricos");
  		diaFecFocus.focus();
  		return false;
  	}else if(!validacionNumerico(mesFec)){
  	    alert("Los campos de la Fecha de nacimiento deben ser num�ricos");
  		mesFecFocus.focus();
  		return false;
  	}else if(!validacionNumerico(anhoFec)){
  	    alert("Los campos de la Fecha de nacimiento deben ser num�ricos");
  		anhoFecFocus.focus();
  		return false;
  	}else if(diaFec.length != 2 || mesFec.length != 2 || anhoFec.length != 4){
  		alert("La longitud de los campos de la Fecha de nacimiento no es correcta (Formato dd/mm/aaaa)");
  		if(diaFec.length != 2){
  		    diaFecFocus.focus();
  		} else if(mesFec.length != 2) {
  		    mesFecFocus.focus();
  		} else if (anhoFec.length != 4){
  		   anhoFecFocus.focus();
  		}
  		return false;
  	}else if(diaFec < 1 || diaFec > 31){
  		alert("El d�a de la Fecha de nacimiento debe estar comprendido entre 1 y 31");
  		diaFecFocus.focus();
  		return false;
  	}else if(mesFec < 1 || mesFec > 12){
  		alert("El mes de la Fecha de nacimiento debe estar comprendido entre 1 y 12");
  		mesFecFocus.focus();
  		return false;
  	}else if ((mesFec==2) && ((diaFec<1) || (diaFec>diaFebrero))){
		alert("El dia introducido no es v�lido para el mes");
		diaFecFocus.focus();
		return false;
  	}else if (((mesFec==1) || (mesFec==3) || (mesFec==5) || (mesFec==7) || (mesFec==8) ||
             (mesFec==10) || (mesFec==12)) && ((diaFec<1) || (diaFec>31))){
        alert("El dia introducido no es v�lido para el mes");
        diaFecFocus.focus();
		return false;
  	}else if (((mesFec==4) || (mesFec==6) || (mesFec==9) || (mesFec==11)) && ((diaFec<1) || (diaFec>30))){
        alert("El dia introducido no es v�lido para el mes");
        diaFecFocus.focus();
        return false;
  	}else if(anhoFec < 1900){
  		alert("El a�o de la Fecha de nacimiento debe ser igual o mayor a 1900");
  		anhoFecFocus.focus();
  		return false;
  	}else if(anhoFec > year){
  		alert("El campo Fecha de nacimiento no puede ser superior a la actual");
  		anhoFecFocus.focus();
  		return false;
  	}else if((anhoFec == year) && (mesFec > month)){
  		alert("El campo Fecha de nacimiento no puede ser superior a la actual");
  		mesFecFocus.focus();
  		return false;
  	}else if(anhoFec == year && mesFec == month && diaFec > day){
  		alert("El campo Fecha de nacimiento no puede ser superior a la actual");
  		diaFecFocus.focus();
  		return false;
  	}else{
    	return true;
  	}
}

function confirmarUsuarioCampanyaEndesa (){
	if(comprobarCamposCampanyaEndesa()){
		document.forms[0].submit();
	}
}



//Funci�n para rellenar con ceros a la izquierda hasta que el nif/nie llegue
//a tener 10 caracteres
function rellenaCeros(numDoc){
  var ceros = "";
  var tamCadena = numDoc.length;
  var diferencia = 10-tamCadena;
  
  for(var i = 0; i < diferencia; i++){
    ceros+="0";
  } 
  numDoc = ceros + numDoc;
	
  return numDoc;
	
}

function rellenaCerosDia(){

	formu =document.forms[0];
  	var diaFec = formu.diaFecNacimiento.value;
  	if(diaFec.length == 1){
  		diaFec = "0" + diaFec;
  	}
  	formu.diaFecNacimiento.value = diaFec;
}

function rellenaCerosMes(){

	formu =document.forms[0];
  	var mesFec = formu.mesFecNacimiento.value;
  	if(mesFec.length == 1){
  		mesFec = "0" + mesFec;
  	}
  	formu.mesFecNacimiento.value=mesFec
}


//Funci�n para eliminar cualquier car�cter no alfanum�rico del nif/nie
function eliminarNoAlfaNum(inputStr){
  var tempStr = "";
  
  for (var i = 0; i < inputStr.length; i++) {
     var oneChar = inputStr.charAt(i);
     if(isNumber(oneChar)||isLetter(oneChar)){
     	tempStr+=oneChar;
     }
     
  }
  
  return tempStr;

}
//Funci�n para comprobar si una variable es un n�mero
function isNumber(inputStr) {
	for (var i = 0; i < inputStr.length; i++) {
	var oneChar = inputStr.charAt(i)
		if (oneChar < "0" || oneChar > "9") {
			return false
		}
	}
	return true
}

//Funci�n para comprobar si una variable es una letra
function isLetter(inputStr){
	var lowercase_letters = "abcdefghijklmnopqrstuvwxyz"
	var uppercase_letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	for (var i = 0; i < inputStr.length; i++) {
		var character = inputStr.charAt(i)
		if(lowercase_letters.indexOf(character) == -1 &&
		   uppercase_letters.indexOf(character) == -1) {
		   return false  
		}
	}
	return true
}

//Funcion para determinar si un a�o es bisiesto
function anyoBisiesto(anyo){
	if (anyo < 100)
    	var fin = anyo + 1900;
    else
        var fin = anyo ;
    if (fin % 4 != 0)
        return false;
    else{
        if (fin % 100 == 0){
	        if (fin % 400 == 0){
    	        return true;
            }else{
                return false;
            }
       	}else{
        	return true;
        }
   }
}

//Funci�n para validar el formato del nif
function validarFormatoNif(pNIF){

  //Los primeros d�gitos deben ser n�meros y el �ltimo una letra  
  cNumero = pNIF.substr(0,pNIF.length -1)
  cLetra = pNIF.toUpperCase().substr(pNIF.length -1,1)
  
    if (isNaN(cNumero)){
        return false;
    }else if(!isNaN(cLetra)){
    	return false;
    }else{
        return true;
    }
}

//Funci�n para validar la letra del nif
function validarLetraNif(pNIF){
 
  try{
    //Separamos el n�mero y lo validamos
    cNumero = pNIF.substr(0,pNIF.length -1)
    
    cPatron = /["."]/
    aPatron = cPatron.exec(cNumero)
         
    if (aPatron != null) return false

    // Separamos la letra y la validamos
    cLetra = pNIF.toUpperCase().substr(pNIF.length -1,1)

    // Comprueba que la letra se corresponde con el n�mero
    cPatron = new String("TRWAGMYFPDXBNJZSQVHLCKE")
    if (cLetra != cPatron.substr( (parseInt(cNumero,10) % 23), 1)) return false

   }catch(e){
   
      return false
   }
return true;

} 

//Funci�n para validar el formato del nif
function validarFormatoNie(nie, primeraParte, segundaParte, indice_X){

  //Si hay algo antes de la X, deben ser ceros
  if(indice_X > 0){
      for(var i=0; i < primeraParte.length; i++){
      	  var oneChar = primeraParte.charAt(i);
      	  
      	  if(oneChar != 0){
      	    return false;
      	  }
  		
      }
  }
  //Despu�s de la X debe venir una secuencia de n�meros y despu�s una letra  
  if(!validarFormatoNif(segundaParte)){
      return false;
  }
  else{  
      return true;
  }
}

//----------------------------------------------------------------------
//     Funci�n : validacionNumerico
// Descripci�n : Comprobaci�n b�sica de que todos los caracteres de la
//               cadena son s�lo uno de estos "0123456789+-.,() "
//  Par�metros : Cadena con en n�mero a comprobar
//     Retorno : false / true
//       Notas : -
//----------------------------------------------------------------------
function validacionNumerico( cNumero ) {
	// Recorremos los datos introducidos
	for ( nPos = 0; nPos < cNumero.length; nPos++ ) {
		var cCaracter = cNumero.charAt( nPos )
		if ( isNaN( parseInt( cCaracter ) ) 
		     && cCaracter != "-"
		     && cCaracter != "+"
		     && cCaracter != "."
		     && cCaracter != ","
		     && cCaracter != "("
		     && cCaracter != ")"
		     && cCaracter != " " ) {
			return false;
		}
	}
	return true;
}
//----------------------------------------------------------------------
//     Funci�n : validacionAlfaNumEsp
// Descripci�n : Comprueba que en el texto s�lo hay letras sin acentuar, 
//               los caracteres extendidos (����) o d�gitos, o ./...
//  Par�metros : Cadena con el texto a analizar
//     Retorno : false / true
//       Notas : -
//----------------------------------------------------------------------
function validacionAlfaNumEsp( cTexto ) {
	// Recorremos los datos introducidos
	for ( nPos = 0; nPos < cTexto.length; nPos++ ) {
		var cCaracter = cTexto.charAt( nPos );
		if ( !( (cCaracter >= '0' && cCaracter <= '9') ||
				(cCaracter >= 'a' && cCaracter <= 'z') ||
				(cCaracter >= 'A' && cCaracter <= 'Z') ||
				(cCaracter == '�' || cCaracter == '�'  ||
				 cCaracter == '�' || cCaracter == '�'
|| cCaracter == ' ' || cCaracter == '.' ||cCaracter == '/'||cCaracter == ':'||cCaracter == ','||cCaracter == ';') ) ) {
			return false;
		}
	}
	return true;
}

/**
  * Nos redirecciona al tarificador al pinchar el bot�n cancelar
  * @author: Celinda Ruiz
  */
function botonCancelar() {
	window.close();
	window.opener.location = "ForwardAction.do";			
}
/**
  * Selecciona el texto 'varTexto' de entre los textos del combo 'varCombo'
  *
  * @author Fernando Vega Gonz�lez
  */
function seleccionarCombo(varCombo,varTexto) {
		for (var i=0;i<varCombo.options.length;i++) {
			if (varCombo.options[i].text==varTexto) {
				varCombo.options[i].selected=1;
				return 0;				
			}			
		}	
	}
/**
  * Valida email que se le pasa como par�metro
  *
  * @author Fernando Vega Gonz�lez
  */	
function validarMail(pEmail){ 

	if(pEmail.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) == -1){
	   return false;
	}
	
	return true;
	
} 	

/**
  * Selecciona el valor 'varValor' de entre los valores del combo 'varCombo'
  *
  * @author Fernando Vega Gonz�lez
  */
function seleccionarCombo2(varCombo,varValor) {
	var esta=false;
	var j=0;
		for (var i=0;i<varCombo.options.length;i++) {	
			if (cambio(varCombo.options[i].value)==cambio(varValor)) {
				esta=true;
				j=i;
			}	
		}	
	if (esta){
		varCombo.options[j].selected=1;
		return true;
	}
	return false;	
}
/**
  * Funci�n que pasa una cadena a num�rico comprobando antes ciertos valores de la cadena para los
  *	que no funciona bien el parseInt()
  *
  * @author Fernando Vega Gonz�lez
  */
function cambio(strValor){
			if (strValor=='08') strValor='8';
			else if (strValor=='09') strValor='9';
			strValor=parseInt(strValor);
			return strValor;
}
/**
  * Funci�n que borra los campos de pantalla
  *
  * @author Celinda Ruiz Ortega
  */
function borrarCampos(){
			document.forms[0].txtClave.value="";
			document.forms[0].txtClave.focus();
			
}
darFoco
/**
  * Funci�n que le asigna el foco al campo confirmaci�n contrato de ActivacionDefinicion.JSP
  *
  * @author Celinda Ruiz Ortega
  */
function darFoco(){
			document.forms[0].txtClave.focus();
			
}

function enviarDefinicion()
{
	document.forms[0].txtPregunta1.value=document.forms[0].txtPregunta1.value.toUpperCase();
	document.forms[0].txtPregunta2.value=document.forms[0].txtPregunta2.value.toUpperCase();
	document.forms[0].txtRespuesta1.value=document.forms[0].txtRespuesta1.value.toUpperCase();
	document.forms[0].txtRespuesta2.value=document.forms[0].txtRespuesta2.value.toUpperCase();
	
}
function enviarDefinicionClave()
{
document.forms[0].txtClave.value=document.forms[0].txtClave.value.toUpperCase();
document.forms[0].txtConfirmar.value=document.forms[0].txtConfirmar.value.toUpperCase();

}
function enviarActivacion()
{
document.forms[0].txtClave.value=document.forms[0].txtClave.value.toUpperCase();


}
function comprobarCamposNuevaCampanya( ){
  	formu =document.forms[0];
  	var clave = formu.clave.value;
  	var claveFocus = formu.clave;
  	var nifNie= formu.numDoc.value;
  	var nifNieFocus = formu.numDoc;
  	var diaFec = formu.diaFecNacimiento.value;
  	var diaFecFocus = formu.diaFecNacimiento;
  	var mesFec = formu.mesFecNacimiento.value;
  	var mesFecFocus = formu.mesFecNacimiento;
  	var anhoFec = formu.anhoFecNacimiento.value;
  	var anhoFecFocus = formu.anhoFecNacimiento;
  	var tipoDoc = formu.txtTipoDoc.value;
  	var tipoDocFocus = formu.txtTipoDoc;
  	var campoFocus;
  	var diaFebrero; 
  	var noClave = 0;
  	var noNifNie = 0;
  	var noFec = 0;
  	var campoFocus;
  	var hayVacios = false;
  	var msjeVacios = "Debes rellenar los campos ";
  	var today = new Date();
  	var day = today.getDay();
  	var month = today.getMonth()+1;
  	var year  = today.getFullYear();
  
   	if(anyoBisiesto(anhoFec)){
  		diaFebrero = 29;
  	}else{
  		diaFebrero = 28;
  	}
  
  	if(clave == null || clave == ""){
  		noClave = 1;
  		hayVacios = true;
  		msjeVacios+="Clave de activaci�n, ";
  		campoFocus=claveFocus;
  		 
  	}   
  	if (tipoDoc == -1) {
  		hayVacios = true;
  		msjeVacios+="Tipo de Documento, ";
  		if (campoFocus == null){
  		    campoFocus=tipoDocFocus;
  		}
  	}	
  	if(nifNie == null || nifNie == ""){
  		noNifNie = 1;
  		hayVacios = true;
  		msjeVacios+="N� Documento, ";
  		if (campoFocus == null){
  		    campoFocus=nifNieFocus;
  		} 
  	}
  	if(diaFec == null || diaFec == "" || mesFec == null || mesFec == "" ||
     			anhoFec == null || anhoFec == ""){
   		noFec = 1;
   		hayVacios = true;
   		msjeVacios+="Fecha nacimiento, ";
   		if (campoFocus == null){
   		    if ((diaFec == null || diaFec == "" )){
   			    campoFocus=diaFecFocus;
       		} else if ((mesFec == null || mesFec == "")){
   		        campoFocus=mesFecFocus;
     		} else if ((anhoFec == null || anhoFec == "")){
    		    campoFocus=anhoFecFocus;
    		}
   		}		
   		
  	} 
  		
  	//Se eliminan en primer lugar los caracteres no alfanum�ricos del nif/nie
  	var fixNifNie = eliminarNoAlfaNum(nifNie);
  	//Despu�s se rellena con ceros por la izquierda
 	if(noNifNie==0){
   		fixNifNie = rellenaCeros(fixNifNie);
	}
  	//Se comprueba si es nif o nie, verificando si hay una X en la cadena introducida
  	var checkNifNie = "";
  	var indice_X = fixNifNie.indexOf("X");
  	var firstNie = "";
  	var secondNie = "";
  
  	if(indice_X == -1 || indice_X == 9){
  		checkNifNie = "nif";
  	}else{
  		checkNifNie = "nie";
  		if(indice_X > 0){
           firstNie= fixNifNie.substr(0,indice_X);
        }
        secondNie = fixNifNie.substr(indice_X+1,fixNifNie.length);
  	}
  	formu.numDoc.value = fixNifNie;
  	if(hayVacios){
  		msjeVacios = msjeVacios.substring(0,(msjeVacios.length)-2);
  		alert(msjeVacios);
  		campoFocus.focus();
  		return false;
  	}else if(clave.length != 8){
  		alert("El campo Clave de activaci�n debe tener 8 caracteres");
  		claveFocus.focus();
  		return false;
  	}else if(!validacionAlfaNumEsp(clave)){
  		alert("El campo Clave de activaci�n s�lo admite caracteres alfanum�ricos");
  		claveFocus.focus();
  		return false;
  	}else if((tipoDoc == 0 && checkNifNie == "nie")||(tipoDoc == 2 && checkNifNie == "nif")){
  		alert("El N� de Documento introducido no se corresponde con el Tipo de Documento seleccionado");
  		nifNieFocus.focus();
  	}else if(tipoDoc == 0 && !validarFormatoNif(fixNifNie)){
  		alert("El formato del NIF no es correcto. (Formato NIF:99999999X)");
  		nifNieFocus.focus();
  		return false;	
  	}else if(tipoDoc == 0 && !validarLetraNif(fixNifNie)){
  		alert("La letra del NIF no es correcta");
  		nifNieFocus.focus();
  		return false;	
  	}else if(tipoDoc == 2 && !validarFormatoNie(fixNifNie,firstNie,secondNie,indice_X)){
  		alert("El formato del NIE no es correcto. (Formato NIE:X99999999X)");
  		nifNieFocus.focus();
  		return false;	
  	}else if(tipoDoc == 2 && !validarLetraNif(secondNie)){
  		alert("La letra del NIE no es correcta");
  		nifNieFocus.focus();
  		return false;
  	}else if(!validacionNumerico(diaFec)){
  		alert("Los campos de la Fecha de nacimiento deben ser num�ricos");
  		diaFecFocus.focus();
  		return false;
  	}else if(!validacionNumerico(mesFec)){
  	    alert("Los campos de la Fecha de nacimiento deben ser num�ricos");
  		mesFecFocus.focus();
  		return false;
  	}else if(!validacionNumerico(anhoFec)){
  	    alert("Los campos de la Fecha de nacimiento deben ser num�ricos");
  		anhoFecFocus.focus();
  		return false;
  	}else if(diaFec.length != 2 || mesFec.length != 2 || anhoFec.length != 4){
  		alert("La longitud de los campos de la Fecha de nacimiento no es correcta (Formato dd/mm/aaaa)");
  		if(diaFec.length != 2){
  		    diaFecFocus.focus();
  		} else if(mesFec.length != 2) {
  		    mesFecFocus.focus();
  		} else if (anhoFec.length != 4){
  		   anhoFecFocus.focus();
  		}
  		return false;
  	}else if(diaFec < 1 || diaFec > 31){
  		alert("El d�a de la Fecha de nacimiento debe estar comprendido entre 1 y 31");
  		diaFecFocus.focus();
  		return false;
  	}else if(mesFec < 1 || mesFec > 12){
  		alert("El mes de la Fecha de nacimiento debe estar comprendido entre 1 y 12");
  		mesFecFocus.focus();
  		return false;
  	}else if ((mesFec==2) && ((diaFec<1) || (diaFec>diaFebrero))){
		alert("El dia introducido no es v�lido para el mes");
		diaFecFocus.focus();
		return false;
  	}else if (((mesFec==1) || (mesFec==3) || (mesFec==5) || (mesFec==7) || (mesFec==8) ||
             (mesFec==10) || (mesFec==12)) && ((diaFec<1) || (diaFec>31))){
        alert("El dia introducido no es v�lido para el mes");
        diaFecFocus.focus();
		return false;
  	}else if (((mesFec==4) || (mesFec==6) || (mesFec==9) || (mesFec==11)) && ((diaFec<1) || (diaFec>30))){
        alert("El dia introducido no es v�lido para el mes");
        diaFecFocus.focus();
        return false;
  	}else if(anhoFec < 1900){
  		alert("El a�o de la Fecha de nacimiento debe ser igual o mayor a 1900");
  		anhoFecFocus.focus();
  		return false;
  	}else if(anhoFec > year){
  		alert("El campo Fecha de nacimiento no puede ser superior a la actual");
  		anhoFecFocus.focus();
  		return false;
  	}else if((anhoFec == year) && (mesFec > month)){
  		alert("El campo Fecha de nacimiento no puede ser superior a la actual");
  		mesFecFocus.focus();
  		return false;
  	}else if(anhoFec == year && mesFec == month && diaFec > day){
  		alert("El campo Fecha de nacimiento no puede ser superior a la actual");
  		diaFecFocus.focus();
  		return false;
  	}else{
    	return true;
  	}
}

function codigoPostal(){
	var strCP = document.forms[0].txtCP.value;

		 if (strCP.length!=5)
			{
				document.forms[0].txtProvincia.value="";
				return;
			}
		
		strCP = strCP.substring(0,2);
		if(seleccionarCombo2(document.forms[0].cboProvincia, strCP)){
			document.forms[0].txtProvincia.value=document.forms[0].cboProvincia.options[document.forms[0].cboProvincia.selectedIndex].text;	
		}else{
			document.forms[0].txtProvincia.value="";
		}
		
	}
