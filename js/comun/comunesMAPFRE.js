// FUNCIONES COMUNES A TODO EL PORTAL MAPFRE

// Aumentamos y disminuimos el texto
var txtSz = 0.7;
function textSize(signo)
{
	var cuerpo = document.getElementsByTagName("BODY")
	if(signo=="plus")
		txtSz = txtSz+0.1;
	else if(signo == "same")
		txtSz = txtSz;
	else
		txtSz = txtSz-0.10;

	//txtSz = (signo=="plus") ? txtSz+0.10 : txtSz-0.10;
	if (txtSz>1.1) txtSz = 1.1;
	if (txtSz<0.5) txtSz = 0.5;
	ts = txtSz+"em";
	cuerpo[0].style.fontSize = ts;
	createCookie("txtSz",txtSz);
}


//Funciones para las cookies
function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
	if(document.cookie != null){
    	for(var i=0;i < ca.length;i++) {
	        var c = ca[i];
	        while (c.charAt(0)==' ') c = c.substring(1,c.length);
			c.substring(nameEQ.length,c.length);
	        if (c.indexOf(nameEQ) == 0) {
				return c.substring(nameEQ.length,c.length);
			}
	    }
	}
    return null;
}



function eraseCookie(name) {
    createCookie(name,"",-1);
}
// Cookie management functions - END
function cargarLinea(id){
	createCookie("linea",id);
}

function mostrarLineaCorrecta(){
	linea = readCookie("linea");
	if(linea)
		activarLinea(linea);
	else
		activarLinea();
}

function cargarTexto(){
	if(readCookie("txtSz")!=null)
		txtSz = parseFloat(readCookie("txtSz"));
	else
		txtSz = 0.7;
	if(txtSz!=0.7)
		textSize("same");
}

//Funci�n marquesina
function marquesina(){
	document.getElementById("carga").src='http://mapfre.fininfo.fr/ticker.wts';
	setTimeout('marquesina()',15*60*1000)
}
// Funciones par el CONTROL DE MENU
// funciones menu para no parpadeo
var timerID = null;
var rollID = null;
var timerOn = false;
var timecount = 500;

function startTime()
{
	if (timerOn == false)
	{
       	timerID = setTimeout( "menu0()" , timecount);
		timerOn = true;
	}
}

function stopTime()
{
	if (timerOn)
	{
		clearTimeout(timerID);
       	timerID = null;
		rollID = null;
    	timerOn = false;
    }
}

function menu0()
{
	menu('0');
}

var menuAct = 0;
function menu(num)
{
	var numMenus = document.getElementById("menu").getElementsByTagName('DIV').length; // El n�mero de men�s es el n�mero de DIV que contiene el objeto MENU
	var smNum = eval("document.getElementById('sm"+num+"')");
	var smAct = eval("document.getElementById('sm"+menuAct+"')");
	var mNum = eval("document.getElementById('m"+num+"')");
	var mAct = eval("document.getElementById('m"+menuAct+"')");

	if (num == 0)
	{
		for (i=1;i<=numMenus;i++)
		{
			eval("document.getElementById('sm"+i+"').style.display = 'none'");
		}
		if (menuAct!=0)
		{
			if (mAct.className == "smOnCl") mAct.className = "smOn";
			if (mAct.className == "smCl") mAct.className = "sm";
		}
	}

	else
	{
		smNum.style.display = "block";
		if (mNum.className == "smOn") mNum.className = "smOnCl";
		else if (mNum.className == "sm") mNum.className = "smCl";

		if (menuAct != 0)
		{
			smAct.style.display = "none";
			if (mAct.className == "smOnCl") mAct.className = "smOn";
			else if (mAct.className == "smCl") mAct.className = "sm";
			if (num ==  menuAct)
			{
				smNum.style.display = "block";
				if (mNum.className == "smOn") mNum.className = "smOnCl";
				if (mNum.className == "sm") mNum.className = "smCl";
			}
		}
	}
	menuAct = num;
}

/* Dado un objeto y un className, comprimimos todos los objetos con dicho class */
function inicio(objeto,clase,posicion)
{

	var capas = document.getElementsByTagName(objeto);
	var nCapas = capas.length;
	for (i=0;i<nCapas;i++)
	{
		if (capas[i].className == clase)
		{
			capas[i].style.position = posicion;
			capas[i].style.display = "none";
		}
	}
}

/* Mostrar y ocultar objetos */
function blockNone(nombre,estado)
{
	document.getElementById(nombre).style.display = estado;
}

/* Consultas: abre la consulta en concreto */
function consultas(num,accion)
{
	if (accion=="abrir")
	{
		eval("blockNone('r"+num+"','block')");
		eval("document.getElementById('c"+num+"').className = 'selec'")
	}
	else
	{
		eval("blockNone('r"+num+"','none')");
		eval("document.getElementById('c"+num+"').className = ''")
	}
}

/* Tel�fonos: abre una capa en concreto */
function telefono(num)
{
	if (eval("document.getElementById('t"+num+"').className == ''"))
	{
		eval("blockNone('tel"+num+"','block')");
		eval("document.getElementById('t"+num+"').className = 'desplegado'")
	}
	else
	{
		eval("blockNone('tel"+num+"','none')");
		eval("document.getElementById('t"+num+"').className = ''")
	}
}

/* Funci�n que controla el Men� Lateral izquierdo */
function subMenuLat(num)
{
	var sm = eval("document.getElementById('smLat"+num+"')");
	eval("document.getElementById('sml"+num+"').style.position = 'static'");
	switch (sm.className)
	{
		case "mLat on":
		sm.className = "mLat onZip";
		eval("document.getElementById('sml"+num+"').style.display = 'none'");
		break;
		case "mLat onZip":
		sm.className = "mLat on";
		eval("document.getElementById('sml"+num+"').style.display = 'block'");
		break;
		case "mLat":
		sm.className = "mLat noZip";
		eval("document.getElementById('sml"+num+"').style.display = 'block'");

		break;
		case "mLat noZip":
		sm.className = "mLat";
		eval("document.getElementById('sml"+num+"').style.display = 'none'");
		break;
	}
}

/* Funci�n para las FAQ del lateral derecho */
function menuFAQ(num)
{

	var m = eval("document.getElementById('mFAQ"+num+"')");

	eval("document.getElementById('mFAQ"+num+"').style.position = 'static'");
	switch (m.className)
	{
		case "mFAQ":
		m.className = "mFAQ displayBlock";
		break;
		case "mFAQ displayBlock":
		m.className = "mFAQ";
		break;
	}
}

/* Abrir Ventana */
function abrirVentana(url,ancho,alto,nombre)
{
	if(nombre)
		eval("window.open(url, '"+nombre+"', 'toolbar=no, directories=no, location=no, status=no, menubar=no, resizable=no, scrollbars=yes, width="+ancho+", height="+alto+"')");
	else
		eval("window.open(url, 'ventana', 'toolbar=no, directories=no, location=no, status=no, menubar=no, resizable=no, scrollbars=yes, width="+ancho+", height="+alto+"')");
}

/* Abrir Ventana Gnral*/
function abrirVentanaGnral(url,ancho,alto, location, toolbar, menubar, resize, scroll)
{
	if(toolbar == null) toolbar = "1"; if(toolbar=="0") toolbar = "no"; else toolbar="yes";
	if(location == null) location = "1"; if(location=="0") location = "no"; else location="yes";
	if(menubar == null) menubar = "1";if(menubar=="0") menubar = "no"; else menubar="yes";
	if(resize == null) resize = "1";if(resize=="0") resize = "no"; else resize="yes";
	if(scroll == null) scroll = "1";if(scroll=="0") scroll = "no"; else scroll="yes";

	eval("window.open(url, 'ventana', 'toolbar=" + toolbar + ",location=" + location + ", menubar=" + menubar + ", resizable=" + resize +", scrollbars="+scroll+", width="+ancho+", height="+alto+"')");
}

function openCenteredWindow(url, height, width) {
   var left = Math.floor( (screen.width - width) / 2);
   var top = Math.floor( (screen.height - height) / 2);
   var winParms ="resizable=yes,top=" + top + ",left=" + left + ",height=" + height + ",width=" + width;
   var win = window.open(url, name, winParms);
   if (parseInt(navigator.appVersion) >= 4) { win.window.focus(); }
}

/* Activar capas de los servicios telef�nicos */
var srvActiv = 0;
function srv(num)
{
	if (srvActiv != 0)
	{
		eval ("document.getElementById('ser"+srvActiv+"').style.display = 'none'");
		if (srvActiv%2 == 0)
		{
			eval ("document.getElementById('srv"+srvActiv+"').className = 'par'");
		}
		else
		{
			eval ("document.getElementById('srv"+srvActiv+"').className = ''");
		}

	}
	eval ("document.getElementById('ser"+num+"').style.display = 'block'");
	eval ("document.getElementById('srv"+num+"').className = 'activo'");
	srvActiv = num;
}

/* Colocamos los linsk de zoom seg�n el navegador. */
// La �ltima l�nea (window.onresize=colocarZoom;) debe descomentarse en caso que se elija esta opci�n.
function colocarZoom()
{
var winW = 630, winH = 460;

if (parseInt(navigator.appVersion)>3) {
 if (navigator.appName=="Netscape") {
  winW = window.innerWidth;
  winH = window.innerHeight;
 }
 if (navigator.appName.indexOf("Microsoft")!=-1) {
  winW = document.body.offsetWidth;
  winH = document.body.offsetHeight;
 }
}
	var ancho = document.body.clientWidth;
	//var anchoP = document.getElementById("principal").clientWidth;
	if (ancho > 920)
	{
		document.getElementById("tipo").className = "sup";
	}
	else
	{
		document.getElementById("tipo").className = "";
	}
}
//window.onresize=colocarZoom;

/* Funci�n para ocultar y mostrar el mapa web */
var mwAct = 0;
function mapaweb(num)
{
	if (mwAct != 0)
	{
		if(document.getElementById("smMW"+mwAct) != null){
			eval ("document.getElementById('smMW"+mwAct+"').style.display = 'none'");
			eval ("document.getElementById('mw"+mwAct+"').className = ''");
		}
	}
	if(document.getElementById("smMW"+num) != null){
		eval ("document.getElementById('smMW"+num+"').style.display = 'block'");
		eval ("document.getElementById('mw"+num+"').className = 'on'");
	}
	mwAct = num;
}

/* Funci�n que activa la pestanya correspondiente del producto*/
function inicioPestanya(producto){
	var url = document.location.href;
	var contenidos = "";
	contenidos = document.getElementById("pestFicha").children;

	//Mostrar el contenido
	if(url.indexOf("idPestanya")!=-1){
		var idPestanya = url.substring(url.indexOf("idPestanya=") + 11, url.length);

		//Poner activa la pestanya
		for(i=0; i<contenidos.length; i++){
			if(contenidos[i].children[0].pest == idPestanya){
				contenidos[i].children[0].click();
			}
		}
	}else if(url.indexOf("faq")==-1){
		contenidos[0].children[0].click();
	}
}

/* Funci�n que controla el click para mostrar el contenido de las pesta�as de productos */
function cambiarPestanya(num, pestanya, producto, coletilla)
{
	var url = document.location.href;

	if(url.indexOf("productos")>0){
		/*var idProducto = url.substring(url.indexOf("padre=") + 6, url.length);
		var contenidos = document.getElementsByName("pestProd");
		alert("1: " + contenidos.length);
		for(i=0; i<contenidos.length; i++){
			alert(contenidos[i].idProducto);
			if(contenidos[i].idProducto!=null){
				if(contenidos[i].idProducto == idProducto){
					var contenidosAsociados = contenidos[i].children;
					alert(contenidosAsociados.length);
					for(i=0; i<contenidosAsociados.length; i++){
						if(contenidosAsociados[i].children[0].pest == pestanya){
							contenidosAsociados[i].children[0].className = "on";
						}else{
							contenidosAsociados[i].children[0].className = "";
						}
					}
				}
			}
		}*/

		var contenidos = document.getElementById("pestFicha").children;
		for(i=0; i<contenidos.length; i++){
			if(contenidos[i].children[0].pest == pestanya){
				contenidos[i].children[0].className = "on";
			}else{
				contenidos[i].children[0].className = "";
			}
		}


		for(i=0;i<document.getElementsByName("listOrd").length;i++){
			if(document.getElementsByName("listOrd")[i].pest==pestanya){
				document.getElementsByName("listOrd")[i].style.display = "block";
			}else{
				document.getElementsByName("listOrd")[i].style.display = "none";
			}
		}

		if(pestanya == "Comparacion"){
			enviarProducto('noDetallada');
		}

		if(url.indexOf(producto + "#" +coletilla)<0){
			document.location.href=producto + "#" +coletilla+ "?idPestanya=" + pestanya;
		}

	}else{
		document.location.href=producto + "#" +coletilla+ "?idPestanya=" + pestanya;
	}

}
//Funci�n para ocultar los posibles nombres de producto de un mas info
function ocultar(nombreCampo,etiqueta,cookie){
	var url = document.location.href;
	if(url.indexOf("padre")!=-1){
		if(cookie)
			var idProducto = readCookie("linea");
		else
			var idProducto = url.substring(url.indexOf("padre=") + 6, url.length);
	}else{
		var idProducto = readCookie("linea");
	}

	if (nombreCampo.indexOf("botonera")!=-1){
		nombreCampo = "botonera " + nombreCampo;
	}

	var contenidos = document.getElementsByTagName(etiqueta);
	var primera = "N";
	for(i=0;i<contenidos.length; i++){

		if(contenidos[i].className==nombreCampo){
			if(idProducto!=null){
				if(contenidos[i].id!= idProducto){
					contenidos[i].style.display="none";
				}
			}else if(primera == "N"){
				contenidos[i].style.display = "block";
				primera = "S";
			}else{
				contenidos[i].style.display = "none";
			}
		}
	}

		/*if(nombreCampo == "pestProd"){
			var idProducto = url.substring(url.indexOf("padre=") + 6, url.length);
			var pestanya = url.substring(url.indexOf("idPestanya=") + 11, url.indexOf("&"));
			var contenidos = document.getElementsByName("pestProd");
			for(i=0; i<contenidos.length; i++){
				if(contenidos[i].idProducto!=null){
					if(contenidos[i].idProducto == idProducto){
						var contenidosAsociados = contenidos[i].children;
						for(i=0; i<contenidosAsociados.length; i++){
							if(contenidosAsociados[i].id=="pestFicha"){
								var finalContenidos = contenidosAsociados[i].children;

								for(i=0; i<finalContenidos.length; i++){
									if(finalContenidos[i].children[0].pest == pestanya){
										finalContenidos[i].children[0].className = "on";
									}else{
										finalContenidos[i].children[0].className = "";
									}
								}
							}
						}
					}
				}
			}
		}*/
}

function mostrarFaqs(producto){
	//Nada
}

//Funci�n que desde el contenido de m�s info selecciona como marcado el producto en el men� izquierdo.
function activarProducto(idProducto,cookie){
	linea = readCookie("linea");
	if(idProducto==null){
		var url = document.location.href;
		if(url.indexOf("padre")!=-1){
			var idProducto = url.substring(url.indexOf("padre=") + 6, url.length);
		}
	}

	if(linea != null && cookie == null){
		var contenidoLinea = document.getElementById("_"+ linea);
		var hijos = contenidoLinea.childNodes;
		for(i=0;i<hijos.length;i++){
			var contenido = hijos.item(i);
			if(contenido.nodeName == "LI")
			{
				var nietos = contenido.childNodes;
				for(z=0;z<nietos.length;z++)
				{
					contenidoNietos = nietos.item(z);
					if(contenidoNietos.nodeName == "DL"){
						var ninietos = contenidoNietos.childNodes;
						for(y=0;y<ninietos.length;y++){
							contenidoNinietos = ninietos.item(y);
							if(contenidoNinietos.nodeName == "DT" && contenidoNinietos.id == "_" + idProducto){
								contenidoNinietos.className = "on";
								break;
							}
						}
					}
				}
			 }
		}
		/**/
	}else{
		if(document.getElementById("_" + idProducto)!=null)
				document.getElementById("_" + idProducto).className = "on";
	}
}

//Funci�n que desde el contenido de m�s info selecciona como marcado el producto en el men� izquierdo.
function activarLinea(idLinea){
	if(idLinea != null){
		if(document.getElementById("_" + idLinea)!=null){
			document.getElementById("_" + idLinea).style.display = "block";
		}
	}else{
		var contenidos = document.getElementsByTagName("ul");
		var encontrado = "N";
		for(i=0;i<contenidos.length; i++){
			if(contenidos[i].className=="menuLinea"){
				if(encontrado == "N"){
					contenidos[i].style.display="block";
					encontrado = "S";
				}else
					contenidos[i].style.display="none";
			}
		}
	}
}

function cerrarFaq(producto){
	url=document.location.href;
	if(url.indexOf("faq")<0){
		document.getElementById("faq").style.display="none";
		document.getElementById("ficha").style.display="block";
		if(document.getElementById("coberturaParrafo")!=null)
		document.getElementById("coberturaParrafo").style.display="block";
	    document.getElementById("pestFicha").style.display="block";
		var titulo = document.getElementById("tituloProd").innerHTML;
		if(titulo.indexOf("&gt;")>0 && titulo.indexOf("Faq")>0)
			document.getElementById("tituloProd").innerHTML = titulo.substring(0,titulo.indexOf("&gt;")-1);
	}else{
		document.location.href=producto;
	}
}

function abrirFaq(producto){
	url=document.location.href;
	if(url.indexOf("productos")>0){
		if(document.getElementById("faq")!=null)
		document.getElementById("faq").style.display="block";
		inicio("div","mFAQ","absolute");
		document.getElementById("ficha").style.display="none";
		if(document.getElementById("coberturaParrafo")!=null)
		document.getElementById("coberturaParrafo").style.display="none";
		document.getElementById("pestFicha").style.display="none";
		document.getElementById("tituloProd").innerHTML = document.getElementById("tituloProd").innerHTML + " > FAQ's";
	}else{
		document.location.href=producto + "?faq";
	}
}

function cerrarFaqSolucion(solucion){
	url=document.location.href;
	if(url.indexOf("faq")<0){
		document.getElementById("faq").style.display="none";
		document.getElementById("ficha").style.display="block";
		var titulo = document.getElementById("tituloSol").innerHTML;
		if(titulo.indexOf("&gt;")>0 && titulo.indexOf("Faq")>0)
			document.getElementById("tituloSol").innerHTML = titulo.substring(0,titulo.indexOf("&gt;")-1);
	}else{
		document.location.href=producto;
	}
}

function abrirFaqSolucion(solucion){
	url=document.location.href;
	if(url.indexOf("soluciones")>0){
		document.getElementById("faq").style.display="block";
		inicio("div","mFAQ","absolute");
		document.getElementById("ficha").style.display="none";
		document.getElementById("tituloSol").innerHTML = document.getElementById("tituloSol").innerHTML + " > FAQ's";
	}else{
		document.location.href=producto + "?faq";
	}
}



function inicioFaq(){
	url=document.location.href;
	if(url.indexOf("faq")>0)
		abrirFaq();
	else
		cerrarFaq();
}

function inicioFaqSolucion(){
	url=document.location.href;
	if(url.indexOf("faq")>0)
		abrirFaqSolucion();
	else
		cerrarFaqSolucion();
}

function enviarProducto(detallada){
	var valor = "";
	var contravalor = "";
	if (detallada == "detallada"){
		valor="tablaComparaDet";
		contraValor = "tablaCompara";
	}else{
		valor="tablaCompara";
		contraValor = "tablaComparaDet";
	}
	var poliza = document.getElementById("poliza2").value;

	for(i=0; i < document.getElementsByName(valor).length; i++){
		if(document.getElementsByName(valor)[i].idContenido != null){
			if(document.getElementsByName(valor)[i].idContenido == poliza)
				document.getElementsByName(valor)[i].style.display="block";
			else
				document.getElementsByName(valor)[i].style.display="none";
		}else{
			if(document.getElementsByName(valor)[i].tagName=="LI")
				document.getElementsByName(valor)[i].className = "";
			document.getElementsByName(valor)[i].style.display="block";
		}
	}

	for(i=0; i < document.getElementsByName(contraValor).length; i++){
		if(document.getElementsByName(contraValor)[i].tagName=="LI"){
			document.getElementsByName(contraValor)[i].className = "on";
		}else{
			document.getElementsByName(contraValor)[i].style.display="none";
		}

	}
}
//window.onresize=colocarZoom;

// Funci�n que activa los combos en la po�gina de contacto
function mostrarSolucion(tagName, idName, objeto){
	valor = document.getElementById(objeto).value;

	var capas = document.getElementsByTagName(tagName);
	var nCapas = capas.length;
	for (i=0;i<nCapas;i++)
	{
		if(capas[i].id != "tp"){
			if (capas[i].id == idName  && capas[i].idAgrupacion == valor)
			{
				capas[i].style.display = "block";
			}else{
				capas[i].style.display = "none";
			}
		}
	}
}

function llamadaBuzon(tagName, idName){
	var capas = document.getElementsByTagName(tagName);
	var nCapas = capas.length;
	for (i=0;i<nCapas;i++)
	{
		if (capas[i].id == idName && capas[i].style.display == "block")
		{
			if(	capas[i].value != ""){
				eval(capas[i].value);
			}else{
				alert("Seleccione una Soluci�n");
			}
		}
	}
}

function inicioContacto(tagName, idName, position){
	var capas = document.getElementsByTagName(tagName);
	var nCapas = capas.length;
	var pos = 0;
	for (i=0;i<nCapas;i++)
	{
		if (capas[i].id == idName)
		{
			if(pos != 0){
				capas[i].style.position = position;
				capas[i].style.display = "none";
			}
			pos++;
		}
	}
}

function paises()
{
	var p = document.forms["pais"].listaPaises.value;
	if (p!="")
	{
		eval("document.location.href = 'paises/info_"+p+".html'");
	}
	else
	{
		alert("Seleccione un pa�s de la lista / Select a country of the list")
	}
}

function popup(msg){
	var content="<div class='altTittle'>"+msg+"</strong></div>";
	yyy=Yoffset;
	document.getElementById("dek").innerHTML=content;
	skn.display="";
}

function checkwhere(e) {
	if (document.layers){
		xCoord = e.x;
		yCoord = e.y;
	}else if (document.all){
		xCoord = event.clientX;
		yCoord = event.clientY;
	}else if (document.getElementById){
		xCoord = e.clientX;
		yCoord = e.clientY;
	}

	skn.left = xCoord+Xoffset+"px";
	skn.top  = yCoord+Yoffset+document.documentElement.scrollTop+"px"; //Utilizamos "document.body.scrollTop" en vez de "document.body.scrollTop"
}

function nocheck(){
	skn.display="none";
}

/* Tel�fonos: abre una capa en concreto */
function telefono(num)
{
	if (eval("document.getElementById('t"+num+"').className == ''"))
	{
		eval("blockNone('tel"+num+"','block')");
		eval("document.getElementById('t"+num+"').className = 'desplegado'")
	}
	else
	{
		eval("blockNone('tel"+num+"','none')");
		eval("document.getElementById('t"+num+"').className = ''")
	}
}

function blockNone(nombre,estado)
{
	document.getElementById(nombre).style.display = estado;
}

/* Consultas: abre la consulta en concreto */
function consultas(num,accion)
{
	if (accion=="abrir")
	{
		eval("blockNone('r"+num+"','block')");
		eval("document.getElementById('c"+num+"').className = 'selec'")
	}
	else
	{
		eval("blockNone('r"+num+"','none')");
		eval("document.getElementById('c"+num+"').className = ''")
	}
}

function activarPromo (url,num,total)
{
			var promo = document.getElementById("promocionTC");
			promo.src = url;
			for(i=1; i<=total; i++)
			{
					var numero = eval("document.getElementById('imgNumPromoTC_"+i+"')");
					if(numero!=null){
						numero.src = "docsmovil/img/imgNum"+i+"-TC.gif";
					}
			}
			var numero = eval("document.getElementById('imgNumPromoTC_"+num+"')");
			numero.src = "docsmovil/img/imgNum"+num+"-On-TC.gif";
}

function activarPromo_old (num)
		{
			var promo = document.getElementById("promocionTC");
			promo.src = "..\\oimtc\\es\\fundacion\\detalle\\TC-subhome-promocion-"+num+".html";
			alert('promo.src'+promo.src);
			for(i=1; i<=4; i++)
			{
				var numero = eval("document.getElementById('imgNumPromoTC_"+i+"')");
				numero.src = "docsmovil/img/imgNum"+i+"-TC.gif";
				alert('numero.src'+numero.src);
			}
			var numero = eval("document.getElementById('imgNumPromoTC_"+num+"')");
			numero.src = "docsmovil/img/imgNum"+num+"-On-TC.gif";
			alert('numero.src'+numero.src);
		}


