var dias = new Array("Domingo","Lunes","Martes","Mi�rcoles","Jueves","Viernes","S�bado");
var meses = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
var fecha = new Date();
var bFlagContinuarProceso=false;
var contratoImp = false;
var bFlagContinuarProcesoEndesa=false;
//var contratoImpEndesa = false;

function formatoFechas(mifecha)
{	
	var formato;
	var tano = mifecha.substring(0,4);
	var tmes = mifecha.substring(5,7);
	var tdia = mifecha.substring(8,10);
	formato = tdia + "/" + tmes + "/" + tano;
	return formato;

}

function formatoFechasSegur(mifecha)
	{	
		var formato;
		var tdia = mifecha.substring(6,8);
		var tmes = mifecha.substring(4,6);
		var tano = mifecha.substring(0,4);
	
                         if (tano=="0")
                          {
 		     formato = "No disponible";
                          }
                          else
                           {
 		      formato = tdia + "/" + tmes + "/" + tano;			
                           }	
		
                       
		return formato;
	}

function calendario(){
	document.getElementById("dia_semana").innerHTML = dias[fecha.getDay()];
	document.getElementById("dia").innerHTML = fecha.getDate();
	document.getElementById("mes").innerHTML = meses[fecha.getMonth()];
	document.getElementById("anio").innerHTML = fecha.getFullYear();
}

function abrirVentana (donde, ancho, alto, scroll){
	var params = "scrollbars="+ scroll + ",width=" + ancho + "px,height=" + alto + "px";
	window.open(donde,'', params);
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v; }
}

function cambiaColor(elemento,num){
if(num ==1){
elemento.style.color="#CE0831";
elemento.style.fontWeight="bold";
}else{
elemento.style.color="#000000";
elemento.style.fontWeight="normal";
}
}

function cambiaColor2(elemento,num){
if(num ==1){
elemento.bgColor="#F7C6D6";
}else{
elemento.bgColor="#DADBD2";
}
return;
}

/* OLD 21-12-2004
function recogeOrdenElementos(oForm) {
	var sElementos = "";
	var obj = "document."+oForm;

	for(i=0; i<obj.length; i++) {
		if( (obj.elements[i].type != "hidden") && (sElementos.indexOf(obj.elements[i].name) == -1) ) {
			sElementos += obj.elements[i].name + "$";
		} else {
			if( (obj.elements[i].type == "hidden") && (obj.elements[i].id == "TITULO") ) {
				sElementos += obj.elements[i].name + "$";
			}
			if( (obj.elements[i].type == "hidden") && (obj.elements[i].id == "MAILRESP") ) {
				sElementos += obj.elements[i].name + "$";
			}
		}
	}
	
	sElementos = sElementos.substring(0, sElementos.lastIndexOf("$"));
	obj.ordenElementos.value = sElementos;	
}*/
function recogeOrdenElementos(oForm) {
	var sElementos = "";
	var obj = "document."+oForm;
	
	for(i=0; i<obj.length; i++) {

		if (document.ContactenosPreAccesoForm[i] != null) {		
			if( (document.ContactenosPreAccesoForm[i].type != "hidden") && (document.ContactenosPreAccesoForm[i].name != undefined) && (sElementos.indexOf(document.ContactenosPreAccesoForm[i].name) == -1) ) {
				sElementos += document.ContactenosPreAccesoForm[i].name + "$";
			} else {
				if( (document.ContactenosPreAccesoForm[i].type == "hidden") && (document.ContactenosPreAccesoForm[i].id == "TITULO") ) {
					sElementos += document.ContactenosPreAccesoForm[i].name + "$";
				}
				if( (document.ContactenosPreAccesoForm[i].type == "hidden") && (document.ContactenosPreAccesoForm[i].id == "MAILRESP") ) {
					sElementos += document.ContactenosPreAccesoForm[i].name + "$";
				}
			}
		}
		else {
			break;
		}
		
	}
	
	sElementos = sElementos.substring(0, sElementos.lastIndexOf("$"));
	document.ContactenosPreAccesoForm.ordenElementos.value = sElementos;	
}	


//----------------------------------------------------------------------
//
//     Funci�n : validacionDigito
// Descripci�n : Comprobaci�n b�sica de que todos los caracteres de la
//               cadena son s�lo uno de estos "0123456789"
//  Par�metros : Cadena con en n�mero a comprobar
//     Retorno : false / true
//       Notas : -
//
//----------------------------------------------------------------------
function validacionDigito( cNumero ) 
{
	// Recorremos los datos introducidos
	for ( nPos = 0; nPos < cNumero.length; nPos++ ) 
	{
		var cCaracter = cNumero.charAt( nPos )
		if ( isNaN( parseInt( cCaracter ) ) ) 
		{
			return false;
		}
	}
	return true;
}
//----------------------------------------------------------------------
//      M�todo : Trim
// Descripci�n : A�ade un m�todo Trim al objeto String de JavaScript 
//               para eliminar blancos por delante y por detr�s de una 
//               cadena
//  Par�metros : -
//     Retorno : Cadena sin espacios por delante y por detras del texto
//       Notas : -
//----------------------------------------------------------------------

function String_trim(Cadena)
{
	// Recorremos los datos introducidos		     
	var lon;
	var p1;
	var p2;
	var c;
	var buffer;
	
	lon=Cadena.length;
	p1=0;
	c=Cadena.charAt(p1);
	
	while (p1<=lon-1 && c==" ")
	{
		p1++;
		c=Cadena.charAt(p1);
	}
	
	p2=lon;
	c=Cadena.charAt(p2-1);
	
	while (p2>=0 && c==" ")
	{
		p2--;
		c=Cadena.charAt(p2-1);
	}

	if (p1>=p2) 
		buffer="";
	  else
	    buffer=Cadena.substr(p1,p2-p1);	

	return buffer;
}
//----------------------------------------------------------------------
//
//     Funci�n : validacionMail()
// Descripci�n : Comprueba que una determinada cadena tiene formato de 
//               direcci�n de correo electr�nico
//  Par�metros : cMail - Cadena a comprobar
//     Retorno : true / false
//     Ejemplo : <input type="text" onBlur="return CheckMail( this.value )">
//
//----------------------------------------------------------------------

function validacionMail( cMail ) {

	// Comprobar que se tiene una arroba y esta no est� al principio
	var nPos1 = cMail.indexOf("@");

	if ( nPos1 < 1 ){
		return false;
	}

	// Comprobar que se termina con un dominio	
	var nPos2 = cMail.lastIndexOf(".");
	if (   nPos2 < nPos1 
		|| nPos2 == nPos1+1 
		|| (nPos2 + 3 > cMail.length) ) {		
		return false;
	}	

	// Comprobar que no contiene blancos entre medias
	if ( cMail.indexOf( " " ) != -1 ) {
		return false
	}

	// El formato parece correcto
	return true;
}


// AQUI EMPIEZAN LAS FUNCIONES PARA LOS SUBMENUS
var timerID = null;
var rollID = null;
var timerOn = false;
var timecount = 500;

function startTime()
{
	if (timerOn == false)
	{
       	timerID = setTimeout( "hideAll()" , timecount);
		timerOn = true;
	}
}

function stopTime()
{
	if (timerOn){
		clearTimeout(timerID);
    timerID = null;
		rollID = null;
    timerOn = false;
    }
}

// OCULTAMOS Y MOSTRAMOS
function showHide(capa){
    hideAll();
		document.getElementById('submenu_0'+capa).style.display = "block";
		document.getElementById('menu_0'+capa).style.backgroundPosition = "top right";
		document.getElementById('menu_0'+capa).style.backgroundColor = "#ccc";
}

function hideAll()
{
	var cuantossubmenus = document.getElementsByTagName("map")["submenuOIM"].childNodes[0].childNodes.length;
	for (i=1;i<=cuantossubmenus;i++)
	{
		document.getElementById('submenu_0'+i).style.display = 'none';
	  document.getElementById('menu_0'+i).style.backgroundPosition = "bottom right";
		document.getElementById('menu_0'+i).style.backgroundColor = "transparent";
	}
}
function posiciona(){
// Accesibilidad:
// Esta funci�n hace que si javascript no est� activo, el submenu aparece sin estilos y por lo tanto no oculta contenido
	document.getElementById('submenuproductos').className = 'submenuprodclass';
	var cuantossubmenus = document.getElementsByTagName("map")["submenuOIM"].childNodes[0].childNodes.length;
	for (i=1;i<=cuantossubmenus;i++){
	// le damos un left a cada submenu
		document.getElementById('submenu_0'+i).style.left = document.getElementById('menu_0'+i).offsetLeft+"px";
	}
}	

// Selecciona la opci�n del combo que coincide con el 
// texto pasado.	
function seleccionarComboTexto(combo, texto){
	var i = 0;
	var ok = "true";
	while ((i < combo.length) && (ok == "true" )) {
	   if (combo[i].text == texto) {
	   	  combo.selectedIndex = i;
	   	  ok = "false";
	   	  break;
	   } else {
		   i++;
	   }
	}
}

/* Abrir Ventana */
function abrirVentana(url,ancho,alto)
{
	eval("window.open(url, 'ventana', 'toolbar=no, directories=no, location=no, status=no, menubar=no, resizable=no, scrollbars=yes, width="+ancho+", height="+alto+"')"); 
}

function enviar(){
			
	var sElementos="direccionCorreo$comentarios";
	document.ContactarForm.ordenElementos.value = sElementos;			
	document.ContactarForm.submit();
	return;
}

 function volverInicio() {
 
 	document.forms[0].action="VolverAction.do";
 	document.forms[0].submit();
 }

function mostrarImprimir(){
	pulsarContinuar();
	window.open("ImprimirContratoAction.do?act=imprimir&tipo=html","","toolbar=1,menubar=1,scrollbars=yes,width=591px,height=525px");
}
/*Funci�n utilizada para controlar que se ha pulsado el bot�n para continuar 
con el proceso de alta y no se ha intentado cerrar la ventana y validar que 
el usuario ha marcado la check de Aceptaci�n de Contrato*/
function aceptarContrato (){
    //Variable de Aceptaci�n Contrato 
    bFlagContinuarProceso = true; 

}

/*Funci�n utilizada para controlar que se ha pulsado el bot�n para continuar 
con el proceso de alta y no se ha intentado cerrar la ventana*/
function pulsarContinuar(){
	bFlagContinuarProceso=true;
	contratoImp=true;
}

function comprueboContratoImprimido(){	
	if(!contratoImp){
		alert("Recuerda que puedes imprimir tu contrato pulsando sobre la imagen imprimir.");
		contratoImp=true;
		return false;
	}
	return true;
}

function controlVentanaContrato(){
    //Compruebo q no se haya pulsado el bot�n que permite continuar con el proceso de alta   
    if (!bFlagContinuarProceso){    	
   		event.returnValue = 'Se dispone a abandonar el proceso de alta en el �rea de Clientes. Todos los datos introducidos se perder�n. �Desea Continuar?';    		      
    }
}

function controlVentanaPreguntas (){
	//Compruebo q no se haya pulsado el bot�n que permite continuar con el proceso de alta   
    if (!bFlagContinuarProceso){    	
   		event.returnValue = 'El proceso de alta a�n no ha finalizado. Si cierra la ventana no se habr� dado de alta correctamente en el �rea de Clientes. �Continuar?';	      
    }
}

function controlVentanaFinProceso(){
	//Compruebo q no se haya pulsado ning�n link o bot�n en la ventana   
    if (!bFlagContinuarProceso){    	
   		event.returnValue = 'Recuerda que, en adelante, el acceso al �rea de Clientes deber�s realizarlo a trav�s de www.mapfre.com.\nTe agradecemos nuevamente la confianza que has depositado con nosotros.';	      
    }
}
/* Funciones del Buscador de Oficinas de Mutua*/
	function validarCP()
	{
		var subCodPostal = ""; 
		var codPostal = document.tarificador.cpOfi.value;
		if (codPostal.length == 0)
		{
			alert("Debe introducir un c�digo postal.");
			return false;
		}
		else
		{
			if (codPostal.length == 5)
			{
				subCodPostal = codPostal.substring(0, 2);
			
				if ((0 < parseFloat(subCodPostal)) && (parseFloat(subCodPostal) < 53)) 
				{
					
					
					return true;
				}
				else
				{
					alert("Revise el c�digo postal introducido.\n Debe ser un n�mero entre 01xxx y 52xxx.");
					return false;
				}
				
			}
			else
			{
				alert ("Revise el c�digo postal introducido.\n Debe ser un n�mero entre 01xxx y 52xxx.");
				return false;
			}
			
		}
	}
	
	function buscar()
	{
		if (document.tarificador2.comunidad.options[document.tarificador2.comunidad.selectedIndex].text == "[Seleccione]")
		{
			alert("Debe indicar una comunidad.");
			return false;
		}
		else if (document.tarificador2.provincia.options[document.tarificador2.provincia.selectedIndex].text == "[Seleccione]")
		{
			alert("Debe indicar una provincia.");
			return false;
		}
		
		return true;
		

	}

/* Fin funciones del Buscador de Oficinas de Mutua*/

function LongitudComentarios(){
	var cadenaasunto = document.forms[0].comentarios.value;
	//document.ContactenosProductoActionForm.asunto.value = String_trim(cadenaasunto);
	if(String_trim(cadenaasunto).length > 1700){
		document.forms[0].comentarios.value = cadenaasunto.substring(0,1700);
		alert("Los comentarios exceden la longitud permitida");
		return;
	}
}

/*Funci�n utilizada para controlar que se ha pulsado el bot�n para continuar 
con el proceso de alta y no se ha intentado cerrar la ventana y validar que 
el usuario ha marcado la check de Aceptaci�n de Contrato*/
function aceptarContratoEndesa (){
    //Variable de Aceptaci�n Contrato 
    bFlagContinuarProcesoEndesa = true; 

}

function controlVentanaContratoEndesa(){
    //Compruebo q no se haya pulsado el bot�n que permite continuar con el proceso de alta   
    if (!bFlagContinuarProcesoEndesa){    	
   		event.returnValue = 'Se dispone a abandonar el proceso de alta en el �rea de Clientes. Todos los datos introducidos se perder�n. �Desea Continuar?';    		      
    }
}

/*Funci�n utilizada para controlar que se ha pulsado el bot�n para continuar 
con el proceso de alta y no se ha intentado cerrar la ventana*/
function pulsarContinuarEndesa(){
	bFlagContinuarProcesoEndesa=true;
}

function controlVentanaPreguntasEndesa (){
	//Compruebo q no se haya pulsado el bot�n que permite continuar con el proceso de alta   
    if (!bFlagContinuarProcesoEndesa){    	
   		event.returnValue = 'El proceso de alta a�n no ha finalizado. Si cierra la ventana no se habr� dado de alta correctamente en el �rea de Clientes. �Continuar?';	      
    }
}

function controlVentanaFinProcesoEndesa(){
	//Compruebo q no se haya pulsado ning�n link o bot�n en la ventana   
    if (!bFlagContinuarProcesoEndesa){    	
   		event.returnValue = 'Recuerde que, en adelante, el acceso al �rea de Clientes deber� realizarlo a trav�s de www.mapfre.com.\nLe agradecemos nuevamente la confianza que ha depositado en nosotros.';	      
    }
}

function mostrarImprimirEndesa(){
	pulsarContinuarEndesa();
	window.open("ImprimirContratoAction.do?act=imprimir&tipo=html","","toolbar=1,menubar=1,scrollbars=yes,resizable=yes,width=640px,height=480px");
	contratoImpEndesa=true;
}

function comprueboContratoImprimidoEndesa(){	
	if(!contratoImpEndesa){
		alert("Recuerde que puede imprimir su contrato pulsando sobre la imagen imprimir.");
		contratoImpEndesa=true;
		return false;
	}
	return true;
}

function irContactoEndesa(){
	document.location.href="IntContactenosCampanyaEndesa.do";
}

function LongitudComentariosEndesa(){
	var cadenaasunto = document.forms[0].comentarios.value;
	//document.ContactenosProductoActionForm.asunto.value = String_trim(cadenaasunto);
	if(String_trim(cadenaasunto).length > 1700){
		document.forms[0].comentarios.value = cadenaasunto.substring(0,1700);
		alert("Los comentarios exceden la longitud permitida");
		return;
	}
}

function enviarEndesa(){
			
	var sElementos="direccionCorreo$comentarios";
	document.ContactarFormEndesa.ordenElementos.value = sElementos;			
	document.ContactarFormEndesa.submit();
	return;
}

function volverInicioEndesa() {
 
 	document.forms[0].action="VolverEndesaAction.do";
 	document.forms[0].submit();
}

function codigoPostalUserEndesa(){
	var strCP = document.forms[0].cp.value;
	if (strCP.length!=5){
		alert("Debe introducir un c�digo postal v�lido");
		return;
	}
	strCP = strCP.substring(0,2);
	if(seleccionarCombo2(document.forms[0].cboProvincia, strCP)){
		document.forms[0].provincia.value=document.forms[0].cboProvincia.options[document.forms[0].cboProvincia.selectedIndex].text;	
	}else{
		document.forms[0].provincia.value="";
	}
}

function borrarProvinciaUserEndesa(){	
	if (document.getElementById('cp').value == ""){
		if(document.forms[0].provincia.value != ""){
			document.forms[0].provincia.value="";	
		}
	}
}

function cargarDescripcionTipoVia (){
	document.forms[0].descripcionTipoVia.value=document.forms[0].codTipoVia.options[document.forms[0].codTipoVia.selectedIndex].text
}

