//  Par�metros : *  Cadena que indica el tipo de validaci�n o 
//                  transformaci�n que desamos seg�n los siguientes
//                  indicadores:
//                   N Admite s�lo expresiones Num�ricas
//                   D Admite s�lo D�gitos (no admite puntos, espacios
//                     o s�mbolos matem�ticos)
//                   A Admite s�lo caracteres Alfanum�ricos (sin
//                     espacios o s�mbolos de puntuaci�n)
//                   L Admite s�lo Letras (sin espacios o s�mbolos 
//                     de puntuaci�n)
//                   M Combierte la entrada a May�sculas
//                   m Combierte la entrada a min�sculas
//                   S Combierte la entrada a su correspondiente sin 
//                     s�mbolos diacr�ticos, excepto en el caso de 
//                     la e�e (��)
//                   F S�lo admite digitos y los caracteres "/" y "-"
//                     se utiliza para fechas
//     Ejemplo : <input type="text" onkeypress="Mascara('MS')">

function Mascara( cTipo )
{
 
 

  for ( nCont = 0; nCont < cTipo.length; nCont++ ) {
    switch ( cTipo.charAt( nCont ) ) {
    case "N":
      if ( isNaN( parseInt( String.fromCharCode( event.keyCode ) ) ) 
           && String.fromCharCode( event.keyCode ) != "-"
           && String.fromCharCode( event.keyCode ) != "+"
           && String.fromCharCode( event.keyCode ) != "."
           && String.fromCharCode( event.keyCode ) != "," ) {
        event.returnValue = false;
      }
      break;
    case "D":
      if ( isNaN( parseInt( String.fromCharCode( event.keyCode ) ) ) ) {
        event.returnValue = false;
      }
      break;
    case "L":
      if ( String.fromCharCode( event.keyCode ).toUpperCase() == String.fromCharCode( event.keyCode ).toLowerCase() ) {
        event.returnValue = false;
      }
      break;
    case "A":
      if ( String.fromCharCode( event.keyCode ).toUpperCase() == String.fromCharCode( event.keyCode ).toLowerCase() 
           && isNaN( parseInt( String.fromCharCode( event.keyCode ) ) ) ) {
        event.returnValue = false;
	return false;
      }
      break;
    case "M":
   
      event.keyCode = String.fromCharCode( event.keyCode ).toUpperCase().charCodeAt(0);
    
      break;
    case "m":
      event.keyCode = String.fromCharCode( event.keyCode ).toLowerCase().charCodeAt(0);
      break;
    case "S":
      switch ( String.fromCharCode( event.keyCode ) ) {
      case "�": case "�": case "�": case "�": case "�": case "�":
        event.keyCode = "A".charCodeAt(0); 
        break;
      case "�":
        event.keyCode = "A".charCodeAt(0); 
        break;
      case "�":
        event.keyCode = "C".charCodeAt(0); 
        break;
      case "�": case "�": case "�": case "�":
        event.keyCode = "E".charCodeAt(0); 
        break;
      case "�": case "�": case "�": case "�":
        event.keyCode = "I".charCodeAt(0); 
        break;
      case "�":
        event.keyCode = "D".charCodeAt(0); 
        break;
      case "�": case "�": case "�": case "�": case "�": case "�":
        event.keyCode = "O".charCodeAt(0); 
        break;
      case "�": case "�": case "�": case "�":
        event.keyCode = "U".charCodeAt(0); 
        break;
      case "�":
        event.keyCode = "Y".charCodeAt(0); 
        break;
      case "�": case "�": case "�": case "�": case "�": case "�": case "�":
        event.keyCode = "a".charCodeAt(0); 
        break;
      case "�":
        event.keyCode = "c".charCodeAt(0); 
        break;
      case "�": case "�": case "�": case "�":
        event.keyCode = "e".charCodeAt(0); 
        break;
      case "�": case "�": case "�": case "�":
        event.keyCode = "i".charCodeAt(0); 
        break;
      case "�": case "�": case "�": case "�": case "�": case "�": case "�":
        event.keyCode = "o".charCodeAt(0); 
        break;
      case "�": case "�": case "�": case "�":
        event.keyCode = "u".charCodeAt(0); 
        break;
      case "�": case "�":
        event.keyCode = "y".charCodeAt(0); 
        break;
      }
      break;
    case "F":
      if ( isNaN( parseInt( String.fromCharCode( event.keyCode ) ) ) 
           && String.fromCharCode( event.keyCode ) != "/"
           && String.fromCharCode( event.keyCode ) != "-" ) {
        event.returnValue = false;
      }
      break;
    }
  }
}

//-------------------
// Fin de mascara.js
//-------------------


