// combosAjax.js

var primeraVez = true;
function nuevoAjax(){

	var xmlhttp=false;
	try{
		xmlhttp= new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try{
			xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
		}catch(E){
			xmlhttp=false;
		}
	}
	
	if(!xmlhttp && typeof XMLHttpRequest!='undefined'){
		xmlhttp= new XMLHttpRequest();
	}
	return xmlhttp;	
	
}

function cargarContenido(idSelect,opcionSelect){
	selectObject = document.getElementById(idSelect);
	ajax= nuevoAjax();
	ajax.open("GET","/oim/cargaTipoProductosAjaxAction.do",false);
	ajax.send(null);
	if(ajax.readyState==4){
		 var xml = ajax.responseXML;
		 if (xml != null){
			 if(xml.getElementsByTagName("descripciontipo").length>0){
				var i=0;
				var tipo ;
				var desTipo ="";
				borrarSelect(selectObject);	
				var valueOption = "";		
				while(i>=0){
						tipo = xml.getElementsByTagName("descripciontipo")[i];
						if(tipo !=null){
							
							desTipo=tipo.firstChild.data;
							
							var op= document.createElement("OPTION");
							op.value=desTipo;
							op.text=desTipo;
							
							if(desTipo==opcionSelect){
								op.selected=true;
								valueOption = desTipo;
								
							}
							
							selectObject.options[i]=op;
							if (op.selected==true) {
								selectObject.value = valueOption;
							}	
							i++;
						}else{
							i=-1;
						}
				}
				selectObject.onchange.call();
			
			}
		}
	}
	
}


function borrarSelect (objSelect) {
	var j;
	var tamanSelect = objSelect.length; 
	for(j=tamanSelect-1;j>=0;j--){
		objSelect.remove(j);
	}
}


function cargarProductos(tipoProducto,idSelect,opcionSelect,urlShadowAdjuntos, msgSeleccione, msgError){
	
	
	selectObject = document.getElementById(idSelect);
	
	ajax= nuevoAjax();
	ajax.open("GET","/oim/cargaProductosAjaxAction.do?tipo="+tipoProducto,false);
	ajax.send(null);
	
		if(ajax.readyState==4){
			 var xml = ajax.responseXML;
			 if (xml != null ){
			 if(xml.getElementsByTagName("coEntidad").length>0){
			 	var codEntidad=xml.getElementsByTagName("coEntidad")[0];
			 	
					if(codEntidad.firstChild!= null){
						 document.getElementById('codEntidad').value = codEntidad.firstChild.data;
					}else{
						document.getElementById('codEntidad').value = codEntidad.text;
					}
				
			 }
				 if(xml.getElementsByTagName("optText").length>0){
					var i=0;
					var producto ;
					var desProducto ="";
					var id_producto="";
					var id;
					borrarSelect(selectObject);				
					while(i>=0){
							producto = xml.getElementsByTagName("optText")[i];
							id =xml.getElementsByTagName("optValue")[i];
							if(producto !=null){
								id_producto=id.firstChild.data;
								desProducto=producto.firstChild.data;
								var op= document.createElement("OPTION");
								op.value=id_producto;
								op.text=desProducto;
								if(id_producto==opcionSelect){
									op.selected=true;
								}
								selectObject.options[i]=op;
								i++;
							}else{
								i=-1;
							}
					}
				selectObject.onchange.call();
				comprobarTodo(urlShadowAdjuntos, msgSeleccione, msgError );
				}
			}
		}
}


function cargarNumContrato(idNumContrato,productoSelect){
	contrato= document.getElementById(idNumContrato);
	contrato.value = "";
	ajax= nuevoAjax();
	ajax.open("GET","/oim/cargaNumContratoAjaxAction.do?producto="+productoSelect,false);
	ajax.send(null);
	if(ajax.readyState==4){
			 var xml = ajax.responseXML;
			 if (xml != null ){
				
					var contratoValor=xml.getElementsByTagName("descripcionContrato")[0];
					if(contratoValor!=null){
						var valor;
						if(contratoValor.firstChild!= null){
							 valor = contratoValor.firstChild.data;
						}else{
							valor = contratoValor.text;
						}
						contrato.value=valor;
					}
					if (primeraVez){
						primeraVez = false;
					
					}else {
						document.getElementById('numExpediente').value='';
					
					}				
			}
		}
}


function cargarNumContratoContactenos(idNumContrato,productoSelect){
	contrato= document.getElementById(idNumContrato);
	contrato.value = "";
	ajax= nuevoAjax();
	ajax.open("GET","/oim/cargaNumContratoContactenosAjaxAction.do?producto="+productoSelect,false);
	ajax.send(null);
	if(ajax.readyState==4){
			 var xml = ajax.responseXML;
			 if (xml != null ){
				
					var contratoValor=xml.getElementsByTagName("descripcionContrato")[0];
					var valor;
					if(contratoValor.firstChild != null){
						valor = contratoValor.firstChild.data;
					}else{
						 valor = contratoValor.text;
					}
					contrato.value=valor;
			}
		}
}



function cargarProductosContactenos(tipoProducto,idSelect,opcionSelect){
	
	selectObject = document.getElementById(idSelect);
	
	ajax= nuevoAjax();
	ajax.open("GET","/oim/cargaProductosContactenosAjaxAction.do?tipo="+tipoProducto,false);
	ajax.send(null);
	
		if(ajax.readyState==4){
			 var xml = ajax.responseXML;
			 if (xml != null ){
				 if(xml.getElementsByTagName("optText").length>0){
					var i=0;
					var producto ;
					var desProducto ="";
					var id_producto="";
					var id;
					borrarSelect(selectObject);				
					while(i>=0){
							producto = xml.getElementsByTagName("optText")[i];
							id =xml.getElementsByTagName("optValue")[i];
							if(producto !=null){
								id_producto=id.firstChild.data;
								desProducto=producto.firstChild.data;
								var op= document.createElement("OPTION");
								op.value=id_producto;
								op.text=desProducto;
								if(id_producto==opcionSelect){
									op.selected=true;
								}
								selectObject.options[i]=op;
								i++;
							}else{
								i=-1;
							}
					}
					
				selectObject.onchange.call();
				
				}
			}
		}
		
		//A�adir opcion "No veo mi producto"
		var newOptionText = "No veo mi producto";
		var opt = document.createElement("option");
		opt.value = newOptionText;
		opt.text = newOptionText;
		selectObject.add(opt);
		
}

function habilitarCombo(tipo,producto){
	if(tipo != "[Seleccione]"){
		document.getElementById(producto).disabled = '';
	}else{
		document.getElementById(producto).disabled = true;
	}
}



















